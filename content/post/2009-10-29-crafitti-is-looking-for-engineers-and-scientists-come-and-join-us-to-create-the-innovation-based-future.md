---
title: CRAFITTI is Looking for Engineers and Scientists – Come and join us to create the Innovation based future
author: Navneet Bhushan
date: 2009-10-29T04:11:48+00:00
banner: blogs/36-Crafitti_Header.png
url: /2009/10/crafitti-is-looking-for-engineers-and-scientists-come-and-join-us-to-create-the-innovation-based-future/
post_views_count:
  - 249
  - 249
categories:
  - Justrizin

---
**Advertisement – Invention & Patent Analyst (Trainee)**

<p style="text-align: left;">
  <img src="blogs/36-Crafitti_Header.png" alt="" />
</p>

Crafitti Consulting Pvt. Limited (<http://www.crafitti.com>) is looking for Invention & Patent Analyst Trainees with 0-2 years experience with following minimum qualification – BE/BTech (Chemical, Mechanical, Electrical, Computer Science, Bio-medical) or M.Sc (Chemistry, Electronics, biochemistry, etc).
  
Selected candidates will go through a comprehensive training course for one year as detailed below:

(a) 0-3 months: Training and Certification as Invention Analyst (No Stipend)
  
(b) 4-6 months: Application in live assignments (with stipend)
  
(c) 7-12 months: Comprehensive Invention Analysis Project (with Stipend)

Selected candidates will be absorbed as “Invention & Patent Analyst” at Crafitti after one year of training, based on performance.

An entrance test will be held in second week of November. Interested candidates can send their resume to info@crafitti.com. You can also call us at +91-80-26993728 during 3.00 PM – 5.00 PM on weekdays.

**Crafitti Consulting Private Limited
  
An NSRCEL Incubated Company at IIM Bangalore
  
<http://www.crafitti.com>,
  
info@crafitti.com Phone: +91-80-26993728**