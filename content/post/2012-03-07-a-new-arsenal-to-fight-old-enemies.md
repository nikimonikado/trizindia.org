---
title: A new arsenal to fight old enemies
author: Murali Loganathan
date: 2012-03-07T16:00:32+00:00
banner: /images/default.jpg
url: /2012/03/a-new-arsenal-to-fight-old-enemies/
post_views_count:
  - 509
  - 509
categories:
  - Justrizin
  - Tools

---
# <span style="font-family: arial,helvetica,sans-serif;" class="font-size-2"><a href="http://www.thehindu.com/sci-tech/agriculture/article2964302.ece" target="_blank">A new arsenal to fight old enemies by Martin</a> recently in The Hindu is one of the extreme examples of Resources (something that is available for free and has potential utility)  that I have seen. I am reproducing the article below with <em><strong>emphasis</strong></em> on Resources, check it out. The frequency of resource occurring in our mind is possibly higher only if we pay attention deliberately. </span> {.detail-title}

<div class="articleLead">
  <blockquote>
    <p>
      A farmer in Nilampur puts to good use <strong><em>FM radio broadcasts</em></strong> against wild boar raids on her yam and tapioca farm
    </p>
  </blockquote>
</div>

> <p class="body">
>   What do <em><strong>detergent cakes, abandoned compact discs, sardines, FM radio broadcasts and good old garlic</strong></em> have in common?
> </p>
> 
> <p class="body">
>   They are part of a new arsenal being built by farmers across Kerala to fight old enemies that include wild boars and hares; stem borers and rice sappers.
> </p>
> 
> <p class="body">
>   Farmers have come up with new concoctions and found new applications for things of everyday use such as <em><strong>plastic bags, used rubber tyres and old cotton saris</strong></em>, robustly vouched for by a new compendium of farm knowledge.
> </p>
> 
> <p class="body">
>   Running into more than 30 print pages, it is the fruit of an elaborate exercise by <em><strong>Kudumbashree poverty eradication mission</strong></em>, credited with providing the blueprint for the National Rural Livelihood Mission (NRLM) and its sub-programme Mahila Kisan Sashaktikaran Pariyojana (MKSP).
> </p>
> 
> <p class="body">
>   Compiling the tome took more than <em><strong>six months</strong></em> and involved <em><strong>68,000 women farmers</strong></em> in 699 panchayats, who were brought together to share their best practices. With scribes in toe, every word was taken down for posterity.
> </p>
> 
> <p class="body">
>   In its present form, Nattarivu (literally, local knowledge) stands on its own and endorses the need for thinking out of the box and adopting a hands-on approach to common and varied problems.
> </p>
> 
> <p class="body">
>   Stringing up <em><strong>old compact discs using abandoned cloth around a plot</strong></em> can considerably reduce attacks by wild boars, say farmers in Kozhikode. A ‘savoury&#8217; <em><strong>mix of cement and fried rice flour</strong></em> is a rat-killer without parallel, say farmers in Wayanad.
> </p>
> 
> <p class="body">
>   They also found that <em><strong>music from mobile phones</strong></em> drives away animals just as <em><strong>cotton balls soaked in jaggery</strong></em> liquid are traps easily taken in by rats.
> </p>
> 
> <p class="body">
>   <em><strong>Yellow boards smeared with castor oil</strong></em> are effective fly traps, say farmers in Thrissur.
> </p>
> 
> <p class="body">
>   Their counterparts in Ernakulam found the slurry from a mixture of <em><strong>sardines and jaggery</strong></em>, seasoned over <em><strong>45 days</strong></em>, effective against pests in root crops.
> </p>
> 
> <p class="body">
>   Reena Gijo, a farmer in Nilampur puts to good use <em><strong>FM radio broadcasts</strong></em> against wild boar raids on her yam and tapioca farm. She says that the <em><strong>endless chatter on FM radio</strong></em> makes the animals a little cagey. And, the word is spreading.
> </p>