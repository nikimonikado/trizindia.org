---
title: Light Weight Vs Heavy Weight
author: Prakasan K
date: 2010-02-19T17:54:46+00:00
banner: /images/default.jpg
url: /2010/02/light-weight-vs-heavy-weight/
post_views_count:
  - 359
  - 359
categories:
  - Justrizin

---
<p style="margin-top: 4px;">
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">decided to be fit this year and reduce my personal carbon footprint by introducing a bicycle in my daily life. I spent more money (considering this will be a worthy, onetime investment) than I would spend in a high class fitness center on this. This one is an imported bicycle, weighing about 5 -8 Kg (I heard there are cycles weigh just about 5 Kg!), and attracts lot of attention when I ride 🙂</span>
</p>

<p style="margin-top: 4px;">
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">As we all know, the reduced weight is an important aspect of racer bikes to provide enough comfort, speed, modularity etc to the rider, but the reduced weight is also a big problem in India for the high end cycle consumer base, and may be in many other countries too. Here is why;</span>
</p>

<p style="margin-top: 4px;">
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">I want to use this cycle for all my local travels, including going to shops, going to restaurants for my dinner etc. But, I&#8217;m really afraid to take the cycle for anything other than a morning fitness ride so that I don&#8217;t have to â€œparkâ€ anywhere. I&#8217;m afraid of the theft if have I parked, even after locking the rear wheel as usually done; the reduced weight means even a 10 year old boy can lift the cycle and simply walk away!</span>
</p>

<p style="margin-top: 4px;">
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">One obvious option is that I lock with a long chain, but we need some fixed, â€œintermediaryâ€ object to do that. This is the common solution available in some countries, but the super-system (government, perhaps?) has provided the options.</span>
</p>

<p style="margin-top: 4px;">
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">Well, this is not my own problem by the way. I did a small search including the shop I purchased this. Interestingly, all the high end bicycle consumers are not using their cycles as much as they would like due to this reason. The bicycle industry is in India is growing. Unlike in the past, there are bicycles available for USD 4000.00 (costs more than a Tata Nano) and interestingly there are buyers (any bicycle above USD 500 will have a lead time of 3 weeks) for these varieties.</span>
</p>

<p style="margin-top: 4px;">
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">Other obvious solutions are :<br />&#8211; Encourage government to provide parking areas for bicycles, including the &#8220;fixed, intermediary&#8221; option to lock it : About changing the system<br />&#8211; Dismantle the parts and take away the small parts with you, without which the cycle is not usable. Still, not sure..<br />&#8211; Encourage people around where I park the cycle to keep an eye on 🙂 Use the free resources around me..<br />&#8211; Insurance coverage for bicycles: One company in India is already providing this for high end cycles, but at a premium, and nowhere close to the 10% of the cost</span>
</p>

<p style="margin-top: 4px;">
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">As usual, I have tried to use this problem as a case study in my TRIZ sessions, and also wanted to hear from others through this commentory. While this is explained here only through the contradictions, application of 9-Windows, IFR and Trends would be more interesting for a real solution.</span>
</p>

<p style="margin-top: 4px;">
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">Looking though the sub-system contradiction: I want a light weight cycle(for all the good thing I mentioned above) Vs I want the cycle to be heavy (So that no simple lifting is possible). There are more contradictions we can talk about, at sub-system, system, and super-system. What do you think are the contradictions, and solutions?</span>
</p>

<p style="margin-top: 4px;">
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">I also shared this problem with my TRIZ friends, and here is an interesting illustration of TRIZ applied by Dr. Ragunath.</span>
</p>

<p style="margin-top: 4px;">
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">In fact you cannot have a meaningful system contradiction with improving parameter being (weight of moving object) and worsening parameter being (weight of the stationary object) as the contradiction matrix for this is empty!</span>
</p>

<p style="margin-top: 4px;">
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">If OTOH you read the classical TRIZ books you will find similar problems about ships to be light & heavy at different times and the solution being filling in/emptying out of sea water into the buoyancy chambers in the hull. This is separation in time principle for the resolution of this physical contradiction. Assuming that the weight of the bicycle does not change when it is moving or when stationary (except for your weight 🙂 ), what other parameters are there to conflict resolution?</span>
</p>

<p style="margin-top: 4px;">
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">if you forget the fact that heaviness is insurance against theft, what worsening parameter do you map theft to? Loss of substance :)? I used to attend college in Canada where lots of students come cycling. They used to detach their seats and front wheels and bring along with them to the class leaving only the frame and back wheel chained! The fear there was people used to steal the detachable parts of the bike 🙁</span>
</p>

<p style="margin-top: 4px;">
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">Probably people won&#8217;t steal what is not a whole bike (?)</span>
</p>

<p style="margin-top: 4px;">
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">If you think of the worsening parameter (due to reduced weight of the (stationary)cycle â€&#8221; param 2) as Object Affected Harmful Factors (param 30), you get the following principles as applicable:</span>
</p>

<p style="margin-top: 4px;">
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">2 : Taking out</span>
</p>

<p style="margin-top: 4px;">
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">19 : Periodic Action</span>
</p>

<p style="margin-top: 4px;">
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">22 : Blessing in Disguise</span>
</p>

<p style="margin-top: 4px;">
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">37 : Thermal expansion</span>
</p>

<p style="margin-top: 4px;">
  <p>
    <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">Do you take out the valuable parts of a cycle to avoid its theft?<br />Do you periodically peek to check if anybody is near the bike? Or employ someone to watch it? Can&#8217;t think of blessing in disguise and thermal expansion principles here.</span>
  </p>
  
  <p style="margin-top: 4px;">
    <p style="margin-top: 4px;">
      <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">Is potential theft of a light-weight cycle an object generated or object affected harmful effect? OTOH if you take the worsening parameter to be param 31 â€&#8221; object <strong>generated harmful factors</strong> you get the following principles:</span>
    </p>
    
    <p style="margin-top: 4px;">
      <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">35 : Parameter Changes</span>
    </p>
    
    <p style="margin-top: 4px;">
      <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">22 : Blessing in Disguise</span>
    </p>
    
    <p style="margin-top: 4px;">
      <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">1 : Segmentation</span>
    </p>
    
    <p style="margin-top: 4px;">
      <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">39 : Inert Atmosphere</span>
    </p>
    
    <p style="margin-top: 4px;">
      <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">If the bike is so flexible that it can be rolled up and pocketed then may be parameter changes works.</span>
    </p>
    
    <p style="margin-top: 4px;">
      <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;"><strong>Inert atmosphere</strong> &#8211; is it a benign environment where you leave the bike without worry of theft? Or is it that you don&#8217;t worry if it is stolen â€&#8221; like you cantrack it through GPS? Looks last solution is feasible but expensive than insurance? Even when one insures, the insurer would like to track, right?</span>
    </p>