---
title: Solution for the last mile problem – Successful innovation ideas
author: Prakasan K
date: 2009-08-14T06:00:00+00:00
banner: blogs/42-9w.GIF
url: /2009/08/solution-for-the-last-mile-problem-successful-innovation-ideas/
post_views_count:
  - 386
  - 386
categories:
  - Justrizin

---
The success and failure of innovation is overly discussed. Just do a google (or binging) around, and you will be directed to zillions of articles with advices and experiences listed out with &#8220;n&#8221; factors of success and failure of innovations. However, have you ever noticed the success of innovation some time is really dependent on solving the “last mile problem”?

Consider the innovations happening in the alternative fueled cars, especially Electric Vehicles. Have you really seen any successful implementation of this innovation benefiting to the mass consumer base? While there are new ideas being implemented to store electricity, light weight materials, high end technologies underhood, the EV&#8217;s are still not there on road even 0.5% of the gas guzzlers. Is that because we don&#8217;t have enough success in making an EV? Perhaps not; every year, any famous car shows around the world, there are at least 3 new EV cars are being displayed, and the cost is really coming down. Yet the manufactures, Nissan of the world are perfecting their cars trying to provide higher mileage at one charge.

When Ford asked some help from Edward deBono to improve their market share, the idea he had was Ford buying parking lots around the country and allow only Ford car owners to park. I don&#8217;t know whether Ford really implemented this idea. What if the EV manufacturers do some lateral implementation instead of trying to perfect their solution, and provide the &#8220;last mile solution&#8221;?

Here is a company in Israel doing the same, <a href="http://www.betterplace.com/solution/" target="_blank">Better Place</a>. Do visit their website, and learn what they are doing. This is really a simple business idea thinking at the super-system level (while the manufacturers are thinking at the sub-system level perfection) and providing solutions to the customer. Draw a simple 9-Windows on this, and here is my interpretation.

<p style="text-align: left;">
  <img src="blogs/42-9w.GIF" alt="" width="634" height="403" />
</p>

The future part of this 9-Windows is intentionally left blank. If Better Place can look at the last mile problems of making EV cars successful, the future, super-system thinking should help someone to create new business ideas.
  
What are your ideas?