---
title: TrizTrends 2014
author: Murali Loganathan
date: 2014-03-21T02:26:40+00:00
banner: /images/default.jpg
url: /2014/03/triztrends-2014/
post_views_count:
  - 2865
categories:
  - Events
tags:
  - art
  - conference
  - evolution
  - ge
  - popular triz
  - techniques

---
Set in the picturesque GE Bangalore campus had some interesting pick of speakers. Thanks to GE for being a great host.
  
Peter from Reliance was hyper selective with his picks with just the contradiction and principles as the approach and I liked what worked in teams from his experiments.
  
Of course he hit the mark with India being a country of contradictions.

Sergei shared some interesting roi numbers and gave clear idea of Triz evolution. I liked the test done with 3 contradiction matrix, as long as ideas solved problems all is well. Most popular techniques was cool. Looks like I have made my choices always popular.

Interesting non technical examples resolving contradictions from art, politics, nature and religion from Sergei and Alex.
  
Debjani spoke about Triz being applied in IP field. Shankar spoke about his forever fresh innovation flow.
  
We also relaunched our site in front of Triz masters and some very innovative people. Alex&#8217;s definition of disruptive as creating access to new resources was spot on.