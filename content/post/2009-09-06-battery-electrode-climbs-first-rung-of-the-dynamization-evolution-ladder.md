---
title: Battery Electrode climbs first rung of the Dynamization evolution ladder
author: Dr Shankar MV
date: 2009-09-06T07:02:37+00:00
banner: blogs/40-mitliquidbattery.jpg
url: /2009/09/battery-electrode-climbs-first-rung-of-the-dynamization-evolution-ladder/
post_views_count:
  - 340
  - 340
categories:
  - Justrizin

---
<p style="text-align: left;">
  <img src="blogs/40-mitliquidbattery.jpg" alt="" />
</p>

As a materials scientist, I constantly look for breakthrough in new materials that enable technology to move up the evolutionary curve. I present here two recent materials innovation that enable cleaner batteries. The electrode of the battery is conventionally a rigid solid &#8211; but this is no longer true.

(a) MIT invent ecofriendly 100%-liquid battery
  
http://www.greenoptimistic.com/2009/03/08/mit-liquid-battery/?wscr=1280&#215;800

What you see in the image is a liquid battery, everything in it is liquid. It is composed of two molten metals, acting as electrodes, and one molten salt, acting as an electrolyte.

When the battery is fully charged, the two metals are separated from the electrolyte. When you connect a load on the battery, the metals slowly react and dissolve into the electrolyte, ionize, and the electrolyte portion is getting larger, as the electrodes get thinner. When you charge the battery, magnesium ions are reduced and the antimony ions are oxidized, and return to their initial state. The process resembles the picture above.

(b) A Liquid Design for Cheaper Fuel Cells &#8211; A platinum-free liquid cathode could cut fuel-cell costs by 40 percent. http://www.technologyreview.com/energy/23348/

A recent R&D breakthrough, reported in MIT technology Review, has enabled battery technology to move from the expensive solid platinum electrode to a liquid electrode enabling comparable battery performance.

The battery&#8217;s electrode is no longer a rigid solid &#8211; but a more flexible liquid (Molybdenum & vanadium instead of platinum &#8211; this reduces the cost very significantly)

Platinum remains the best material for speeding chemical reactions in hydrogen fuel cells, although the scarcity and cost of this element keep fuel cells from becoming more affordable and practical. Most alternative approaches involve simply replacing the platinum in the electrodes. Now a U.K. company called ACAL Energy has overhauled fuel cell design to reduce the amount of platinum used by 80 percent.

Inside ACAL Energy’s fuel-cell stacks, the cathode is replaced with a platinum-free catalyst solution, which could reduce costs by 40 percent. In a conventional fuel cell, platinum is embedded in porous carbon electrodes. ACAL&#8217;s design replaces this with a solution containing low-cost molybdenum and vanadium as the catalyst. The resulting fuel cell works as well as a conventional one but should cost 40 percent less, the company says.

This is only the first rung of the evolutionary ladder. Each of the higher rungs is an innovation opportunity. A battery is a treasure house of technical contradictions &#8211; a great place to apply TRIZ & resolve contradictions.