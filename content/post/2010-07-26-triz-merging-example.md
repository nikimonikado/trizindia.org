---
title: TRIZ – Merging Example
author: Kumar Rajamani
date: 2010-07-26T22:30:00+00:00
banner: /images/default.jpg
url: /2010/07/triz-merging-example/
post_views_count:
  - 666
  - 666
categories:
  - Justrizin

---
<span class="z19Dle" id="col-z12bwhgo1sbuy3r0r23ii5nxsqn0e1tzv04"><span class="zo">Check out TabCandy (Watch the video).</p> 

<p>
  <a href="http://www.azarask.in/blog/post/tabcandy/" class="ot-anchor">http://www.azarask.in/blog/post/tabcandy/</a>
</p>

<p>
  Aza Raskin (Author of TabCandy &#8211; His dad started the Macintosh project for Apple)<br /> <br /> <a href="http://en.wikipedia.org/wiki/Aza_Raskin" class="ot-anchor">http://en.wikipedia.org/wiki/Aza_Raskin</a>
</p>

<p>
  This is one of classic example of TRIZ &#8211; Merging principle in actions
</p>

<p>
  Surprising why it took such a long time to come up with such a basic idea
</p>

<p>
  I guess that in general innovation/innovators come up with crude, complex, not-ready <br />mashed up versions of ideas <br />Then it is eventually refined leading to incremental innovations<br /></span></span>
</p>