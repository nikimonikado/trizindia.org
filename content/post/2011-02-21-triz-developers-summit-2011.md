---
title: TRIZ Developers Summit-2011
author: Vladimir Petrov
date: 2011-02-21T16:27:10+00:00
banner: /images/default.jpg
url: /2011/02/triz-developers-summit-2011/
post_views_count:
  - 427
  - 427
categories:
  - Events
  - Justrizin

---
<http://www.triz-summit.ru/en/section.php?docId=4908>

# Information Letter 1

 

<div>
  <span style="color: black; font-size: 12pt;">Information letter #1, November 30, 2010.</span>
</div>

<div>
   
</div>

<div>
  <span style="color: black; font-size: 12pt;">TRIZ Summit 2011</span>
</div>

<div>
  <b> </b>
</div>

<div align="center">
  <span style="color: black; font-size: 12pt;">Dear</span> <span style="color: black; font-size: 12pt;">colleagues</span><span style="color: black; font-size: 12pt;">!</span>
</div>

<div align="center">
   
</div>

<div style="text-align: justify; text-indent: 1cm;">
  <span style="color: black; font-size: 12pt;">1. The Organizing Committee of TRIZ Developers Summit selected the topic for Summit-2011 &#8211; it is &#8220;Action Principle of Systems&#8221;. This topic is very important for TRIZ research. It embraces issues of system modeling, functional analysis and synthesis, trends of engineering systems evolution and methods for forecasting via transfer of functions and trends of such systems, and terminology issues in this field. Practically all TRIZ specialists use this term in their lexicon, but the universally accepted definite interpretation of this term is still missing. We hope that the discussion of these issues during TRIZ Summit will enable to clarify all major issues associated with the topic &#8220;Action Principle of the Systems&#8221;.      </span>
</div>

<div style="text-align: justify; text-indent: 1cm;">
   
</div>

<div style="text-align: justify; text-indent: 1cm;">
  <span style="color: black; font-size: 12pt;">2. You are kindly requested to send the title and short abstract of your article for the collection of papers of TRIZ Summit &#8220;Action Principle of the Systems&#8221; before December 25, 2010. We will do our best to confirm prior to January 1, 2011, which papers will be interesting for TRIZ Summit 2011. We are planning to prepare collections of materials for TRIZ Summit 2011 both in Russian and English.    </span>
</div>

<div style="text-align: justify; text-indent: 1cm;">
   
</div>

<div style="text-align: justify; text-indent: 1cm;">
  <span style="color: black; font-size: 12pt;">3. Based on proposals from participants of previous Summits, the Organizing Committee has taken a decision to change the format of the TRIZ Summit sessions. According to results of analysis of the received materials, 2-3 keynote speakers will be selected, which will be given an opportunity to speak about their materials in greater detail and to discuss them with Summit participants. We will also try to develop the general recommendations on the discussed topic from Summit participants to the entire TRIZ community.   </span>
</div>

<div style="text-align: justify; text-indent: 1cm;">
   
</div>

<div style="text-align: justify; text-indent: 1cm;">
  <span style="color: black; font-size: 12pt;">4. The date of TRIZ Summit 2011 is coordinated with the dates of MATRIZ bi-annual conference &#8220;TRIZfest 2011&#8221; in such a way that participants of TRIZ Summit would have an opportunity to take part in both events. According to the decision of MATRIZ Presidium the TRIZfest 2011 conference will be held in the premises of St.-Petersburg Polytechnic University on July 21-23. The closed session of TRIZ Summit 2011 will take place on July 20, 2011 in St.- Petersburg at Algorithm LLC office. The authors of articles selected for the Collection of Materials will be kindly invited to the closed session.   </span>
</div>

<div style="text-align: justify; text-indent: 1cm;">
   
</div>

<div style="text-align: justify; text-indent: 1cm;">
  <span style="line-height: 115%; font-size: 12pt;">We wish you all the best!</span>
</div>

<div style="text-align: justify; text-indent: 1cm;">
   
</div>

<div style="text-align: justify; text-indent: 1cm;">
  <span style="line-height: 115%; font-size: 12pt;">Organizers and moderators of TRIZ Developers Summit</span>
</div>

<div style="line-height: normal;">
  <span style="color: black; font-size: 12pt;">Alexander Vladimirovich Kudryavtsev</span> <span style="color: #666666; font-size: 12pt;"><a href="mailto:metodolog1@yandex.ru"><font color="#0000FF">metodolog1@yandex.ru</font></a></span> <span style="color: #666666; font-size: 12pt;">           </span>
</div>

<div style="line-height: normal;">
  <span style="color: black; font-size: 12pt;">Semyon Solomonovich Litvin</span> <span style="color: #666666; font-size: 12pt;"><a href="mailto:Simon.Litvin@GEN3partners.com"><font color="#0000FF">Simon.Litvin@GEN3partners.com</font></a></span> <span style="color: #666666; font-size: 12pt;">  </span>
</div>

<div style="line-height: normal;">
  <span style="color: black; font-size: 12pt;">Vladimir Mikhailovich Petrov</span> <span style="color: #666666; font-size: 12pt;"><a href="mailto:vladpetr@netvision.net.il"><font color="#0000FF">vladpetr@netvision.net.il</font></a></span> <span style="color: #666666; font-size: 12pt;">                     </span>
</div>

<div style="line-height: normal;">
  <span style="color: black; font-size: 12pt;">Mikhail Semyonovich Rubin</span> <span style="color: #666666; font-size: 12pt;"><a href="mailto:mik-rubin@yandex.ru"><font color="#0000FF">mik-rubin@yandex.ru</font></a></span><span style="color: #666666; font-size: 12pt;">   </span>
</div>