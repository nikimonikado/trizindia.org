---
title: 'Cloud Computing: Analogy to Automotive'
author: Tushar Kanikdale
date: 2012-11-26T11:00:00+00:00
banner: /images/default.jpg
url: /2012/11/cloud-computing-analogy-to-automotive/
post_views_count:
  - 488
  - 488
categories:
  - Justrizin

---
Read about Cloud Computing and learnt that it is a solution which seperates the function of computing (Heavy Lifting job) and Display. This is like combining / integrating CPU of all the computers to form a Cloud and providing user with only User interface connected to Cloud through internet. This reduces the Cost as user will only pay for services (ex: 3D modelling) being used with hourly based rates. Second , size of devises is reduced as it&#8217;s function is to only display information. Of course this is enabled by high speed internet emergent today. The solution essentially resolves Size vs Cost contradicition for computing industry.  
An analogy to Automotive industry is to have a seperated power plant for all vehicles and power will be  electromagnetically or wirelessely tranferred to vehicles equipeed with transmission system. Electromagnetic power transmission is already a topic of state of the art research going on.  The industry has already taken first step to have electric power with it&#8217;s own power plant (I.C. Engine) as in Hybrid vehicle.

Although  an analogy, the extremes lies in designating solution in computing  as incremental  whereas for automotive a disruptive one!