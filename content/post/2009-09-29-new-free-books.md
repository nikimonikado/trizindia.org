---
title: New (Free) Books
author: Ellen Domb
date: 2009-09-29T18:43:44+00:00
banner: /images/default.jpg
url: /2009/09/new-free-books/
post_views_count:
  - 819
  - 819
categories:
  - Justrizin

---
I just got this announcement from Larry Ball about the latest from his consortium of authors:

_Just wanted to let you know that another book is available on <a href="http://www.opensourcetriz.com" target="_blank">http://www.opensourcetriz.com</a>The name of the book is “Job 4—Simplifying”. It regards tools for simplifying or overhauling a system. The download is also free. A for-pay trade paperback will be available in a couple of weeks._

My advice is that if you don&#8217;t have &#8220;Job 5&#8211;Problem Solving&#8221; you should get both. There&#8217;s heavy, disciplined emphasis on function analysis, then on how to decide which of the options that you discover should be pursued in both books. The &#8220;Simplifying&#8221; book deals with both trimming, in the sense of removing unnecessary, wasteful elements of the system, and the more abstract sense of simplifying by making the system more ideal, ultimately merging with the supersystem.