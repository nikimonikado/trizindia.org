---
title: Forkless Bicycle
author: nikimonikado
date: 2009-11-21T08:10:50+00:00
banner: /images/default.jpg
url: /2009/11/forkless-bicycle/
post_views_count:
  - 356
  - 356
categories:
  - Justrizin
  - Tools

---
This is an excellent invention by a designer. It is a bicycle without the front fork.
  
By removing the assumption that a front fork is necessary he forced himself to think of other ways to control the bicycle.[

<embed src="http://www.youtube.com/v/7qiR7E3dpjk&hl=en_US&fs=1&" type="application/x-shockwave-flash" allowscriptaccess="never" width="425" height="344">
  
</embed>


  
&#8221; target=&#8221;_blank&#8221;>][1]

 [1]: %3Cobject%20width=