---
title: Design for Tranquility
author: Prashant Yeshwant Joglekar
date: 2010-01-12T16:39:18+00:00
banner: blogs/28-TrainCarriageOld.jpg
url: /2010/01/design-for-tranquility/
post_views_count:
  - 340
  - 340
categories:
  - Justrizin

---
<p style="text-align: left;">
  <img src="blogs/28-TrainCarriageOld.jpg" alt="" />
</p>

Mumbai suburban trains are like Bhel Pots. They accommodate different ingredients of human personas making travelling as delicious as Mumbai Bhel. Some of the ingredients of this moving Bhel Pot are gamblers, readers, dozers, chanters, chatters, amateur singers & the grumblers (you can add your own to make it tastier).
  
There are many types of grumblers who spice up this Bhel; one of them is the one who is victim of the falling luggage (“Luggage Slide” similar to a “Landside”). His tranquility gets seriously disturbed by this sudden onslaught. The luggage rack perfectly represents the rest of the compartment as it is stuffed with baggages of fellow passengers. Bags Shuffle as passengers get out and get in the compartment. These bags are like Jack & Jill. First Jack is pulled down (the bag which needs to be pulled) and then Jill tumbles (the other bag which is wedged to the bag being pulled).

The reason for such an occurrence is bad rack design; a bag is pulled with a force applied parallel to the plane of the rack which also pulls the other bag as two bags as said before are wedged together. The other bag is simply pulled down by the gravity and falls over the passenger sitting below. What is the problem and what can be done?

The improved rack design seems to address this. In this design the bags needs to be lifted first before it is removed so the applied force is perpendicular to the plane of the rack (Lift), so even if the other wedged luggage gets moved a bit it rolls up and remain trapped inside.(It has to go up before it can go down, which of course it can’t, I got the picture in an empty train but readers who have travelled in the suburban trains of Mumbai can imagine what is being portrayed above.)

<p style="text-align: left;">
  <img src="blogs/29-TrainCarriageNew.jpg" alt="" />
</p>

I coined a term for this Design “Design for Tranquility”. Now if you are under the old design then be careful, with new you can close your eyes and meditate 