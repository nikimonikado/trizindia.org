---
title: How can vegetable pulao give you ideas on improving a telephone?
author: Murali Loganathan
date: 2011-08-01T11:21:34+00:00
banner: http://www.indianrecipes.co.in/wp-content/uploads/2009/05/Vegetable-Pulao.JPG
url: /2011/08/how-can-vegetable-pulao-give-you-ideas-on-improving-a-telephone/
enclosure:
  - |
    |
        http://www.archive.org/download/HowCanVegetablePulaoGiveYouIdeasOnImprovingATelephone/TrizindiaPodcastMetaphor.mp3
        18661856
        audio/mpeg
        
post_views_count:
  - 332
categories:
  - Podcasts
tags:
  - forced association
  - innovation
  - metaphors
  - pulao
  - triz
  - trizindiapodcast

---
[<img class="alignnone" title="iPhone" src="http://ohgeeky.com/wp-content/uploads/2010/12/iphone-funny-photo.jpg" alt="" width="445" height="296" /><img class="alignnone" title="Veg Pulao" src="http://www.indianrecipes.co.in/wp-content/uploads/2009/05/Vegetable-Pulao.JPG" alt="" width="351" height="280" />][1]

[TRIZ India Podcast Metaphor][1]

Intrigued? Listen to the podcast to find neat tips and tricks to generate ideas based on metaphors. 4 innovation facilitators, Murali Loganathan, Shankar Venugopal, Prakasan Kappoth and Bala Ramadurai, use a pencil as a metaphor to generate ideas to solve a worldwide problem.

 [1]: http://www.archive.org/download/HowCanVegetablePulaoGiveYouIdeasOnImprovingATelephone/TrizindiaPodcastMetaphor.mp3