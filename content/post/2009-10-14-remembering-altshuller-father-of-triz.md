---
title: Remembering Altshuller – Father of TRIZ
author: Prashant Yeshwant Joglekar
date: 2009-10-14T20:58:26+00:00
banner: blogs/37-Altshuller.gif
url: /2009/10/remembering-altshuller-father-of-triz/
post_views_count:
  - 459
  - 459
categories:
  - Justrizin

---
<p style="text-align: left;">
  <img src="blogs/37-Altshuller.gif" alt="" />
</p>

I was introduced to TRIZ 4 years back, when I attended Darrell Mann’s course at IIT Mumbai. It was a great feeling at the conclusion of a two day course and I had a great satisfaction to have been introduced to something different which seems to have disrupted my learning curve. I was fortunate that I have been asked to further develop on it by my seniors at Mahindra and I started voraciously mining the literature on the subject. Anyhow, more on this, little later.
  
TRIZ is a Russian acronym for “Theory of Inventive Problem Solving” (the original Russian version is “Teoriya Resheniya Izobreatatelskikh Zadatch”). The science has origin in erstwhile Soviet Union and is fathered by the Great Genrich Altshuller, a Russian of Jewish origin.

Altshuller was born on this day, 15th October in the year 1926. As a child he was very curious. He received his first author’s certificate for an underwater diving apparatus while a student in the 9th grade; in the 10th grade he built a boat having a rocket engine that used carbide as fuel. In the year 1946, he developed his first mature invention, a method for escaping from an immobile submarine without driving gear. This invention was immediately classified as military secret and he was soon employed with the navy in the patent’s department.

While reviewing the patents filed from various fields of invention, it was revealed to him that many inventors have solved the same problem using some common principles irrespective of the field of their invention. He wished, only if these inventors would have communicated with each other they would have arrived at the solution in the lesser time and with lesser number of trials. So the basic premise on which the whole science based was “Someone somewhere has solved problems like yours”. To move towards the possible solutions one needs to formulate his specific problem into a generic one to search for a generic solution to be converted later into a specific one.

He concluded that innovation & creativity can be learned, taught and eagerly decided to write a letter to Stalin. To his surprise he was arrested in the year1950 on the false charge of sabotaging inventor’s idea (By talking about TRIZ) and was sentenced for 25 years of imprisonment. During this period he was interrogated without allowing him of any sleep. He solved this problem by defining a physical contradiction as “I want to Sleep and I can not sleep” (Not allowed to sleep, but instead was allowed to sit on a chair), he made smart use of resource and used the paper of cigarette carton, tore it in two halves and with a charred match, drew a pupil on each one, further spitting on it & sticking it on to his closed eyes, so while he was asleep, the attendant would think otherwise. This way he could get some sleep during the day and was fresh for the trial at night. The judge running the trial used to get astonished to see him fresh during interrogation but has never known the real reason. 

While in the prison Altshuller used to discuss his ideas of TRIZ with fellow inmates’ who were scientist, academicians, and researchers. Soon after Stalin’s death he was released in the year 1956.

Central Theme of TRIZ Research

Altshuller was concerned about the trial and error methods of innovation, he argued that lot many resources are wasted in trials instead he suggested to have more mind trials by thinking systematically & working with tools to arrive at the successful innovative solutions, of course to be experimented later.

Any system evolves towards increasing IDEALITY (the central of concept of TRIZ, a solution having all the benefits with no cost and harm) and innovator while achieving this fate is faced with several contradictions. A technical contradiction is recognized when improvement in one parameter results in worsening of the other and a physical contradiction is recognized when system itself wants to have two different contradicting parameters in it. Solving a contradiction means rejecting a compromise and moving towards an IDEAL SOLUTION. The research has revealed several approaches towards solving a contradiction. I will briefly touch upon four areas of focus

Contradiction Matrix and Inventive Principles

Altshuller gathered team of researchers and started his work on TRIZ; the team mentored by him analyzed 200,000 patents to decode the common themes, known as 40 inventive principles which different innovators used to solve around 48 sets of different contradictions they encountered while aiming for an IDEAL solution. This provided a good level of abstraction hinting potential inventors towards possible solution directions. In practice, to solve a pair of contradiction, on an average 4 set of principles are used for further ideation.

Substance Field Analysis

His research further involved developing a substance field models. Some TRIZ evangelists use this tool extensively in their work. The model is based on a simple theory that for any system to be viable in order to deliver a function there needs to be minimum two substances and at least one interacting field between them. The solution strategies then are broadly classified into 4 parts .1) Solve the problem by completing an incomplete S-Field. 2) Solve the problem by modifying one or more of the existing substances or fields. 3) Solve the problem by adding new substances, fields or combination thereof or 4) Solve the problem by transitioning to super or sub-system.

The first step of representing a system is to draw a function attribute model and identify the harmful, effective, ineffective, excessive or missing functions. The set of 76 standard solutions developed by the TRIZ researchers then help the potential innovators to classify the problem at hand in one of the above situations and look at the solutions others innovators used solving similar problem. After successful application the new example can be tabled in one of these standards to enhance solutions data base.

The substance field analysis is also developed for psychological applications with emotions being the field acting between two substances ( humans ) it is amazing sometimes to find striking similarity between the technical system and a non-technical one.

Trends of Evolution

Another approach that he & his team developed was the trends of evolution wherein they observed technical system evolution more closely ( and more recently business system evolution) and captured them in the form of trend directions which can be applied to different levels of system hierarchy that exist in the product & system build up. Each of these trend lines solves a problem by offering a solution direction and moving a system towards increasing ideality. They are the best starting point to the know where the current system is and will possible head to.

Researchers also developed many psychological tools for a potential inventor to think and break the mental inertia to explore right innovative solutions in quick time.

Function & Knowledge Data Base

W. Brian Arthur in his book “The Nature of Technology” discusses evolution of technology. The first step in the evolution begins with understanding of the basic phenomenon which lay at the core of any technology. Functional and Knowledge Data Base of TRIZ identifies such a phenomenon (effects) . The data base has classified these effects in two categories first of which is relevant when we are trying to improve the function (for e.g. Absorb, Bend, Dries, Heat, Clean etc) better than it is being currently delivered. Second type concerns with improvement in the attribute delivery (e.g. Change in Pressure, Force. Density etc) associated with a part of the system. This systematic innovation tool quite often hints innovators to look at such a data base to solve the current problem.

TRIZ in the Globalised Business Word

It is a known fact that many successful western companies work with TRIZ but are not willing to share their experiences. What is good and beneficial is not generally shared in fear of losing the competitive edge. Science of TRIZ has been further enhanced by some of the western researchers like Darrell Mann to solve business problems in addition to their dedicated technical research and at present though the science seems to be growing in pockets but is surely growing as more & more companies have started realizing the importance of Innovation in order to survive in bad times & organically grow in favorable conditions. Only one company which openly admits use of TRIZ as its formula of success is SAMSUNG. It is known that companies like GE, Intel, Boeing, Dell and even TOYOTA have successfully applied TRIZ in their products, process and for reinventing their business models. In India attempts are made to learn and apply this science by companies like Infosys, Thermax, WIPRO, Mind tree & few others. There are many TRIZ enthusiasts in India and the community is steadily but surely growing.

TRIZ in Indian Context

India is a country where we think that we have billions of problems; instead we need to think that we have billion opportunities. India requires extreme innovations therefore TRIZ is very relevant IDEA for India. In the process our Great country can offer Indian TRIZ learners (I am not using the word researchers) solutions which can be catalogued in the existing data base to make Indian minds & solutions more enticing to the world.

Future Challenges with TRIZ

TRIZ is in a very nascent stage in India. Indian industry is still in Lean, Six Sigma Mindset while these two methods are the best for waste elimination and optimization; they seldom offer any solution strategies for systematic solution generation. In short there is no point in improving SLR camera when the world is already using a digital one. The pace with which the world is moving continuous improvement initiatives is the thing of the past and continuous innovation is the thing of the future.

TRIZ researchers, practitioners & enthusiasts need to propagate this science in a friendlier manner not tying any belts (Yellow, Green & Black) around the neck of the potential innovators thus drawing others away from this wonderful science. We all need to be recognizing the fact that every potential innovator will have a role to play in enriching this science making it “Theory of Everything”, in the unfolding future.

The best way to learn TRIZ is to reverse engineer the solutions and recognize a problem  Of course this is my view as a student, experts are welcome to correct us, guide us.

Relevance to the Indian Corporate World

Two simple points of relevance

1. Developing a common language of Innovation (Just like we Indians Communicate in HINDI irrespective of our mother tongue, innovators communicate with the world) solving contradictions and staying one contradiction ahead of competition.

2. Building Organizational Memory in order to capture, archive & retrieve ideas at faster rate to cross breed them.

Last Words

I conclude with a quote offering my tribute for the MASTER. I quote “The Soul & his theory we know now is very unique, not because he developed an amazing science, but he bestowed it upon the world & in return never asked “Give Me”, but said “Take It”.

On behalf of all the TRIZ students I bow before the MASTER and seek his blessings for each one of us to play our part in taking this wonderful innovation science to as many people and organizations as possible.

TRIZ HO!!!!!!!!!!!!!!!!!!!!!!