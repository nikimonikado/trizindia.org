---
title: Split Brain theory
author: Bala Ramadurai
date: 2010-03-31T04:40:35+00:00
banner: /images/default.jpg
url: /2010/03/split-brain-theory/
post_views_count:
  - 457
  - 457
categories:
  - Justrizin

---
<span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 17px;">For the naysayers, here is the <a href="http://nobelprize.org/educational_games/medicine/split-brain/background.html">link</a> that illustrates the split brain theory. This theory won Roger Sperry a Nobel Prize. such is the power of the split brain theory.</span>

<span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 17px;"><em>What is the split brain theory ?</em></span>

<span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 17px;">There are 2 distinct functions in the brain &#8211; logical and creative. Roger Sperry&#8217;s experiments prove that logical (or commonly left hemisphere of the brain. For south paws, it is interchanged) thinking is associated with verbal, analytical, linguistic abilities. Logical thinking is also linear, organized, deductive and inductive. Whereas, the creative part of the brain (or commonly right brain) is associated with non-verbal (body language), synthetic, non-linear, random, emotional abilities. Corpus Callosum is the bridge between the two hemispheres and is the super highway transmitting information back and forth. All the subjects that Roger Sperry worked with, had their corpus callosum severed due to fits and other complicated symptoms.</span>

<span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 17px;">Now that you have a background, here is my equation</span>

<span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 17px;"><em>Z (t) = n(t)X + m(t)Y &#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;..(1)</em></span>

<span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 17px;">where <em>Z</em> is the total cognitive ability at one point in time. <em>X</em> is 100% logical and <em>Y</em> is 100% illogical or creative.</span>

<span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 17px;"><em>n(t)</em> represents the fraction of logical ability and <em>m(t)</em> represents the fraction of creative ability, where <em>t</em> is time.</span>

<span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 17px;"><em>n(t) + m(t) = 1 &#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;(2)</em></span>

<span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 17px;">The assumptions in here are that there is no person who&#8217;s gifted with 100% logical abilities or 100% creative abilities. The implication of the equation is that there is always a mix of the 2 abilities at any given instant of time. This, of course, is an empirical equation and needs experimental proof. When moving to experimentation, <em>n(t)X</em> can be the amount of neural activity in the logical part of the brain and m(t)Y can be the amount of neural activity in the creative part of the brain. since equation (2) is imposed, individual values for X and Y can be found and plotted as a daily chart or an hourly chart. This can further be mapped on to what kind of activities, a person is involved in.</span>

<span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 17px;">The situations in which these measurements need to be carried out are</span>

<span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 17px;">(a) brainstorming</span>

<span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 17px;">(b) grunt work</span>

<span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 17px;">(c) day dreaming</span>

<span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 17px;">(d) watching TV</span>

<span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 17px;">(e) sports<br /></span>

<span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 17px;">(f) solving sudoku</span>

<span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 17px;">(g) humming a tune</span>

<span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 17px;">(h) face to face interactions with a person, whom you like, whom you detest</span>

<span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial, Verdana, sans-serif; font-size: 17px;">(i) addressing an audience of 100 people</span>