---
title: Why don’t we innovate?
author: Sajeev VK
date: 2009-10-16T09:21:55+00:00
banner: /images/default.jpg
url: /2009/10/why-dont-we-innovate/
post_views_count:
  - 337
  - 337
categories:
  - Justrizin

---
The &#8216;we&#8217; in the question refers to the Indian IT industry.

I know there are people out there who would find this question offensive; but frankly that&#8217;s not the intention.

We had someone coming over to present a Capability Maturity model for innovation at our office. It had a staged representation, similar to CMM, which organizations could adopt to become more innovative. True, lot of effort was put in to that. I have also seen many other tools and techniques that promise to make innovative individuals and organizations. But most of the time, there is this uncomfortable feeling I have – _NO. Something missing_.

As the sessions ended last week, I was left thinking – _will such models / tools / frameworks help Indian IT companies to Innovate_. That lead to a more basic question – _why aren’t we innovative?_

Basic questions lead to basic answers and when the answer came, it was simple. In most of the Indian IT companies, people worked on products that they never get to use themselves.(I am just talking of product innovation here…)For e.g. they wrote application for a hotel reservation system, but never would use it as a customer. Or they wrote code for an expensive automobile which they will never drive. They write applications to process insurance claims, but never would use it themselves. **And I think this is the most important condition for innovation – you need to feel, experience and connect to what you are trying to innovate on** (and it is not to do with people not being innovative as we would like to believe. Try selling the same fellow an insurance policy, he will tear you apart with questions and other possibilities). In some cases, our engineers are separated from the end user by multiple intermediaries. So, what we are talking about as product innovation in service projects is trying to mimic something that would naturally occur to someone who is using it.
  
The scenario is changing. One possibility is the increasing presence in the domestic market (oh! Not Defense / ISRO projects, but some products that people can feel and use) and the other is more personal exposure to the markets that we cater to (Possibly this is the reason behind the strange phenomenon – Indians are more creative in US than in India..)

Once this happens (given the direction economies are moving, it wouldn’t be long), I think people will just become more innovative naturally. So should we wait or should do we need to create more processes / tools / models for innovation?