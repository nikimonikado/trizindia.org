---
title: How does a superstar plan his career moves?
author: Murali Loganathan
date: 2011-08-01T11:24:01+00:00
banner: http://www.planetradiocity.com/home/images/mr_news/rajinikanth-robot_1042011103371.jpg
url: /2011/08/how-does-a-superstar-plan-his-career-moves-2/
enclosure:
  - |
    |
        http://www.archive.org/download/HowDoesASuperstarPlanHisCareerMoves/TrizindiaPodcast9windows.mp3
        17426528
        audio/mpeg
        
post_views_count:
  - 334
categories:
  - Podcasts
tags:
  - 9-windows
  - atm
  - business
  - customer
  - india
  - innovation
  - money
  - rajinikanth
  - triz
  - trizindiapodcast

---
&nbsp;

&nbsp;

&nbsp;

&nbsp;

[<img class="alignnone" title="Superstar Rajinikanth" src="http://www.planetradiocity.com/home/images/mr_news/rajinikanth-robot_1042011103371.jpg" alt="" width="495" height="260" />][1]

[Triz India Podcast 9 windows][1]

Bala Ramadurai, Murali Loganathan, Prakasan Kappoth and Shankar Venugopal discuss &#8220;thinking out of the box&#8221; and &#8220;big picture thinking&#8221; citing examples of BMW, software development, dispensing cash on the third podcast of the series.

 [1]: http://www.archive.org/download/HowDoesASuperstarPlanHisCareerMoves/TrizindiaPodcast9windows.mp3