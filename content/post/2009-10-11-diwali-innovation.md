---
title: Diwali Innovation
author: Prashant Yeshwant Joglekar
date: 2009-10-11T15:55:23+00:00
banner: blogs/38-Lanterns.jpg
url: /2009/10/diwali-innovation/
post_views_count:
  - 258
  - 258
categories:
  - Justrizin

---
<p style="text-align: left;">
  <img src="blogs/38-Lanterns.jpg" alt="" />
</p>

Many times we come across unique artefacts, incidences, appreciate innovativeness of it at that very moment, but loose it like sea sand afterwards, not capturing its essence. Such incidences many times serve as “Innovation Boosters”, but they lay hidden deep in our pattern oriented brain and require external stimulus to bring it to the surface again. TRIZ provides a good external stimulus by way of analogy to excavate this hidden treasure.
  
I may have come across many such artefacts, incidences but I could capture essence of only few, usually such sparks are as natural as our breath, sometimes we don’t even notice them. While I was shopping for Diwali, I came across one such artefact.

I decorate door & window frames with hanging lanterns during Diwali, so I buy at least dozen of them every year. When I enquired its price it was Rs.8, I thought it was just the right price and was about to buy, but my wife bargained hard for Rs. 5, the vendor of course did not agree. I thought Rs. 5 was too much to ask for, so I kept myself away from their bargaining session. If you are holding branded shirt bag in your hand, which costs Rs.1500 + then why to waste time & energy for such a small amount, I thought. But bargaining wherever you can is a favourite past time & ingrained quality of a woman. The discussion of course failed. I was bit unhappy as it means I will not able to decorate the way I planned.

After a while, we went on to another street for shopping and saw two boys selling same lanterns. I was in no mood to enquire the price as I thought it would be Rs. 8 which means futile bargaining again. To my surprise, the price was Rs. 5; I was really taken a back. I thought the vendor must be in SALE SALE SALE Mode……

But no he was not, why was this difference, the decorative paper was the same, the size of the lantern was the same (may be little bigger) everything was the same, then where he was accounting his price differential?. After I carefully saw the lantern, I spotted a very smart idea, innovation. The vendor has used plastic tea, coffee cups from inside, around which he had decorated with a paper to make an attractive lantern. I saluted the vendor for his idea. He used cheap, easily available resource and has also done good functional thinking while designing his lantern.

In a labour intensive activity like lantern making in this case, substitution by way of a cheaper, already available material can improve productivity and reduce labour cost.(Which is a dominant factor in such cases), therefore I think the innovation needs applause.

Market conditions and frugal, value seeking customers (like my wife and in general women) put innovators to task and then innovative mind blossoms with SMART Innovations. The vendor must have sold such lanterns in numbers and have made good overall profit.

I had double benefit as a customer; I saved some money and also got an idea for my TRIZ , Creativity workshop.

I conclude with three points

1. I feel innovator in every one of us needs to be like foam, which can absorb ideas & retain them, not like a hard stone (expert syndrome) which never absorbs anything in it.

2. If CK Prahalada says Future Lies at the Bottom of the Pyramid, what I think he means is Innovations lies at the bottom of the Pyramid, because Innovation is Future.

3. Let us be like a honeybee in spotting innovations and share that honey & its sweetness with everyone, like the way I did.