---
title: Gazing into the Crystal Ball using TRIZ technology trends part 2
author: Murali Loganathan
date: 2012-11-08T04:50:42+00:00
banner: /images/default.jpg
url: /2012/11/gazing-into-the-crystal-ball-using-triz-technology-trends-part-2/
tagazine-media:
  - 'a:7:{s:7:"primary";s:0:"";s:6:"images";a:0:{}s:6:"videos";a:0:{}s:11:"image_count";i:0;s:6:"author";s:7:"2227365";s:7:"blog_id";s:8:"25726430";s:9:"mod_stamp";s:19:"2012-11-08 04:50:42";}'
post_views_count:
  - 378
categories:
  - Podcasts
  - Videos
tags:
  - business
  - evolution
  - innovation
  - method
  - trends
  - triz
  - trizindiapodcast
  - video

---
[youtube http://www.youtube.com/watch?v=ay8eSNrp9I4&w=560&h=315]

From our [last podcast discussing a few trends from TRIZ][1] portfolio system evolution, we are continuing this time with a few more trends and an evolution from podcast to video. Only thing we probably missed was using the full size Casio keyboard that was right next to us for some extra sound effects.

Hope you enjoy it.

 [1]: http://trizindia.wordpress.com/2012/10/13/gazing-into-the-crystal-ball-using-triz-technology-trends-2/