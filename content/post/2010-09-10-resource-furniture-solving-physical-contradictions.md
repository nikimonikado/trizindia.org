---
title: Resource Furniture – Solving Physical Contradictions?
author: Prakasan K
date: 2010-09-10T03:38:25+00:00
banner: /images/default.jpg
url: /2010/09/resource-furniture-solving-physical-contradictions/
post_views_count:
  - 1466
  - 1466
categories:
  - Justrizin

---
<div>
  The concept isn&#8217;t new. The IKEA of world has done it a while back, and the classic TRIZ examples dated in 1960 has examples of same from Japan. But, this video is an interesting one, shows different options, and apart from all, the beauty associated with it, and for us, good examples of TRIZ techniques.
</div>

<div>
</div>

<div>
  Physical Contradictions are considered as closest point of a completely new invention according to TRIZ. Once we identify the physical contradictions, a new paradigm of innovation is emerged. Here in this case, the contradiction at high level can be defined ad, want vs don&#8217;t want, or need that huge table vs don&#8217;t need that huge table. The solution is then segment in time and in space. Watch the video in the Video section
</div>

<div>
</div>

<div>
</div>

<div>
</div>