---
title: Search Party is over for Google? A TRIZ Analsysis
author: Prakasan K
date: 2010-09-27T19:02:05+00:00
banner: /images/default.jpg
url: /2010/09/search-party-is-over-for-google-a-triz-analsysis/
post_views_count:
  - 432
  - 432
categories:
  - Justrizin

---
Cross posting from [TRIZ Journal Commentary][1]

<div>
</div>

<div>
  <p style="margin-top: 4px;">
    <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">I posted a <a href="http://www.triz-journal.com/commentary/archive/evolution_of_browsers_and_google_chrome_trizing_it.html" target="_blank" style="color: rgb(0, 0, 153); text-decoration: none;"></a><a href="http://www.triz-journal.com/commentary/archive/evolution_of_browsers_and_google_chrome_trizing_it.html">commentary on search engine and IFR</a> of a search engine here a while back; as search engine has found a place in my TRIZ examples, I thought of giving a shot to this news article (<a href="http://tech.fortune.cnn.com/2010/07/29/google-the-search-party-is-over/">Google: The party is over</a>) appeared in Fortune recently through another TRIZ interpretation. While I wouldn&#8217;t state that Google is struggling to find the next innovation, statement like slowing down their core business of search is certainly intriguing me especially when some data points are given by the authors from their research.</span>
  </p>
  
  <p style="margin-top: 4px;">
    <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">What might have been going wrong with them, a very innovative company like Google? While failure like <a href="http://www.zdnet.com/blog/open-source/lessons-from-google-wave-failure/7025" target="_blank" style="color: rgb(0, 0, 153); text-decoration: none;"></a><a href="http://http://www.zdnet.com/blog/open-source/lessons-from-google-wave-failure/7025">Google Wave</a> is not a measurement to say they are going down, (In fact we all know that in the journey of innovation, you must fail), getting stuck with a conundrum of &#8220;search using Google is not the future&#8221; is a matter of concern especially if you generate 91% profit through search.</span>
  </p>
  
  <p style="margin-top: 4px;">
    <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">If we can do a simple 9-Windows thinking, considering Search as the system-present, can we see a subtle evolution happening there in the super-system, beyond technology evolution (If it is only technology, Google will certainly catch up) and business model innovation? As said in the article, if 5 years ago, we want a shoe, we will simply Google it, but now I just need to &#8220;tweet&#8221; or add it in the &#8220;what&#8217;s on my mind&#8221; in FaceBook, and you get answers with &#8220;human touch&#8221;..</span>
  </p>
  
  <p style="margin-top: 4px;">
    <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">The technology enabling these systems (Twitter and FB) aren&#8217;t great innovation according to me as in the search engine algorithm of Google; then what makes the search engine is not the future for Google? One element I see is the social system evolution as the game changer. What I mean the social system is the ability to have &#8220;emotion&#8221; in what you are doing like search. It is far better feeling to ask your friend or relative than a machine about something and get answers based on reference-able experiences, and those changes, enabled by platforms like Twitter and FaceBook is the new way in future. Now, could Google have come up with this earlier?</span>
  </p>
  
  <p style="margin-top: 4px;">
    <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">Perhaps yes if they have applied TRIZ trends 🙂</span>
  </p>
  
  <p style="margin-top: 4px;">
    <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">Well, the trend is that Search engine is transitioning to the super-system not because the current system has reached to its limit, but a newer system (like social system) is emerging faster and is almost outdated the current system. In a scenario like this, what a company like Google should perhaps try to do is to look at the way how in future people will communicate to each other with emotions.</span>
  </p>
  
  <p style="margin-top: 4px;">
    <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">Apart from the &#8220;Transition to super-system trend&#8221;, we can look at other trends like &#8220;Increasing degree of ideality&#8221; (Try applying this in the super-system) too.</span>
  </p>
  
  <p style="margin-top: 4px;">
    <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: Arial; -webkit-border-horizontal-spacing: 2px; -webkit-border-vertical-spacing: 2px;">What else do you think Google can apply and come out with the next best search? Remember the &#8220;Resources&#8221; are aplenty for a company like Google.</span>
  </p>
</div>

 [1]: http://www.triz-journal.com/commentary/prakasankappoth