---
title: TRIZ India podcast
author: Bala Ramadurai
date: 2011-07-04T06:31:50+00:00
banner: /images/default.jpg
url: /2011/07/triz-india-podcast/
post_views_count:
  - 346
  - 346
categories:
  - Justrizin
  - Videos

---
The inside story of TRIZ India podcast was published by Ellen Domb in her <a target="_blank" href="http://www.triz-journal.com/commentary/ellendomb">commentary</a>. 

 

&#8220;_Podcast was one of those ideas which came to me when I was stuck in Bangalore traffic and I thought this is the best resource to listen to podcasts. So, I listened to Getting Things Done podcasts streaming from my cellphone to the car stereo. One fine day, it hit me that TRIZ should have a podcast of our own. Instead of dismissing the idea, I wrote it down at the next light 🙂 next action &#8211; talk to Murali to find out if he was interested and then Shankar signed up. Murali couldnt make it, since he had to be in the other MindTree Office and Shankar said his daughters weren&#8217;t doing well. I was hell-bent on recording the podcasts that very day or it would become one more of ideas which never lifted off. (Time = 0 in Size-time-cost, I guess)._

_&#8220;Murali quickly booked an audio bridge and we had Shankar dial in as well. We did a trial run on my cellphone (which doubled up as a digital recorder). It seemed to play my voice well, but drowned Shankar&#8217;s and Murali&#8217;s voices. Now, optimization time and I had the parameters perfect. One hitch though, since this was universality at play, my cellphone went off and disrupted the recording. Learning for next time, change the mode of the phone to Offline before starting to record._

_&#8220;Again, my condition was simple for post-production &#8211; only one step to upload, no intermediate steps. Due to the cut in the recording, Murali had to struggle with formats and freeware, finally he managed to merge the 2 files. What format do we use to upload? wordpress wanted mp3, but some size limitation. Facebook wanted mp4, only 20 minutes, tumblr, youtube, we tried them all, none worked. Prakash to the rescue, podbean was the instant suggestion and turned out just fine. Prakash also managed to embed the player on trizindia.org.&#8221;_

 

Since this is a social network, we would like inputs from you on topics for podcasts.

 

Please suggest topics that you would like discussed. If we have not explored those topics ourselves (Shankar, Murali, Prakash and I), we will reach out to experts to get those topics covered. Better still, if you are THAT expert, please let us know if we can reach out to you for that topic.

 

Suggested topics:

1. 9-windows

2. Reversal of Assumptions (TRIZ principle: Other Way Around)

3. What is TRIZ? Basic overview of TRIZ

4. Po and IFR comparison (Ashok from trizindia.org)

 

What other suggestions to make the podcasts better?

1. Transcripts for all podcasts (work in progress)

2. Examples cited (We did this better in the 2nd podcast, scheduled to release on July 12, 2011)

3. Demo of a tool