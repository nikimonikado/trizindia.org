---
title: 'TRIZ India podcast #3 – 9-Windows'
author: Murali Loganathan
date: 2011-08-06T10:29:02+00:00
banner: 'http://api.ning.com/files/QHO-ed-E7o7hVvXZqsxL8oAJP3bjqfU83m8vCdRcpIqa4k6GuSf2jZnNURdjD8OPixqeezeZAIObFyCK*ajt1mkYcew3upw0/9windows.JPG?width=750'
url: /2011/08/triz-india-podcast-3-9-windows-2/
post_views_count:
  - 335
categories:
  - Podcasts
tags:
  - Podcasts
  - trizindiapodcast

---
# TRIZ India podcast #3 &#8211; 9-Windows

  * <a>Posted by </a>Bala Ramadurai<a> on August 6, 2011 at 6:45am</a>

Bala Ramadurai, Murali Loganathan, Prakasan Kappoth and Shankar Venugopal discuss &#8220;thinking out of the box&#8221; and &#8220;big picture thinking&#8221; citing examples of BMW, software development, dispensing cash on the third podcast of the series.

&nbsp;

<span style="font-size:medium;"><a href="http://api.ning.com/files/QHO-ed-E7o7hVvXZqsxL8oAJP3bjqfU83m8vCdRcpIqa4k6GuSf2jZnNURdjD8OPixqeezeZAIObFyCK*ajt1mkYcew3upw0/9windows.JPG" target="_self"><img alt="" src="http://api.ning.com/files/QHO-ed-E7o7hVvXZqsxL8oAJP3bjqfU83m8vCdRcpIqa4k6GuSf2jZnNURdjD8OPixqeezeZAIObFyCK*ajt1mkYcew3upw0/9windows.JPG?width=750" width="750" /></a></span>

&nbsp;