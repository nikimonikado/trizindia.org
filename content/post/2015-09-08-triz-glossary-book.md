---
title: TRIZ Glossary Book
author: nikimonikado
date: 2015-09-08T07:20:08+00:00
banner: https://gallery.mailchimp.com/a4fe36e0021432bc3bc87a8ab/images/3137fb77-1256-4fe9-ba81-63a440ef31f2.png
url: /2015/09/triz-glossary-book/
post_views_count:
  - 411
categories:
  - Justrizin

---
# <img class=" aligncenter" src="https://gallery.mailchimp.com/a4fe36e0021432bc3bc87a8ab/images/3137fb77-1256-4fe9-ba81-63a440ef31f2.png" alt="" width="181" height="286" align="none" data-cke-saved-src="https://gallery.mailchimp.com/a4fe36e0021432bc3bc87a8ab/images/3137fb77-1256-4fe9-ba81-63a440ef31f2.png" /> {.null}

TRIZ Glossary book written by Prof. Vladimir Petrov is now available at an introductory price of USD 0.99 in the link given below. Prof. Petrov&#8217;s linkedin profile is here &#8211; <a href="https://il.linkedin.com/pub/vladimir-petrov-triz/6/30a/700" target="_blank" data-cke-saved-href="https://il.linkedin.com/pub/vladimir-petrov-triz/6/30a/700">https://il.linkedin.com/pub/vladimir-petrov-triz/6/30a/700</a>

The link to the book is given here &#8211;

<a href="http://www.amazon.in/TRIZ-Glossary-Vladimir-Petrov-ebook/dp/B013YE59M2/ref=sr_1_1?s=digital-text&ie=UTF8&qid=1441695997&sr=1-1" target="_blank" data-cke-saved-href="http://www.amazon.in/TRIZ-Glossary-Vladimir-Petrov-ebook/dp/B013YE59M2/ref=sr_1_1?s=digital-text&ie=UTF8&qid=1441695997&sr=1-1">http://www.amazon.in/TRIZ-Glossary-Vladimir-Petrov-ebook/dp/B013YE59M2/ref=sr_1_1?s=digital-text&ie=UTF8&qid=1441695997&sr=1-1</a>