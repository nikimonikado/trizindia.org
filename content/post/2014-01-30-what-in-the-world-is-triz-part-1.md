---
title: What in the world is TRIZ? Part 1
author: nikimonikado
date: 2014-01-30T09:13:44+00:00
banner: http://www.triz.co.uk/images/where_ideas_come_from...trizsmall.jpg
url: /2014/01/what-in-the-world-is-triz-part-1/
enclosure:
  - |
    |
        http://www.archive.org/download/WhatInTheWorldIsTrizPart1/TRIZIndia-WhatintheworldisTRIZ-part1.mp3
        161
        audio/mpeg
        
  - |
    |
        http://www.archive.org/download/WhatInTheWorldIsTrizPart1/TRIZIndia-WhatintheworldisTRIZ-part1.mp3
        161
        audio/mpeg
        
post_views_count:
  - 614
  - 614
categories:
  - Justrizin
tags:
  - podcasts

---
![where_ideas_come_from...trizsmall.jpg][1]
  
_Image Courtesy: [triz.co.uk][2]_

What is TRIZ? What does it take to start learning TRIZ and master the method? Listen to 3 people &#8211; Dr. Ellen Domb, Prakasan Kappoth and Bala Ramadurai, talk about the basics of the method. <http://www.trizindia.org>



[Listen to the podcast here][3]

 [1]: http://www.triz.co.uk/images/where_ideas_come_from...trizsmall.jpg
 [2]: http://triz.co.uk
 [3]: http://www.archive.org/download/WhatInTheWorldIsTrizPart1/TRIZIndia-WhatintheworldisTRIZ-part1.mp3