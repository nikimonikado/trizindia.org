---
title: TRIZ Solving Pet Problems
author: Prashant Yeshwant Joglekar
date: 2009-09-30T17:19:37+00:00
banner: /images/default.jpg
url: /2009/09/triz-solving-pet-problems/
post_views_count:
  - 271
  - 271
categories:
  - Justrizin
  - Tools

---
Those who own a pet, especially a dog will appreciate with what I am saying. It’s a demanding job to take your pet out for his routine every morning. He may be feeling relieved watering few electric poles but his “street” smart buddies do not take pleasure in his act & consider it an offense committed in their own terrain. They envy cozy life your pet enjoys and therefore start barking endlessly at him and indirectly at you; after all, you are the reason for his comfort. (Much the same way, you abhor your colleague who according to you is a blue eyed boy of your boss )
  
On my jogging track, I have this gentleman who strolls with his pet quite comfortably. Initially he must have had two choices when encountered with such a difficult circumstance either fight or flight. He did neither of these things but still tamed them. I see TRIZ connection is his act. 
  
In terms of Contradiction Matrix parameter, the problem he is facing can be categorized as “Harmful Factors (Barking Street Dogs) Affecting the System ( his pet, he himself & other joggers )” As per TRIZ inventive principles this factor is improved ( reduced I mean) by using three Inventive Principles ( Ideas) : 2 – Taking Out or Extraction, 10 – Prior Action, 35 – Change of Parameters
  
These principles can be applied to own dog, street dogs, he himself and many other system elements (Such elements can be found by going up and down the problem hierarchy explorer)
  
Let us apply these principles and see what solutions we can generate to find a solution to this problem.
  
Principle 2 Extraction: Separate interfering part or property from a system or object, or single out the only necessary part
  
Ideas: Take the street dogs out from the place by informing municipality (Too Ideal Solution)
  
Have a separate toilet for your dog. (Everybody likes the dog, but not his toilet, there are lots of public places so why to have a separate toilet inside Hmmm……)
  
What you really want a dog or his ability to bark & scare thieves, extract that ability and get a doorbell that has a barking sound.
  
Make the dog comfortable with his routine by taking him around in your (un) co-operative society premises. ( Where are there no street dogs) Of course, you will very soon will be thrown out of the society … Neighbors consider your dog as a headache. ( Neighbors headache owner’s pride ).
  
Leave the dog alone on the street while you extract yourself and sit in a car watching the dog show 
  
Use your prowess and get the zone declared as “NO STREET DOG ZONE” (Difficult for the street dogs to follow, they can’t read. Hmmmm…)
  
Think some more & list ….
  
Principle 10: Prior Action
  
• Perform the required change of a system or object ( either fully or partially) before it is needed
  
• Pre-arrange elements such that they can come into action from the most convenient place and without losing time for their delivery
  
Ideas
  
Take the stick or stone to scare the dog and drive them out (Easy to imagine difficult to execute)
  
Take the spray that defunct the street dogs temporarily (Be aware of some animal lovers they will defunct you, they love animals more than they love human beings.)
  
Take something that changes emotional parameters (Read the idea under the principle 35 parameter changes)
  
Make the dog infertile
  
Principle 35: Parameter Changes
  
• Change object’s physical state ( e.g. from physical to virtual)
  
• Change the concentration
  
• Change the degree of flexibility
  
• Change emotional and other parameters
  
Ideas:
  
Why you want a real dog get a virtual dog that keeps you busy with everything as the real dog except the harmful things. Nintendo has such toys where the user take care of this virtual dog much the same way you take care of the real dog. (http://www.lucylearns.com/nintendo-dog.html) In case you don’t follow his routine it dies. ( electronic circuit cease to function after a set number of defaults)
  
Shut the street dog’s mouth by a mask so they cannot bite and bark, here you are reducing the noise decibels (concentration) by introducing a silencer (Difficult, who will bell the cat? Dog I mean…)
  
You & your dog wear a mask or a suite so that street dogs can only bark which you will able to ignore, the danger is they will think you & your dog are extra-terrestrial and may bark even more
  
Change the emotional parameters; if you always thought that stick, stone are the best ways to keep the street dogs away then I am sure in reality you haven’t found this to be the best way. Can you then feed them with some food? Just reverse the emotions from hatred to compassion, love. Yes, this seems to be a better option. This is precisely the idea this gentleman uses. Study has shown that street dogs become violent / bark because they are hungry and therefore can be dangerous.
  
This man gathers all the dogs in one corner & feeds them with biscuits, all the other dog lovers also synchronies their timing with his so they and their dog escape the wrath and enjoy their peaceful routine Uncle is doing a great social service
  
Now, let’s get serious and ponder on few things
  
There is a business opportunity hidden in every problem. It can be unearthed by careful questioning thus going beyond the present solution and problem.
  
In this case by asking a question “Why Do I Want To Solve This Problem”? Leads us to think whether “taking the dog out “is a right problem or “having a dog itself” is a problem. Both these questions lead us to problems of household security or one’s emotional engagement beyond human associations. (Sometime we are comfortable with any living thing except the human, it’s all because of our complex neo-cortex you see) These clear or subtle needs sprouts business ideas like burglar alarms, home security systems or need for a virtual pet (toy) which can be accompanied anywhere ( without being literally carried I mean) and can be cared during moments of loneliness. Nintendo may have picked up business opportunity for its virtual toys with such insights.
  
The morale
  
1. All our social innovations need to address inclusiveness. What applies to dogs applies to human also.

2. Observe carefully and you will find many business opportunities. Your product may not still stop people having a pet, but will provide solutions to those thinking of owning one because of an unmet need.