---
title: Innovation Internship using TRIZ
author: Akashdeep Howladar
date: 2010-04-12T14:25:40+00:00
banner: /images/default.jpg
url: /2010/04/innovation-internship-using-triz/
post_views_count:
  - 390
  - 390
categories:
  - Justrizin

---
<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;"><span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;">Dear All,</span></span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;"><br /></span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;">I am now pursuing <span class="Apple-style-span" style="font-family: 'Times New Roman'; font-size: 16px;">‘<span class="Apple-style-span" style="font-size: 13px; font-family: arial, sans-serif;">Advanced Master in Innovative Design’ (AMID), at INSA-Strasbourg, France. Whole program thrust is totally on Invention Creation and Innovation Management in Company using TRIZ & OTSM. The following is broad description of program. The course is delivered by 12 outstanding TRIZ & OTSM experts. It has 413 hours formal Instruction. Prior to this I had excellent introduction on TRIZ in India by several TRIZ experts. I will be able to pick up any TRIZ based software very quickly.</span></span></span>
</div>

<div>
</div>

<div>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;"><font class="Apple-style-span" face="'Times New Roman'"><span lang="FR" style="font-family: 'Times New Roman';" xml:lang="FR"><span class="Apple-style-span" style="font-size: small;">MODULE 1: PROCESS INNOVATION (composed of 4 sub modules) [35 H]</span></span></font></span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;"><font class="Apple-style-span" face="'Times New Roman'"><span lang="FR" style="font-family: 'Times New Roman';" xml:lang="FR"><span class="Apple-style-span" style="font-size: small;">MODULE 2: THE LAW OF INDUSTRIAL PROPERTY [35 H]</span></span></font></span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;"><font class="Apple-style-span" face="'Times New Roman'"><span lang="FR" style="font-family: 'Times New Roman';" xml:lang="FR"><span class="Apple-style-span" style="font-size: small;">MODULE 3: MANAGEMENT OF THE PROJECT TEAM & COMMUNICATION [35 H]</span></span></font></span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;"><font class="Apple-style-span" face="'Times New Roman'"><span lang="FR" style="font-family: 'Times New Roman';" xml:lang="FR"><span class="Apple-style-span" style="font-size: small;">MODULE 4: ANALYSIS OF THE BACKGROUND [35 H]</span></span></font></span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;"><font class="Apple-style-span" face="'Times New Roman'"><span lang="FR" style="font-family: 'Times New Roman';" xml:lang="FR"><span class="Apple-style-span" style="font-size: small;">MODULE 5: CONTROL OF THEORETICAL FOUNDATIONS AND PRINCIPLES OF THE TRIZ [35 H]</span></span></font></span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;"><font class="Apple-style-span" face="'Times New Roman'"><span lang="FR" style="font-family: 'Times New Roman';" xml:lang="FR"><span class="Apple-style-span" style="font-size: small;">MODULE 6: PRACTICAL TOOLS AND METHODS FOR THE TRIZ [35 H]</span></span></font></span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;"><font class="Apple-style-span" face="'Times New Roman'"><span lang="FR" style="font-family: 'Times New Roman';" xml:lang="FR"><span class="Apple-style-span" style="font-size: small;">MODULE 7: THEORY AND PRACTICE OF ARIZ [35 H]</span></span></font></span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;"><font class="Apple-style-span" face="'Times New Roman'"><span lang="FR" style="font-family: 'Times New Roman';" xml:lang="FR"><span class="Apple-style-span" style="font-size: small;">MODULE 8: ADVANCED PRACTICE OF ARIZ</span></span></font></span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;"><font class="Apple-style-span" face="'Times New Roman'"><span lang="FR" style="font-family: 'Times New Roman';" xml:lang="FR"><span class="Apple-style-span" style="font-size: small;">MODULE 9: META-COGNITIVE APPROACH TO THE DESIGN [35 H]</span></span></font></span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;"><font class="Apple-style-span" face="'Times New Roman'"><span lang="FR" style="font-family: 'Times New Roman';" xml:lang="FR"><span class="Apple-style-span" style="font-size: small;">MODULE 10: OPENING OF SEMINARS [35 H]</span></span></font></span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;"><font class="Apple-style-span" face="'Times New Roman'"><span lang="FR" style="font-family: 'Times New Roman';" xml:lang="FR"><span class="Apple-style-span" style="font-size: small;">MODULE 11: UNIT PROJECT [35 H]</span></span></font></span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;"><font class="Apple-style-span" face="'Times New Roman'"><span lang="FR" style="font-family: 'Times New Roman';" xml:lang="FR"><span class="Apple-style-span" style="font-size: small;">MODULE 12: BUSINESS PROJECT [28 H]</span></span></font></span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;"><br /><span><strong>Innovative design project</strong></span></span></p> 
  
  <div style="text-align: justify;">
    Minimum 4 months.
  </div>
  
  <div style="text-align: justify;">
  </div>
  
  <div style="text-align: justify;">
    It is required to complete internship on TRIZ application on live problems for the award of Degree. I request you for internship in your or your client&#8217;s company. I will sign nondisclosure agreement and honour my commitment of confidentiality.I will share my newly acquired knowledge also. I will work with the differnt domain experts and all the required guidance from INSA-Strasbourg TRIZ experts turned Professors. I am available July-Sept (10weeks) and December, 2010 onwards. Regards,
  </div>
  
  <div style="text-align: justify;">
  </div>
  
  <div style="text-align: justify;">
    <span class="Apple-style-span" style="font-family: 'Times New Roman'; font-size: 16px;"><span style="mso-spacerun:yes"><span class="Apple-style-span" style="font-size: 13px; font-family: arial, sans-serif;">Akashdeep Howladar</span></span></span>
  </div>
  
  <div style="text-align: justify;">
    <span class="Apple-style-span" style="font-family: 'Times New Roman'; font-size: 16px;"><span style="mso-spacerun:yes"><span class="Apple-style-span" style="font-size: 13px; font-family: arial, sans-serif;">Innovation Department (AMID), INSA- Strasbourg, 24 boulevard de la Victoire, 67084 Strasbourg Cedex, France,</span></span></span>
  </div>
  
  <div style="text-align: justify;">
    <span class="Apple-style-span" style="font-family: 'Times New Roman'; font-size: 16px;"><span style="mso-spacerun:yes"><span class="Apple-style-span" style="font-size: 13px; font-family: arial, sans-serif;">Mobile (France) : 0033-640171090, Mobile(India) : 0091-9410390528,</span></span></span>
  </div>
  
  <div style="text-align: justify;">
    <span class="Apple-style-span" style="font-family: 'Times New Roman'; font-size: 16px;"><span style="mso-spacerun:yes"><span class="Apple-style-span" style="font-size: 13px; font-family: arial, sans-serif;">Skype: akashdeep_triz, email id:</span></span> <a href="mailto:akashhowladar@gmail.com">akashhowladar@gmail.com</a></span>
  </div>
</div>