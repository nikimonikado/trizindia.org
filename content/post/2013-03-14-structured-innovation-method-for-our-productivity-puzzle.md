---
title: Structured Innovation Method for our Productivity Puzzle
author: Rajesh Kumar R.
date: 2013-03-14T18:30:00+00:00
banner: /images/default.jpg
url: /2013/03/structured-innovation-method-for-our-productivity-puzzle/
post_views_count:
  - 660
  - 660
categories:
  - Justrizin

---
We have been engaged in a java based development project where the development process is well defined and stream lined. We had already optimized the development process to a fairly optimal level and automated some key areas of development that gave us a good productivity boost.

However the productivity levels achieved with all the process optimization and the automations we adopted were still not sufficient to meet the deadlines, we would be behind schedule by nearly a month if the same productivity was carried forward. The team believed that they have done all possible things and there is no further room for any optimizations and even if there is room it will still be only incremental. But our management team believed that we are not leveraging sufficient re-use opportunities and we must explore possibilities of re-use.

We decided to do a structured innovation exercise to find out if we come up with any ideas that will give us significant productivity improvement. We told ourselves that we are going do the exercise without any pre-conceived opinions and just follow the method

Here is the method that we followed:

**Step-1:**

We started with everyone in the room sharing their Ideal Final Result (IFR) and eventually we narrowed down that the IFR that we wanted to achieve was to bring about 3 fold increase in development productivity.

**Step-2:**

We went on deeper into the problem identification by process decomposition and finding out the development step that took the maximum amount of time, essentially we had an 8 step development process and identified the slowest step

**Step-3:**

Once we identified the specific problem area, we narrowed down the root cause of the problem by repeatedly asking why that problem is occurring or in other words ‘what is preventing’ us to solve the problem and we kept drilling it down until we could not go any further, and eventually came down to a specific step that was slowing down the integration time significantly.

Also we explored the broader problem that we are solving by asking ourselves ‘why do we want to solve’ and established the deadline goals for the project delivery

**The outcome:**

Once the problem was narrowed down to the specific step that is the cause of the problem, the solution to solve it came out naturally from the team and it resulted in us introducing a specific additional process prior to initiating the development that would bring down the integration effort multifold. So eventually it turned out that the re-use was not really the cause of the core productivity issue but the delay in integration.

**Some inventive principles applied**:

After we defined the process step we continued further to explore opportunities for further improvement by applying the inventive principles and here are some principles we applied

**Prior Action**: We introduced a process to be carried out prior to the development that will address the integration delay

**Asymmetry**: Currently our development teams were equally divided to make progress into development, after the exercise we decided to form a team that is smaller than the development team to operationalize the ‘Prior Action’ that needs to be done so that it does not come into the critical path of the development cycle. There by we introduced asymmetry to the team distribution

**Taking out**: Once we have done all the exercise we just glanced at our 8 step development process and asked ourselves can we ‘take out’ any of them and reduce the number of steps and it turned out that indeed we can take out a step and completely automate it, which will further save our overall development time

 

**Finally&#8230;**

we all left the room with a sense of satisfaction that the 90 minutes that we spent on this exercise was worth it, as we truly believe that it will save days if not weeks worth of productivity gains