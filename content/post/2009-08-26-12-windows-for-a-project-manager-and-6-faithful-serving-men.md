---
title: 12 Windows for a Project Manager and 6 Faithful Serving Men
author: Prakasan K
date: 2009-08-26T13:00:00+00:00
banner: /images/default.jpg
url: /2009/08/12-windows-for-a-project-manager-and-6-faithful-serving-men/
post_views_count:
  - 221
  - 221
categories:
  - Justrizin

---
I was attending a workshop on Project Management (yes you heard it right). I wanted to brush up my PM knowledge after working as a “consultant” for few years, and also look at the new dimension of effective PM using systematic thinking methods, especially simple TRIZ tools. I set my expectation before attending this program that the Project Management Body of Knowledge would have embraced something different to say the PM BOK (Body of Knowledge) is evolving, and the session is going to teach me something new, such as using techniques for effective Project Planning. Unfortunately, it was nowhere close to my expectation.

In the last couple of years we have witnessed the increasing complexity of uncertainty in the existence of a business, collective social knowledge for a consumer to change the business decisions of a company within the span of a dusk and a dawn, and finally how has it impacted the people delivering innovative solution, questioning their own “existence” on a project in the helm of these external developments. Naturally, these super-system developments have the undulate effect in a Project Management activity, and without doing something new, I think Project Management will still be the most challenging activity with a success ratio ticking to the top of % charts&#8230;.

<a href="http://trizindia.ning.com/group/trizforit/forum/topics/12-windows-for-a-project" target="_blank">Read the complete blog here in the TRIZ for IT Group</a>