---
title: Human Intellect Evolution
author: Tushar Kanikdale
date: 2012-02-24T07:00:00+00:00
banner: blogs/9-HIE.PNG
url: /2012/02/human-intellect-evolution/
post_views_count:
  - 335
  - 335
categories:
  - Justrizin

---
<a href="blogs/8-HIE.PNG" target="_self"><img class="align-center" src="blogs/9-HIE.PNG" width="457" /></a>We are seeing ever increasing complexity in today’s life. What comes to help are the principles and theories which combines millions observation into single discipline It’s like this if I know law of gravity then I don’t have to study and monitor curvilinear path for every projectile motion on the earth. I can simply draw it using laws of motion.

TRIZ principles are serving the same purpose where we don’t need to reinvent the wheel everytime we need innovations. However every theory has it’s limitation and are most beneficial for given context (Newton’s law not applicable for quantum particles moving with speed of light). Evolution of human Intellect happens when theories and principles are discovered but that&#8217;s not enough. The key is to fall in love of ideas to learn and develop theories. But to graduate and grow to next level we need to detach from what is created by us as depicted in the image. This is equally applicable for organisations.  The products, technology, organisation structures needs to be phased out even when they were created with enthusiasm and proven successful too. Evolution lies in creative destruction.