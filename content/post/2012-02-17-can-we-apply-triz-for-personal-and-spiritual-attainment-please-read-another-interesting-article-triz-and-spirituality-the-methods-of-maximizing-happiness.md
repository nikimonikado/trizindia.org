---
title: 'Can we apply TRIZ for personal and spiritual attainment, please read another interesting article “TRIZ and Spirituality: The methods of Maximizing Happiness”'
author: Umakant Mishra
date: 2012-02-17T06:22:38+00:00
banner: /images/default.jpg
url: /2012/02/can-we-apply-triz-for-personal-and-spiritual-attainment-please-read-another-interesting-article-triz-and-spirituality-the-methods-of-maximizing-happiness/
post_views_count:
  - 260
  - 260
categories:
  - Justrizin

---
TRIZ is a method of high level of commonsense and can be applied in any field, whether social, economical, technical or spiritual. This article analyzes the concept of happiness, the ultimate goal of human being, and demonstrates the application of different TRIZ concepts like Ideality, contradictions, Principles, Resources and Trends in different theories of maximizing happiness. please read the aticle at <a target="blank" href="http://www.linkedin.com/redirect?url=http%3A%2F%2Fwww%2Etriz-forum%2Etk%2Fblog%2Fview%2F48%2Ftriz-and-spirituality-the-methods-of-maximizing-happiness&urlhash=9QpD&_t=tracking_anet" rel="nofollow">http://www.triz-forum.tk/blog/view/48/triz-and-spirituality-the-methods-of-maximizing-happiness</a> and post your comments..