---
title: 8 TRIZ evolution trends applied to Mobile
author: Murali Loganathan
date: 2012-10-03T04:57:14+00:00
banner: /images/default.jpg
url: /2012/10/8-triz-evolution-trends-applied-to-mobile/
post_views_count:
  - 711
  - 711
categories:
  - Trends

---
Cross posting from <http://trizindia.wordpress.com/2012/10/01/8-triz-evolution-trends-applied-to-mobile/>

This was a long pending post, originally to be delivered as a pod cast, but we never got around to doing it. The core idea was to take a commonly used system like a smart phone, apply the 8 evolution trends of TRIZ and exemplify them. For a brief introduction on the trends go [here][1], I would love to see other resources from the readers as well. I am not using any particular order as is normally used for explaining the trends.

**Law of uneven development**pertains to how parts of the system evolve at different paces. In smartphone case Gorilla glass was actually developed for non-phone purposes around 1970, and the substrates were developed around 1980, LEDs that could use these came only during 1990s, while a successful phone with these elements came together for the first time with iPhone only in 2005. Another software example that I can think of is applications on smartphones have a few weeks or even faster release cycles, while the OS and system components evolve at a slower pace typically half-yearly release cycles.

**Transition to Super system**is visible across many functions that smartphones are performing now ranging from alarm clock, radio, social networking, playing, browsing among so many others. These were in reality individual sub system functions of users and all of them unified in one device at the super system level viz. the phone itself.

**Transition to microlevel**is another trend that has been going on at many levels in the phone, most common example is the per second billing instead of the per 3 minutes billing. It can also be seen in control functions, earlier there was only one setting across the phone, now we have settings at a micro level be it at screen level (lock, home screens), profiles and context, application level settings, among many others. This is a clear transition to micro level.

**Law of completeness** is one of the simplest to understand, and it has 4 components, as applied to our example  
ENGINE: the component that delivers the necessary energy, in the case of smart phone it is easier to think of these are needs at personal level to communicate, share, capture etc  
TRANSMISSION: the component that transmits energy from the engine to the working unit, and these are usage, connections of various kinds, networks in case of the smart phone.  
WORKING UNIT: this actually performs the actions intended, in case of smart phone it will be the phone, camera, screen, applications that deliver necessary functions.  
CONTROL: controlling the above elements at different levels of settings, customizations, profiles etc are considered here. It has to touch at least one of the elements above.

**Law of Conduction** on a smart phone is to be understood as movement of data (voice, or others) through a medium (be it USB, wifi, blue tooth, GSM etc)

**Law of Harmonization**is one that necessitates a rhythm and priority for actions from the system, in case of a smart phone you can easily imagine multi-functions happening at the same time like call + SMS with the same carrier, charging + data transfer through one USB connection, background sync processes with social and address books with the same network

**Law of Ideality**can be applied in case of smartphone to many different levels starting from what is ideal for the user, what is ideal for the manufacturer (say one assembly line for all the models), carrier can have their own ideal results (like transfer ability across network types), or Operating system specific (say zero system resource usage)

**Law of increasing su field degrees **I did not find an interesting example to share here,

I would welcome the community to feel free in comment and possibly podcast what they feel will be interesting to other members.

 [1]: http://www.triz-journal.com/archives/2002/03/b/index.htm