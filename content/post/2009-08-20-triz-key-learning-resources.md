---
title: TRIZ – key learning resources
author: Dr Shankar MV
date: 2009-08-20T07:17:36+00:00
banner: /images/default.jpg
url: /2009/08/triz-key-learning-resources/
post_views_count:
  - 343
  - 343
categories:
  - Justrizin

---
Are you new to TRIZ &#8211; are you wondering where to begin &#8211; here are a few resources that you could look at:

TRIZ Books
  
Altshuller, Genrich (1973). Innovation Algorithm. Worcester, MA: Technical Innovation Center. ISBN 0-9640740-2-8.
  
Altshuller, Genrich (1984). Creativity as an Exact Science. New York, NY: Gordon & Breach. ISBN 0-677-21230-5.
  
Altshuller, Genrich (1994). And Suddenly the Inventor Appeared. translated by Lev Shulyak. Worcester, MA: Technical Innovation Center. ISBN 0-9640740-1-X.

TRIZ Journal : http://www.triz-journal.com/ &#8211; excellent articles &#8211; archives from 1996 available

TRIZ Opensource: http://www.opensourcetriz.com/ &#8211; Excellent collection of TRIZ problem solving illustrations & case studies, eBooks &#8211; free downloads

Other TRIZ Resources
  
The Altshuller Institute for TRIZ Studies http://www.aitriz.org/index.php?option=com_frontpage&Itemid=1
  
TRIZ site &#8211; http://www.trizsite.com/startup/default.asp?menuno=999001TM
  
TRIZ-learning platform http://triz.it/eng/ &#8211; good collection of examples of the 40 Inventive principles
  
TRIZ Overview articles: http://en.wikipedia.org/wiki/TRIZ, http://www.mazur.net/triz/

If you have come across other useful resources, please share the info with our community.