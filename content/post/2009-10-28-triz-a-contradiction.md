---
title: TRIZ – A Contradiction
author: Kumar Rajamani
date: 2009-10-28T04:33:50+00:00
banner: /images/default.jpg
url: /2009/10/triz-a-contradiction/
post_views_count:
  - 361
  - 361
categories:
  - Justrizin

---
I was seeing through some of the slides that Shankar had so kindly shared and also the Videos that Prakasan had shared about Altshuller giving sessions on TRIZ

It set me thinking on one strange line of thought, that even TRIZ was A contradiction

The contradiction being that
  
We want Breakthrough innovations and making
  
it systematic and reachable and doable to everyone just makes the final
  
idea that is generated not &#8220;WOW&#8221; enough or thrilling enough

Putting it in another perspective, suppose that there are several industry competitors
  
in a consumer domain market, and if each of the company uses TRIZ and all
  
eventually lead to similar solutions (because if you use structured approach solutions
  
cant be that varied) then where is the differentiator

Is the differentiator only spotting the contradiction itself?

TRIZ indeed is a powerful tool, and I dont in any way deny it.

But, In my current small view of things, TRIZ itself is a contradiction
  
and we can try to use TRIZ to address it,

Probably there is more to innovation that TRIZ alone

Kumar