---
title: IFR at Google
author: Murali Loganathan
date: 2010-09-09T08:17:31+00:00
banner: /images/default.jpg
url: /2010/09/ifr-at-google/
post_views_count:
  - 1503
  - 1503
categories:
  - Justrizin

---
<div>
  Thought I will share this snippet about IFR from Google.
</div>

<div>
  Isak Bukhman while he was in Bangalore for the TRIZIN conference, declared that IFR has nothing Ideal or Final about it and I would add, it is possibly not a result of something as well.
</div>

<div>
</div>

Google operates this website called <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial; font-size: small; line-height: 16px;"><a href="http://www.dataliberation.org/">http://www.dataliberation.org/</a></span>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial; font-size: small; line-height: 16px;">&#8220;<i>with the singular goal to make it easier for users to move their data in and out of Google products</i>&#8220;</span></p> 
  
  <div>
    <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial; font-size: small; line-height: 16px;"><br /></span>
  </div>
  
  <div>
    <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial; font-size: small; line-height: 16px;">Here is the IFR as described in the site, quoting</span>
  </div>
  
  <div>
    <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial; font-size: small; line-height: 16px;"><br /></span>
  </div>
  
  <blockquote class="webkit-indent-blockquote" style="margin: 0 0 0 40px; border: none; padding: 0px;">
    <blockquote class="webkit-indent-blockquote" style="margin: 0 0 0 40px; border: none; padding: 0px;">
      <div>
        <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial; font-size: small; line-height: 16px;"><span class="Apple-style-span" style="font-size: 13px;"><i>People usually don&#8217;t look to see if they can get their data out of a product until they decide one day that they want to leave For this reason, we always encourage people to ask these three questions</i> <b><i>before</i></b> <i>starting to use a product that will store their data:</i></span></span>
      </div>
    </blockquote>
  </blockquote>
  
  <div>
    <div>
      <div style="list-style-type: decimal; margin-left: 4em">
        <ol>
          <li>
            <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial; font-size: small; line-height: 16px;"><i>Can I get my data out at all?</i></span>
          </li>
          <li>
            <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial; font-size: small; line-height: 16px;"><i>How much is it going to cost to get my data out?</i></span>
          </li>
          <li>
            <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial; font-size: small; line-height: 16px;"><i>How much of my time is it going to take to get my data out?</i></span>
          </li>
        </ol>
      </div>
    </div>
  </div>
  
  <blockquote class="webkit-indent-blockquote" style="margin: 0 0 0 40px; border: none; padding: 0px;">
    <blockquote class="webkit-indent-blockquote" style="margin: 0 0 0 40px; border: none; padding: 0px;">
      <div>
        <div>
          <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial; font-size: small; line-height: 16px;"><i>The ideal answers to these questions are:</i></span>
        </div>
      </div>
    </blockquote>
  </blockquote>
  
  <div>
    <div>
      <div style="list-style-type: decimal; margin-left: 4em">
        <ol>
          <li>
            <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial; font-size: small; line-height: 16px;"><i>Yes.</i></span>
          </li>
          <li>
            <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial; font-size: small; line-height: 16px;"><i>Nothing more than I&#8217;m already paying.</i></span>
          </li>
          <li>
            <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial; font-size: small; line-height: 16px;"><i>As little as possible.</i></span>
          </li>
        </ol>
      </div>
    </div>
  </div>
</div>

<blockquote class="webkit-indent-blockquote" style="margin: 0 0 0 40px; border: none; padding: 0px;">
  <blockquote class="webkit-indent-blockquote" style="margin: 0 0 0 40px; border: none; padding: 0px;">
    <div>
      <div>
        <div>
          <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial; font-size: small; line-height: 16px;"><i>There shouldn&#8217;t be an additional charge to export your data. Beyond that, if it takes you many hours to get your data out, it&#8217;s almost as bad as not being able to get your data out at all</i></span>
        </div>
      </div>
    </div>
  </blockquote>
</blockquote>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial; line-height: 16px;"><br /></span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial; line-height: 16px;">Note here that the &#8220;final&#8221; state of the evolved product is not defined as one thing but more as a process and thus covering the intermediates and without losing sense of the direction. That is a more real purpose of IFR.</span>
</div>

<div>
  <font class="Apple-style-span" color="#000000" face="arial"><span class="Apple-style-span" style="line-height: 16px;"><br /></span></font>
</div>

<div>
  <font class="Apple-style-span" color="#000000" face="arial"><span class="Apple-style-span" style="line-height: 16px;"><br /></span></font></p> 
  
  <blockquote class="webkit-indent-blockquote" style="margin: 0 0 0 40px; border: none; padding: 0px;">
    <blockquote class="webkit-indent-blockquote" style="margin: 0 0 0 40px; border: none; padding: 0px;">
      <div>
        <div>
          <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial; font-size: small; line-height: 16px;"><br /></span>
        </div>
      </div>
    </blockquote>
  </blockquote>
</div>