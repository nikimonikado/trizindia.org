---
title: How to get kids off mobile screens - application of inventive methodology - 0.2 and 0.3
author: murali
date: 2017-09-20T06:24:40+05:30
pubdate: 2017-09-20T06:26:40+05:30
draft: false
banner: /img/banners/children-1931189_640.jpg
categories:
  - Tools
tags:
  - evolution
  - system
  - trends
  - triz
  - ariz
  - mobile
  - gaming
  - children
  - addiction
  - ariz85c
  - systematic innovation
  - bypass
  
---

*[Murali Loganathan](https://www.linkedin.com/in/muralidharanl/) and [Bala Ramadurai](https://www.linkedin.com/in/balaramadurai/) have been working on this project for a few weeks now with two objectives in mind-* 

1. *Learn ARIZ*
2. *To solve the "kids spending too much time with the mobile" problem.*

*This post is part of a [series](/tags/ariz85c/)*

<!--more-->

*This piece was already published here - https://trizindia.wordpress.com/2017/06/17/part-0-step-0-2-investigate-a-by-pass-approach/*

By-pass approach is a necessary step, following system operator, it was originally not part of the 85C but brought back from 85A considering its value in examining the problem more closely.

Purpose: The idea of applying the by-pass approach is to determine at which level you want to solve the problem, i.e. in the system operator we have already identified super system elements and sub system elements, we will now reformulate the original problem at each of these levels. This leads to the 2 steps in the By pass approach that in turn will lead to reformulations that are even better than the original problem

Viewpoint: Solver or parent in our case

**Step 1: For the given system reformulate the original problem at the level of the super system**

**Step 2: For the given system reformulate the original problem at the level of the subsystem**

Because most problems these days are complex and have social, technological, behavioral and environmental contexts, we find the Karl Ulrich problem hierarchy very useful http://opim.wharton.upenn.edu/~ulrich/designbook.html (Page 36)

For our specific problem, at super system level, we can now reformulate below as how and why questions across the different contexts. After finishing super system repeat the same at subsystem level and reformulate the problem.
Subsystem from System Operator from our previous exercise includes Device(mobile OS), Eyes, Thumb, Mind, App, Wifi

<img src="/img/karl-ulrich.png" class="img-responsive" />

1. How to make every social (friends and school)  / technology interaction episode a learning experience?
2. Why? Level 5 (Social) – In what way might we make them belong to a specific group/clan?
3. Why? Level 4 (Social) – In what way might we make them heroes/cool guys in their friends circle? (Reason for contact with friends)
4. Why? Level 4 (Tech) – In what way might we control the nature of the apps that are downloaded?
5. Why? Level 4 (Tech) – In what way might we help rate games based on psychological metrics like stickiness (frequency of launch and updates), average install period, average daily play time, and allow selection of games? (beyond content and rating currently available in app store)
6. Why? Level 3 (Social) – In what way might we make them masters of their “art”/games? (Lack of challenge)
7. Why? Level 3 (Tech) – In what way might we include play time as experience gained?
8. Why? Level 2 (Social) – In what way might we engage the kids so that they don’t get bored?
9. How might I increase their exposure to other games/movies/themes?
10. How can the family engage in the games and increase the game time?
11. Why? Level 2 (Tech) – In what way might we reduce internet bandwidth for the device?
12. Why? Level 1 (Social) – In what way might we reduce access to mobile devices?
13. Why? Level 1 (Tech) – In what way might we reduce run time of mobile devices/app? In what way might we get the screen time reduced for children using a mobile device?
14. How? Level 1 (Tech) – How might we reduce the game engagement time in the app?
15. Level 1 (Tech) – How might we reduce the screen-on time on the mobile device?
16. Level 1 (Tech) – How might we reduce the continuous WiFi on time?
17. Level 1 (Env) – How might I reduce strain on eyes?
18. Level 1 (Env) – How can I reduce OCD because of the habit of playing (multi-tasking)?
19. Level 1 (Env) – How can I increase involvement in slow time frame activities?
20. Level 1 (Env) – How might I increase monotony of every level, so that it takes longer to complete the level, with not much interest?
21. Level 1 (Env) – How might I get the school to certify a set of games that are allowed?
22. Level 1 (Env) – How can we reduce the number of apps available from the app store?
23. Level 1 (Env) – How can I make the free games download to zero, and give kids allowance to buy their own games?
24. How might I reduce screen time based on subsystem elements below?
25. How might increase time spent on each level of the game?
26. How might increase the access to many such similar games?
27. How might I give access to “Customers who played this game also played this game” options?

**Next step is 0.3** - Determine which problem, the original or the bypass, makes the most sense to solve. Here a distinction between mini-problem (where we cannot change much in the system elements and their characteristics) or maxi-problem (where almost anything can be changed) is useful, this will make sure you don’t break laws of physics. Also very useful is to think about the potential evolution paths both objectively and subjectively and develop your view on the problem level of choice.

> As a general rule, while selecting the level, I prefer going back to the user of the ultimate solution to see if solving at a chosen level makes sense. In this case of children using disproportionate amount of time on internet and digital devices, I simply asked my son what his preferred solution level was by asking the questions. Of  course curtailing wifi received a “NO”, but his choice was

> **Why? Level 4 (Tech) – In what way might we control the nature of the apps that are downloaded?**

> As a solver now we have to rethink if we can actually build such a system, if yes do we have the necessary capability and resources (time, money, others) and we can make a choice.

Looks like we are getting clearer understanding from this reformulation, in our next post we will look at **Part 0 Step 0.4** 

> Determine the required quantitative characteristics
