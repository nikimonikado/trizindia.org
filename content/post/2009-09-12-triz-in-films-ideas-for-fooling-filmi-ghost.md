---
title: TRIZ In Films – Ideas for Fooling “FILMI GHOST”
author: Prashant Yeshwant Joglekar
date: 2009-09-12T15:55:17+00:00
banner: /images/default.jpg
url: /2009/09/triz-in-films-ideas-for-fooling-filmi-ghost/
post_views_count:
  - 458
  - 458
categories:
  - Justrizin

---
Traveling 4 ½ hrs sitting on the last seat of a 25 seater bus, cramped & clamped is a nightmare. Some of you may empathize with me. My Mumbai- Nasik Return journey perfectly fits this case. In order to dampen this torture, smart transporters, like the one we have here, provide video facility & play latest hits. In TRIZ terms this is called innovation by transition to a super system (Video, films, latest hits etc). The smart innovator asks simple question, can I solve this problem by transition to super system and provides Video facility which acts like a pain killer, provided he plays good movies, otherwise, it results in pain multiplication, of course such incidences presents you with a chance to develop a robust, balanced mind, so consider them as blessings in disguise.

Recently on my return journey from Nasik, my torture subsided as the bus driver played Bollywood’s hit “Bhool Bhulaiyya”. The theme was not that great, but presentation was. (Something like….a good presentation on a lousy topic). I am not going into the details of the story, but to sum up in short, the story goes like this.

From America to his hometowns in the interiors of India, it was a long journey for Siddhartha. But nothing could match the mind boggling journey that now lay in front of him. For it was a journey into an astounding maze in which each step meant mystery, discovery, surprise, shock & revelation.

With open arms, his large extended family welcomes the US based Siddhartha & his newly wed Avani when they come home. But what family resists is Siddhartha’s insistence on staying in his royal ancestral mansion during his stay. The mansion as believed by many held a deadly secret that had repeatedly destroyed family of generations.

The scientifically inclined Siddhartha & his equally modern, archeological expert wife do not believe in such stories and decide to stay put there. Unforeseen happenings, mysterious elements, horrific indications and life threatening incidents swarm their path. Siddhartha calls his dynamic doctor friend Aditya to do root cause analysis & find a permanent solution.

The ancestral property is a haunted mansion. Years ago, a Bengali dancer Manjulica, had committed suicide in the palace, when an evil minded King not able to endure her love for her dance partner beheads him in front of her. Majolica not able to bear the shock commits suicide. Her ghosts (let us say some form of energy) stay in the mansion waiting to find a suitable instrument (someone’s mind & body) to occupy & take revenge abiding law of conservation of energy. So let us say a form of energy that stays locked (static) waiting to get converted into some form of actionable energy.

The story goes further with Avani, who had spent her childhood with her grandmother and away from her parents, who were settled in the US. She grows up listening fairly tales, ghost stories from her grandma. One day suddenly her father decides to take her to the US against her wishes, disturbing her own little world. She stays in a state of shock & helplessness, shrouding agony deep inside her.

The negative energy (extreme agony) of Manjulica fuses with the negative energy of Avani. Avani secretly starts playing Manjulica and nobody understands WHY? May be she finds the closest match to her anguish. Manjulica in her also assumes a long haired handsome man as her lost love in the past birth & considers Siddhartha, her husband in this birth, as the KING who beheaded her love and therefore wants to kill him to take her revenge.

Now you must be wondering why I have taken you so long, have I become a film columnist. No I am not at the moment, in future only knows. What I found at this stage was contradiction. The contradiction was that Manjulica in Avani wants to kill Siddhartha, after which she would leave Avni’s body. Although Avani will be normal afterwards, she would have lost her husband in the present birth. Therefore the contradiction is she wants to kill her husband and don’t want to kill her husband. In TRIZ this is called a physical contradiction in time and the general questions that one asks in formulating them are

When do I want condition (+ A ) ? and When I do not want condition (-A) ?

i.e. I want to kill Siddhartha the KING when I am Manjulica and don’t want to kill him when I am Avani.

The Inventive principles for such a situation are

Principle 15 : Dynamics: If an object or system is rigid or inflexible, make it movable or adaptable

Principle 10 : Preliminary Action : Pre-arrange objects or systems such that they can come into action at the most convenient time & place.

Principle 11 : Beforehand Cushioning: Introduce emergency backups to compensate for the potentially low reliability of an object. E.g. Air Bags or Back-up parachute.

Principle 26 : Copying : Use simple and inexpressible copies in place of expensive, possibly vulnerable objects or systems. e.g. Imitation Jewellery

The scene goes something like this the real Siddhartha is brought on a stretcher mounted on a drum. When Manjulica’s GHOST lifts the sword, the drum is rotated (Principle 15) and the effigy (Principle 26) which is mounted on the other side of the drum (Principle 10, 11) beforehand comes in front. Manjulica cuts the effigy with her sword with all her locked “REVENGE” energy and thus Ghost of Manjulica after taking revenge leaves Avani’s body forever.

TRIZ theory of everything. TRIZ HO!!!!!!!!!!!!!!!!