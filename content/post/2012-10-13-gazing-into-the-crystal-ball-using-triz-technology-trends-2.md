---
title: Gazing into the Crystal Ball using TRIZ technology trends
author: Bala Ramadurai
date: 2012-10-13T21:40:39+00:00
banner: http://cdnimg.visualizeus.com/thumbs/40/6f/art,ball,black,and,white,crystal,light,planet-406f5e2182df35968727c56552e97dc6_h.jpg
url: /2012/10/gazing-into-the-crystal-ball-using-triz-technology-trends-2/
enclosure:
  - |
    |
        http://archive.org/download/TrizInnovationIndiaPodcast-TrendsOfTechnologyEvolution/TRIZIndiapodcast-Trends.mp3
        15752219
        audio/mpeg
        
post_views_count:
  - 380
categories:
  - Podcasts
tags:
  - business
  - evolution
  - innovation
  - method
  - triz
  - trizindiapodcast

---
![the crystal planet by elio pastore][1]
  
_Image Courtesy: <http://cdnimg.visualizeus.com/thumbs/40/6f/art,ball,black,and,white,crystal,light,planet-406f5e2182df35968727c56552e97dc6_h.jpg>_

3 innovation facilitators &#8211; Murali Loganathan, Prakasan Kappoth and Bala Ramadurai discuss about the laws of technology evolution.

<http://trizindia.org/profiles/blogs/8-triz-evolution-trends-applied-to-mobile>

This is the link to the original blog post of the podcast. Also check out this link to the Corning Gorilla Glass story &#8211; <http://www.wired.com/wiredscience/2012/09/ff-corning-gorilla-glass/3/>

[Listen to the podcast here][2]

 [1]: http://cdnimg.visualizeus.com/thumbs/40/6f/art,ball,black,and,white,crystal,light,planet-406f5e2182df35968727c56552e97dc6_h.jpg
 [2]: http://archive.org/download/TrizInnovationIndiaPodcast-TrendsOfTechnologyEvolution/TRIZIndiapodcast-Trends.mp3