---
title: Scaling the Innovation Summit – Surviving the Avalanches and Avoidng the Yetis – Growing Ideas into Innovation
author: Dr Shankar MV
date: 2010-11-01T13:22:54+00:00
banner: /images/default.jpg
url: /2010/11/scaling-the-innovation-summit-surviving-the-avalanches-and-avoidng-the-yetis-growing-ideas-into-innovation/
post_views_count:
  - 295
  - 295
categories:
  - Events
  - Justrizin

---
I had the opportunity to address the CII Karnataka Innovation Forum last week. I spoke about the Challenges in growing ideas into innovation. I outlined the Innovation FlowTM Framework and gave a quick intro to TRIZ-based Innovative Problem Solving. There was good interest from the Innovation forum to learn more about TRIZ.. I plan to do a one-day innovation workout for them next.