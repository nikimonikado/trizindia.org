---
title: A simple Problem…but not able to think through TRIZ Principles
author: Tushar Kanikdale
date: 2012-03-08T15:30:00+00:00
banner: blogs/4-Load2.png?width=750
url: /2012/03/a-simple-problem-but-not-able-to-think-through-triz-principles/
post_views_count:
  - 386
  - 386
categories:
  - Justrizin

---
This is a  simple problem where Load Capacity of a vertical bar needs to be increased without increasing Lateral areas and Material.  There is one proposed solution as shown below. I would like to check two things with you

1. Is the solution proposed really resolves the contradiction? (Load Capacity Vs Crossectional Area)

2. If yes, Which TRIZ principle helps to think of solution like this?

 

<a href="blogs/3-Load2.png" target="_self"><img class="align-center" src="blogs/4-Load2.png?width=750" width="566" height="501" /></a><a href="blogs/5-Load.png" target="_self"></a>