---
title: '1 second billing: Telecom Trends'
author: Murali Loganathan
date: 2009-10-06T06:11:22+00:00
banner: blogs/39-benefitsnumberofparts.JPG
url: /2009/10/1-second-billing-telecom-trends/
post_views_count:
  - 345
  - 345
categories:
  - Justrizin
  - Tools

---
Yesterday there was this announcement from TRAI (Telecom Regulatory Authority of India) that it may go on to <a href="http://economictimes.indiatimes.com/Telecom/TRAI-plans-to-ask-telcos-to-offer-per-second-billing/articleshow/5092147.cms" target="_blank">make per second billing mandatory across all operators</a>.

Currently this feature is a market differentiator, that only <a href="http://www.business-standard.com/india/news/tata-docomo-in-bid-to-%5Cdonew%5C/368233/" target="_blank">TATA DOCOMO offers such a billing of call</a> on a per second basis.

Why do we need to watch this trend, because if all operators adopt this there will be a revenue reduction of 10% in the sector. Probably more according to HSBC.
  
As is evident on the capital markets after the news, today 3 of the top operators are in the top 10 losers of the NSE index.

Two things are evident here
  
1. Mono-Bi-Poly trend on time i.e. from billing every 3 minutes to 60 seconds to 1 second
  
2. Regulators force new rules to maximize benefits to customers by doing a cut off at the maximum benefit point (see pic below)

<p style="text-align: left;">
  <img src="blogs/39-benefitsnumberofparts.JPG" alt="" />
</p>

Two questions here
  
1. Is one second the ideal limit or can we even go to zero? How?
  
2. Can you think of other examples where regulation has come at the maximum leverage point?

Looking forward to hear from you

Reference: Chapter 14 Mann, DL Hands on Systematic Innovation for Business and Management, 2004