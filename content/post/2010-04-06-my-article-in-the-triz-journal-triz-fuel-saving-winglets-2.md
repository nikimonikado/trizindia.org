---
title: 'My Article in the TRIZ Journal : ” TRIZ & Fuel Saving Winglets”'
author: Prashant Yeshwant Joglekar
date: 2010-04-06T17:56:05+00:00
banner: /images/default.jpg
url: /2010/04/my-article-in-the-triz-journal-triz-fuel-saving-winglets-2/
post_views_count:
  - 460
categories:
  - Justrizin

---
My article &#8221; TRIZ & Fuel Saving Winglets&#8221; is published in this month&#8217;s issue of The TRIZ journal. 

I talked about &#8220;Building Innovation Capability ; Process of Reverse Ideation&#8221; in my last blog post which incidently was my 100th blog post, wherein I said, see a solution addreesing a problem & then go back and find out what, why, when, where, how of the problem to learn more about it. I found this as the best way to improve my own innovation quotient. By this way I also validate TRIZ research findings to gain confidence.

The story behind this paper goes something like this &#8221; I came across less than a quarter page news last year talking about blended winglets developed by M/S Aviation Partners an aviation research company in the US. Using my white spaces I mined through the aeronautics literature to know more about the phenomenon that leads to the problem. This has helped me simulate the research background,formulate a contradiction and use inventive principles to validate the elements of the solution. 

This according to me is also the best way to capture the existing knowledge, develop an understanding about it & store it in a manner which is easily retrievable thus improving overall &#8220;innovation productivity&#8221; 

I have two key insights drawn from this exercise

1. Don&#8217;t optimise before the team sufficiently think through the problem, it could lead to watsting of resources.  
2. It is better to formulate a TRILEMMA ( one parameter contradicting with two other) than a DILEMMA ( Two Parameters Contradicting with one another) then the resultant solutions lie at the intersections of the contradicting parameters.

Enjoy the article &#8221; TRIZ & Fuel Saving Winglets&#8221; and let me have your feedback. 

<http://www.triz-journal.com/>