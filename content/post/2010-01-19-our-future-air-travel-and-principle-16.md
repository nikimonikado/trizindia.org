---
title: Our Future Air Travel and Principle 16
author: Prashant Yeshwant Joglekar
date: 2010-01-19T16:33:41+00:00
banner: blogs/26-AirlineSeat.jpg
url: /2010/01/our-future-air-travel-and-principle-16/
post_views_count:
  - 378
  - 378
categories:
  - Justrizin
  - Tools

---
<p style="text-align: left;">
  <img src="blogs/26-AirlineSeat.jpg" alt="" />
</p>

Airline business makes its revenue and in turn profit per seat per mile flown. Naturally it is in their best interest to put more seats in the aircraft and the airline manufacturers have to oblige to see that its customers remain profitable. From 31 inch it may be reduced in future to 25 inch to accommodate more passengers. I recollect Principle 16 Partial or Excessive Action which reads “ If exactly the right amount of action is hard to achieve , use “ slightly less” or “slightly more” of the action, to reduce or eliminate the problem”. Now seating ( Slightly Less) & standing ( Slightly More) what we consider as actions then the principle 16 perfectly fits in here.
  
The pictures below speak comparison, so will not be explicit. These days people take blogs and Tweets seriously, if only it’s politically correct for them, I hope you know what I mean.

<p style="text-align: left;">
  <img src="blogs/27-HensTransportInIndia.jpg" alt="" />
</p>