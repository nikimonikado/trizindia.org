---
title: Triz India Podcast IFR
author: Murali Loganathan
date: 2011-08-01T11:16:24+00:00
banner: /images/default.jpg
url: /2011/08/triz-india-podcast-ifr/
enclosure:
  - |
    |
        http://www.archive.org/download/TrizIndiaPodcastIfr/TrizindiaPodcastIfr.mp3
        25707045
        audio/mpeg
        
post_views_count:
  - 335
categories:
  - Podcasts
tags:
  - ideal final result
  - ideality
  - IFR
  - triz
  - trizindiapodcast

---
<a href="http://www.archive.org/download/TrizIndiaPodcastIfr/TrizindiaPodcastIfr.mp3" target="_blank">TRIZ India Podcast IFR</a>

Murali Loganathan, Shankar Venugopal and Bala Ramadurai, innovation facilitators from MindTree Ltd and Honeywell, engaged in a discussion on Ideal Final Result, a tool from the TRIZ toolkit. This is part of trizindia.org, a network of TRIZ enthusiasts and experts.