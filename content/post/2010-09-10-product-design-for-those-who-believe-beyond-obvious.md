---
title: 'Product Design: For those who believe beyond Obvious!'
author: Tushar Kanikdale
date: 2010-09-10T11:30:00+00:00
banner: /images/default.jpg
url: /2010/09/product-design-for-those-who-believe-beyond-obvious/
post_views_count:
  - 375
  - 375
categories:
  - Justrizin

---
** Product : A new perspective**

 

 

 

Lets me start with the question before going on to principles (not techniques) of product design

 

What is meant by the successful product?

 

I talked to few people around and recieved the following consistent answers..

1.A successful product is what organizes produces to earn profits  
2. A successful product is what customers wants and are ready to buy  
3.A successful product is what is designed with right tools and processes, with all operational aspects, voices of customers and potential failure modes

Althought one can claim but, somehow none of the answers above provides the precise feel of a successful product!!!

I came across a powerful keyword which explains it all &#8230;&#8230;&#8230;..can you guess what it is? Think of the products around&#8230;.share your thoughts.

 

See you in the next post&#8230;&#8230;&#8230;&#8230;.with the Keyword!.

 

 

**&#8211; Customer Experience** 

 

&#8230;&#8230;&#8230;.. So the keyword is Customer Experience!. The products are designed to meet the experience customers are lookin for out of a spoken or latent needs. So it means that before start designing the products.. we have to understand what experience it is required to provide&#8230;in turn it means.. we must capture the requirement or Voices from Customers (VOC). However VOC is not the complete in itself.. for very reason that &#8220;People themselves don&#8217;t know what they want..but when offered they are happy to buy or use it&#8221; . This can be termed as latent or unspoken needs and could be major driver for most succesful product designs. It is worth taking the note of a work called Experience. All everyone needs is an experinece.

&#8220;Product and Services are means to achieve the customer experieces&#8221;. As an example, customer doesn&#8217;t need high torque engine car, they need an experinece of accelerating fast during driving.

So To have a sucessful product, the three major aspects are..

 

1. Understand what are **explicit experiences** customers are looking for. (Collection of VOC)

2. Understand what are unspoken / **latent experiences** which can delight customers

3. Understand how the **product is going to be experienced** while it is used by the customers

 

&#8230;&#8230;&#8230;.Infact there are few business cases where the products are launched only based on latent needs that means without need of any market survey or customer interviews for. Do you know any example of such product? At least now a days this product is popular among us!

 

 **Customer Experience &#8211; V/C ratio &#8211; Blue Ocean Strategy &#8211; Ideality (TRIZ)**

One of the product which appeals me is i-Pod. The features in this product might not have been explicity mentioned by customers however customer will be more than willing to buy it at fair Value to Cost ratio. Value again means a measure of customer experiece. So Value is already known however Cost could be the constraint. Now the objective for such product could be to present various proposoal for achieving right V/C (value to cost) ratio for targetted markets.

This also can be explained by recent book I read called as BLUE OCEAN STRATGEY which focusses on four distinct criteria for tuning the features of the product or services for achieving highest V/C ratio to minimize the impact of competition. The four element are

1. Elliminate ( Elliminate those features which are not really required or will not add to Percieved value)

2. Reduce ( Reduce some features if they are excessive and have crossed the marginal increment limit)

3. Raise (Raise the available features which will provide the required value or experience to customers)

4. Create (Create the new features which will delight customers and will put the product on different scale on the competition)

 

The above four element can be easily connected to TRIZ ideality principles. As we know Ideality is defined as

 

Ideality = Useful Effects / (Harmful Effects + Cost)

 

Blue ocean strategy provides us a framework for increasing the ideality by increasing the Useful effects(UE) and reducing Harmful effect (HE) .

However it may happen while increasing let&#8217;s say a Useful effect the other UE deteriorates or HE aggrevates. Once you know the behaviour, you khow the but Contradiction in the system! Believe me, you hold a powerful information about the system by knowing it&#8217;s all contradiction.

 

And this is where TRIZ or any innovation comes in to picture to resolve the contradiction in the system for increasing the Ideality. We must first identify all the contradiction in the system or the baseline product.

The next logical step seems to be resolving all contradiction for achieving highest ideality. right?

However, this step is certainly not the most appropriage step!!

 

So what you think is the most apporpriate next step?

 

&#8211;  Once we highlight all the contradiction the step is to understand if we are choosing right contradiction to solve. It is also imperative to check if contradiction is really valid. If one effect is highly critical compared to other insignificant effects then it may not justify to spend efforts in solving the contradiction. Rather problem becomes simpliefied with only one requirement with highest relative importance.

 

&#8230;Tushar

 

&#8220;Knowledge beyond obvious is Wisdom&#8221;

 