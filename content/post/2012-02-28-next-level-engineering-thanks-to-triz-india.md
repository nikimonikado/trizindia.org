---
title: 'Next Level Engineering: Thanks to TRIZ India'
author: Tushar Kanikdale
date: 2012-02-28T13:00:00+00:00
banner: blogs/7-NLE.PNG
url: /2012/02/next-level-engineering-thanks-to-triz-india/
post_views_count:
  - 755
  - 755
categories:
  - Justrizin
  - Tools

---
<div>
  What  Engineers must need to know today ?
</div>

<div>
   
</div>

<div>
  Reiterating the words from Management Guru Peter Ducker
</div>

<div>
   
</div>

<div>
  <em>&#8221; Teach them what they  don’t know today if you want to achieve phenomenal success tomorrow&#8221;</em>
</div>

<div>
   
</div>

<div>
   With reference to image below and with respect to my observation, engineers today are doing brilliant job at providing engineering solutions and  optimization studies. But there is hardly any drive for Innovation. Certainly not because they are not capable!  It’s because they are trained mostly  on engineering solution and optimization algorithms in academics. 
</div>

<div>
  Optimization strikes a compromising balance between two functional requirements  whereas innovation eliminates compromises by resolving contradictions. As an example there is a  trade-off between Power and Mileage in a car ( I hope you agree to this). Optimization mindset will tune engine and vehicle parameters to maximize power and mileage only to learn that they can&#8217;t be achieved simultaneously to the highest levels. But it takes innovation mindset to think of Hybird car which provides high Power as well as high Mileage at the same time. 
</div>

<div>
  The Good news is,  there exists definite laws and principles to eliminate underlying contradiction in the system. Developing engineers through all four stages (refer image again) will intelligently transform customer voices to right products ultimately leading to  individual and business  success.  Executing innovation is what I would like to call <strong>Next Level Engineering!</strong> Thanks to TRIZ India to take first  step in this direction.
</div>

<a href="blogs/6-NLE.PNG" target="_self"><img class="align-center" src="blogs/7-NLE.PNG" width="341" height="365" /></a>