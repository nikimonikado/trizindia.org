---
title: ARIZ 85C algorithm application introduction
author: Murali Loganathan
date: 2014-04-05T02:26:40+00:00
banner: /images/default.jpg
categories:
  - JusTRIZin
tags:
  - evolution
  - system
  - trends
  - triz
  - ariz
  - mobile
  - gaming
  - children
  - addiction
---
**I am a parent with 2 kids and I am finding it difficult to control the amount of non-school time they are spending on the internet and digital mobile games. This I am concerned with considering the effects of such engagement on the kids holistic development and well being (body, mind and social development). We (me and my friend Bala) are applying ARIZ 85 C core algorithm of TRIZ to find the right level of problem to attack and find not-so-obvious solutions, and subsequently identify system, and sub-systems to build.**

**If you are interested in being part of this solutioning exercise as a parent or as a kid who is interested in designing better engagements with technology or as a practitioner of systematic innovation methods and applying some of the concepts or as a developer interested in building solutions, you are welcome. **

**There are 5 parts to the method set, which we will cover sequentially and share outputs of the methods here. Each of the method will have our inputs and application specific to the problem in hand. You may enter your responses/questions in comments.**

**Each post will have <<Part 
Number>><<Step Identifier>><<Brief 
Title>><<Type>>. If you are already familiar with some
 of the methods or need an overall flow of methods, you may download the free ARIZ85C guide sheet.
**

**Part 0 Step 0.1 Determine final goal of the solution and initial situation**

**This is a questionnaire Step 0.1: determine the final goal of the solution**

- *What is the technical goal (what characteristic of the object must be changed)? *Reduction in the number of hours of screen time for a child (5-12 years old)
- *What characteristic of the object cannot be obviously changed in the process of solving a problem? *Parents do not have control over the content the kid watches. This cannot be changed.
- *Which expense will be reduced if the problem is solved?*Kids fatigue time
- *What is the roughly acceptable expense?*10-20 hours of analysis time – Murali and Bala; 1 hour a day kids expense time
- *What is the main technical / economical characteristic that must be improved? *Number of hours of screen time for a child (5-12 years old) / kids’ time available for social/creative engagements

Essentially we are trying to avoid the blue ocean thinking, completely open strategy development in this step with clear identification of the technical goal, constraints, and if this effort is worthwhile considering the expenses and the improvement that may result. We also take care to identify metrics that are associated with each of them, from a system perspective. We are ok with emotional or unmeasurable things as long as it is clearly identified and is not ambiguous.

Next post to follow will be on **Part 0 Step 0.1 System Operator **

Reference – NIMHANS -www.nimhans.ac.in/clinical-psychology/clinics-ncwb, has an center by name [Services for Healthy Use of Technology (SHUT) ](https://www.google.co.in/search?q=SHUT+nimhns&ie=utf-8&oe=utf-8&gws_rd=cr&ei=9KK_WPHOOcSNvQSiraHgAg#q=SHUT+nimhans&tbs=sbd:1,qdr:y&tbm=nws&*) This center is set up to help mental conditions created due to overuse of technology at the renowned Bangalore based National Institute of Mental Health and Neurosciences, sponsored by the Central Government.