---
title: On time and relationships
author: Murali Loganathan
date: 2010-08-30T09:41:57+00:00
banner: blogs/20-9WLayers.png?width=300
url: /2010/08/on-time-and-relationships/
post_views_count:
  - 1391
  - 1391
categories:
  - Justrizin

---
<div>
  No, I did not intend to start an alternate career in family counselling with this post. This post is on representation of time and relationships in SI. I have always had this difficulty, that both do not seem to fit in within any single tool.
</div>

<div>
</div>

<div>
  Take for instance 9-Windows, while it is great to have your system in the middle and actually be able to see how it evolves over time (the X Axis) and look at all that happens around and inside, it is way too unclear when you want to represent any relationship between your present system with an element in the super-system future.
</div>

<div>
</div>

<div>
  Here is the otherway around example, FAA (Function Attribute Analysis) while it is great to even capture the missing relationships, it performs badly when it comes to representing time. FAA cant represent any time elements in the relationships (frequency, history, period, duration etc).
</div>

<div>
</div>

<div>
  Similarly for IFR and S Curves.
</div>

<div>
</div>

<div>
  Here is one way I thought for 9 windows, which is faceting the relationships and bringing them to focus as layers.
</div>

<div>
</div>

<div>
  <p style="text-align:left">
    <img src="blogs/20-9WLayers.png?width=300" />
  </p>
  
  <p style="text-align:left">
    Each of the colors representing one facet and when seen together all 3 together will give the entire 9 Windows.
  </p>
  
  <p style="text-align:left">
  </p>
  
  <p style="text-align:left">
    So the question here is, when time is moving and relationships are changing is there an unified way to have a better look at the system universe. If so, can it be just one tool (like the example above but that does not entirely solve the problem) or should it be a combination of tools (if so how)
  </p>
  
  <p style="text-align:left">
  </p>
  
  <p style="text-align:left">
    I am posting this here with a hope that someone has actually solved this problem. Possibly in more than 1 way.
  </p>
  
  <p style="text-align:left">
  </p>
  
  <p style="text-align:left">
    Thoughts, Comments please&#8230;
  </p>
</div>