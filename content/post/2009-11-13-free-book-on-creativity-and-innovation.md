---
title: Free Book on Creativity and Innovation
author: Mark L. Fox
date: 2009-11-13T13:40:05+00:00
banner: /images/default.jpg
url: /2009/11/free-book-on-creativity-and-innovation/
post_views_count:
  - 287
  - 287
categories:
  - Justrizin

---
Free Book on Creativity and Innovation

My name is Mark L. Fox and I am author and leading authority on teaching practical creative thinking techniques for business. At age 31, I was the youngest person with the title of Chief Engineer on the Space Shuttle Program.

My latest book “Da Vinci and the 40 Answers”, A Playbook for Creativity and Fresh Ideas has been getting great reviews.

I just wanted to let you know that I have decided to provide the entire book for free in electronic (PDF) format. This is the full version of the book with no strings attached. You don’t even have to register, provide any information, or anything to get the book if you don’t want to.

You can find the link on the home page at [www.slyasafox.com][1]

Please feel free to spread the word to anyone who you think could use some fresh thinking and out of the box ideas.

&#8212;&#8212;&#8212;&#8212;&#8212;&#8211;

&#8220;Wonderful Book!
  
Fun, practical, and enjoyable, Mark Fox&#8217;s book deserves a wide audience.&#8221;

Roger von Oech, Author of &#8220;A Whack on the Side of the Head,&#8221; and the &#8220;Creative Whack Pack&#8221;

&#8220;I love Mark’s ideas! I find Da Vinci and the 40 Answers to be like an ‘idea roulette wheel’. If I have a difficult problem to solve, I go to Da Vinci and blindly open the book to any chapter. I then read that chapter. When I finish reading the chapter, a creative solution to my problem invariably pops up. It sounds like magic, and maybe it is, but you should try it.&#8221;

Jon Spoelstra, Best Selling Author; “Marketing Outrageously” and “Ice to Eskimos”

&#8220;Mark Fox bridges the mighty gap between science and creativity. I&#8217;ve never known anyone else who could do it. Using the scientific principles of TRIZ, Mark leads us skipping into Wonderland.&#8221;

Roy H. Williams, Best Selling Author, “Wizard of Ads”, Business book of the year, Wall Street and New York Times best seller

 [1]: http://www.slyasafox.com