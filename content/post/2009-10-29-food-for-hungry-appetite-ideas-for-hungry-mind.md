---
title: Food for Hungry Appetite; Ideas for Hungry Mind
author: Prashant Yeshwant Joglekar
date: 2009-10-29T17:03:27+00:00
banner: blogs/33-DelhiDarbar3.jpg
url: /2009/10/food-for-hungry-appetite-ideas-for-hungry-mind/
post_views_count:
  - 251
  - 251
categories:
  - Justrizin

---
<p style="text-align: left;">
  <img src="blogs/33-DelhiDarbar3.jpg" alt="" />
</p>

<p style="text-align: left;">
  <img src="blogs/34-DelhiDarbar2.jpg" alt="" />
</p>

<p style="text-align: left;">
  <img src="blogs/35-DelhiDarbar1.jpg" alt="" />
</p>

It is customary these days to eat out on a pre-diwali day with colleagues and this Diwali was no exception. This year we visited Delhi Durbar, a famous restaurant in Colaba, downtown Mumbai. The first impression of the restaurant was that of a cramped space (what can you expect in downtown Mumbai) but after inflowing inside I felt it was also roomy (Roomy, cramped are all relative terms so your opinion may differ). It was roomy at one place and cramped at the other. Owner successfully solved a physical contradiction & kept the visitors guessing  (Is it Roomy or Cramped ?) The restaurant is best known for its Non Vegetarian Cuisine, so for a vegetarian like me only option was to relish the gourmet’s joy. I then started watching pretty things around me  and observed a unique idea which quenched my intellect more than food served there did with my appetite.
  
Let’s TRIZ it. The bigger problem the restaurant owner had to solve was that of a revenue growth, which forced him to put more tables in the limited space to increase the ratio of revenue earned per available square foot. The second way of doing this is to increase customer turnaround time. (Rate of revenue earned per unit time)
  
On a table of 6, dishes ordered at any time are more than double the number of people. Also there are empty plates kept on the table for serving the food. The table thus then represents the perfect picture of a crowded city. .Therefore normally in a restaurant, a side table is housed equidistance from all the main tables where the dishes are kept temporarily before and after they are served. The waiter keeps moving to & fro to serve the dishes, this wafer thin delay causes us irritation. We have already waited for 10-15 minutes to get us served, but are not going to wait fraction of a second to be served after the food has arrived, our state of mind.
  
The delay also affects restaurant’s turnaround time, as lead time from placing an order till paying the bill increases. The restaurant can best act on things that are under their control (the majority portion of the lead time is under restaurant’s control, viz. Cooking time, serving time, table clearing time etc leaving aside the customer’s own time in placing an order, eating and paying the bill).
  
Here we need to do some functional thinking Let me put down the menu of requirements and the contradictions associated with the solution
  
1. Requirement – We want waiter to serve at the same time without going to & fro
  
2. Problem / Constraint – Limitation of space on the table so can’t keep all the serving dishes on the table. Can’t increase the table length because of overall hotel space constraint
  
3. Solution : Can the hotel table be made long and short ( Physical Contradiction) which leads us to formulate a question When we want the serving table ( long while serving) when we do not want the serving table ( short after serving) this is a physical contradiction in time so we can look at those inventive principles that corresponds to such a situation.

I could correlate some of the ideas with TRIZ inventing principles (See the picture below before you read)

1. Table top is extracted from the table, the bigger plate that carries the dishes becomes the table top ( Principle of Segmentation, Extraction, see figure on the extreme right)
  
2. The tripod which has dynamic members (Principle of Dynamics: parts moving relative to each other), we use such stands while placing a carom board. Did we think anytime that idea can be used in restaurant?
  
3. The plate is supported at two points; there is a danger of plate falling down in between the space of the two supports. Since the tripod is dynamic, flexible links (canvas material) are used which are riveted on the side bars, like you bad straps. ( see the middle picture) These links will not allow the plate to fall down and also facilitate opening and closing of the dynamic stand
  
4. There are many such stands so other waiters can use them instead of using many side tables.

Other ideas

5. One can have the trolley nested under the table where the waiter can keep the dishes and serve them one by one. These dishes can be kept under the table which customer self can serve as he wishes.
  
6. For this table top need to be preferably made up of a glass to see through it.
  
7. We can have foldable edges of the table just like some of the dining tables have for them to be long & short at different times

Conclusion: Every object you see is solving some problem. We need to observe and think what the value behind that object is. I quote Darrell Mann’s Latest TWEET: Value (functional delivery) lays between the subjects (you) and the object (what you see)
  
Happy Idea Dining at Innovation Nukkad (<http://prashantinnofuture.blogspot.com>)