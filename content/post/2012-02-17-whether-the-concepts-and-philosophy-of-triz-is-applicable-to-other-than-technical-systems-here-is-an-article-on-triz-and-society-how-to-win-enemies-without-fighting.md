---
title: 'Whether the concepts and philosophy of TRIZ is applicable to other than technical systems? Here is an article on TRIZ and society..- how to win enemies without Fighting?'
author: Umakant Mishra
date: 2012-02-17T06:24:28+00:00
banner: /images/default.jpg
url: /2012/02/whether-the-concepts-and-philosophy-of-triz-is-applicable-to-other-than-technical-systems-here-is-an-article-on-triz-and-society-how-to-win-enemies-without-fighting/
post_views_count:
  - 259
  - 259
categories:
  - Justrizin

---
The contradiction   
We don’t want to set up a fight but we want to see our enemy is punished. We don’t want to kill our enemy by ourselves or by deploying others but we want to see that the enemy is dead. We don’t want to follow any method of violence but we want to see all our enemies dying before us.. 

Is it possible to break the above contradiction? . How will the enemy die if nobody kills him? Secondly, neither our enemies are created at one time nor are they gathered in one place so that all of them can die in some kind of accident or natural disaster. 

Read more in the article &#8220;TRIZ and Society- Winning enemies without fighting&#8221; at<a target="blank" href="http://www.linkedin.com/redirect?url=http%3A%2F%2Fjournalsite%2Etk%2Fjournalsite%2Farticles%2Fviewarticle%2Easp%3Farticleid%3D203&urlhash=UI1L&_t=tracking_anet" rel="nofollow">http://journalsite.tk/journalsite/articles/viewarticle.asp?articleid=203</a> and post your comments.