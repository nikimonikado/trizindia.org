---
title: 'Commitment versus Compliance: A TRIZ Contradiction'
author: Murali Loganathan
date: 2009-08-11T08:55:20+00:00
banner: /images/default.jpg
url: /2009/08/commitment-versus-compliance-a-triz-contradiction/
post_views_count:
  - 275
  - 275
categories:
  - Justrizin

---
Part 1

Case 1

Imagine this situation where you put together a bunch of mandates at various levels of management, till it reaches the last employee on knowledge capture or learning. May be you can just make a random metric based on contributions (you could complicate it by adding usage, pass through, linking, commenting, etc) to your organization’s knowledge base and give sufficient room to game for employees to have fun which anyway will happen!

Call it currency, credit, kunit, knol or whatever. And then set a target for people and indicate that it will influence somehow their variable pay. Don’t even have to promise anything!!

Case 2

Imagine another situation where you connect people with each other in the context of an unknown and allow them to build knowledge based on a mutual commitment they have.

No carrots, gold biscuits or credits. Just the connection.

QuaLity (very subjective) of knowledge or synthesized content (if any) that comes out from Case 2 is invariably higher than what comes out of Case 1.

QuaNTity that comes from just adopting Case 1 is way higher than Case 2.

Is this a TRIZ contradiction or what…open the contradiction matrix please

Parameter we want to improve is Production Spec/Quality/Means

Explanation: intended to be general in terms of quality of what is produced, and means by which it is achieved, can be both tangible and intangible

Parameter that worsens is Amount of Information

Explanation: though not needed some synonyms might help knowledge, memory, volume, archive, library, repository, search. Actually even antonyms are ok in TRIZ.

TRIZ Principles that are identified for the contradiction are

13 Other way around

Invert actions used

Make movable parts fixed or fixed part moving

Turn System process object upside down

32 Colour Changes

Change colour of object/environment

Change transparency of system or environment

15 Dynamization

Allow system characteristics to change to be optimal or find an optimal operating condition

Divide systems into part capable of movement relative to each other

Make system movable, flexible

23 Feedback

Introduce feedback

If already used change magnitude or influence

24 Intermediary

Use an intermediary carrier article or process

18 Resonance

Find resonant frequency

Ideas on the contradiction stated are now flowing from the TRIZ principles&#8230;.

Part 2

I had identified a TRIZ contradiction on 2 parameters viz. quality and quantity of content base available in organizational knowledge bases. The next step after identifying the principles is the actual ideation. Principles that the contradiction matrix lists are in the same order of utility/importance. It took me exactly 40 minutes to come up with these ideas, it would useful for conducting this in a group setting and trying the same principle is the suggestion that came from my friend Prakash.

Here are the ideas… What do you think is absurd? Why do I ask this question, well

Albert Einstein says “If at first, the idea is not absurd, then there is no hope for it”.

13 Otherway around

1 create knowledge from beginning, instead of waiting till the end of the project
  
2 make knowledge creation/capture a continuous process, instead of a single AAR
  
3 allow all project members edit the knowledge instead of the select few
  
4 remove responsibility of SME/expert for knowledge creation and move the action to the knowledge seeker
  
5 instead of maintaining a knowledge base make it robust for all knowledge needs
  
6 push content (not email) where people can see relevance and access
  
7 move experts from project to project regularly
  
8 remove content from knowledge base that are never read/used to archives

32 Color Changes

9 make the seekers/creators role switch regularly
  
10 bright tag useful knowledgebase documents
  
11 tag knowledge creating people in various shades/belts/bands
  
12 alert system for inbred/non collaborative content creation
  
13 widget for recent changes across wiki with different color
  
14 make cross project wiki access/search possible
  
15 do not make public pages that are created by gamers unless seekers exist

15 Dynamization

16 every workproduct one wiki entry
  
17 every conversation one sense made
  
18 allow access from everywhere
  
19 allow access for everyone willing to create knowledge in context
  
20 divide wiki page into sections that are editable
  
21 create master from graph of wiki pages from links
  
22 make managers role optional
  
23 make managers role rotating
  
24 make content approver role rotating
  
25 make contributors into approvers and approvers into contributors

23 Feedback

26 create daily/weekly/monthly/quarterly/annual reports of activity in knowledge base
  
27 report activity of managers to team
  
28 enlist customers in the collaboration system
  
29 survey usefulness of content pieces/large chunks
  
30 create bulletin boards for transactional needs
  
31 allow other project readers to write review or rate content
  
32 use half life to measure freshness of content
  
33 bring seeker and contributor face to face

24 intermediary

34 sub contract/delegate to new joinees for knowledge creation
  
35 video capture knowledge intensive tasks and avoid hard content creation
  
36 symbolise context for quicker description

18 Resonance

37 dont manage content/knowledge bases
  
38 find frequency/spiral dynamics colors/memes to which people respond
  
39 connect/couple resonating roles/people
  
40 inspire to believe in the system/medium (wiki) as a powerful enough to change the working life

Originally Posted at http://cognitivenoise.wordpress.com/