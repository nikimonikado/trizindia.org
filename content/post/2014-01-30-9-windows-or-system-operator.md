---
title: 9-Windows or System Operator
author: nikimonikado
date: 2014-01-30T09:29:59+00:00
banner: 'http://api.ning.com/files/QHO-ed-E7o7hVvXZqsxL8oAJP3bjqfU83m8vCdRcpIqa4k6GuSf2jZnNURdjD8OPixqeezeZAIObFyCK*ajt1mkYcew3upw0/9windows.JPG?width=750'
url: /2014/01/9-windows-or-system-operator/
post_views_count:
  - 3696
  - 3696
categories:
  - Justrizin

---
# 9-Windows or System Operator

Bala Ramadurai, Murali Loganathan, Prakasan Kappoth and Shankar Venugopal discuss &#8220;thinking out of the box&#8221; and &#8220;big picture thinking&#8221; citing examples of BMW, software development, dispensing cash on the third podcast of the series.

&nbsp;

<span style="font-size:medium;"><a href="http://api.ning.com/files/QHO-ed-E7o7hVvXZqsxL8oAJP3bjqfU83m8vCdRcpIqa4k6GuSf2jZnNURdjD8OPixqeezeZAIObFyCK*ajt1mkYcew3upw0/9windows.JPG" target="_self"><img alt="" src="http://api.ning.com/files/QHO-ed-E7o7hVvXZqsxL8oAJP3bjqfU83m8vCdRcpIqa4k6GuSf2jZnNURdjD8OPixqeezeZAIObFyCK*ajt1mkYcew3upw0/9windows.JPG?width=750" width="750" /></a></span>

&nbsp;