---
title: Best ideas come from a penniless inventor
author: Bala Ramadurai
date: 2011-08-16T11:04:02+00:00
banner: http://apracticalguidetoracism.com/missives/images/uploads/300px-Plasma-lamp_2.jpg
url: /2011/08/best-ideas-come-from-a-penniless-inventor/
enclosure:
  - |
    |
        http://www.archive.org/download/BestIdeasComeFromAPennilessInventor/Trizindiapodcast4-resourceThinking.mp3
        20778023
        audio/mpeg
        
post_views_count:
  - 575
categories:
  - Podcasts
tags:
  - ideas
  - india
  - innovation
  - penniless
  - resources
  - smart
  - triz
  - trizindiapodcast

---
<div>
  <p>
    <img class="alignnone" title="Penny plasma" src="http://apracticalguidetoracism.com/missives/images/uploads/300px-Plasma-lamp_2.jpg" alt="" width="300" height="304" />
  </p>
  
  <p>
    Resources thinking is the trick of being very smart and only using means that don&#8217;t cost anything. The conventional way to solve problems is to throw money at it, but resources thinking is about finding free or very cheap ways to solve problems. The protagonists in &#8220;Shawshank Redemption&#8221; and &#8220;The Count of Monte Cristo&#8221; actually use resources to escape out of prison. Murali Loganathan and Bala Ramadurai describe resources in this 4th podcast of TRIZ India.
  </p>
  
  <p>
    <a href="http://www.archive.org/download/BestIdeasComeFromAPennilessInventor/Trizindiapodcast4-resourceThinking.mp3">TRIZ India podcast #4</a>
  </p>
</div>