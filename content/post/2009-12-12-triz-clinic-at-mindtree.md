---
title: TRIZ Clinic at MindTree
author: Bala Ramadurai
date: 2009-12-12T12:27:46+00:00
banner: /images/default.jpg
url: /2009/12/triz-clinic-at-mindtree/
post_views_count:
  - 432
  - 432
categories:
  - Events
  - Justrizin

---
Wow!! quite some time since I caught up with TRIZ India. Lot of things going on in MindTree on the TRIZ front. Murali (also a TRIZ India member) and I came up with the concept of TRIZ Clinic.

**The Metaphor**
  
Basically, we added the metaphor of a clinic to the problem definition and solution. Picture this &#8211; a patient would go to a doc for a checkup when they have a fever or a cough or one such ailment. The doc examines the patient &#8220;say &#8216;aaaw'&#8221;, looks under the patient&#8217;s eyes, checks her pulse and may be even check her blood pressure. This is diagnosis.

Next part is the treatment. Once, the doc figures out what exactly is wrong with the patient, he goes on to suggest medicines, diet, vitamins for the patient.

After treatment is over, the doc does fix up a follow up.

So our TRIZ Clinic had a similar setup &#8211; Murali and I dressed up as TRIZ doctors and did the same thing.
  
1. Problem Diagnosis using 9-Windows, Problem Explorer, Perception Mapping (managers loved this tool because its quick and efficient:)) and many other such tools.
  
2. Problem Treatment using TRIZ Cards.
  
3. Follow up &#8211; people left their Names, IDs and specific tools that they were interested in.

**TRIZ Cards**
  
Murali actually went a step ahead and printed colour coded TRIZ cards based on the role of the potential visitors &#8211; sales, marketing, developer, manager, support, testing. The cards had &#8220;To improve&#8221; and &#8220;Apply&#8221; categories. Under the &#8220;To improve&#8221; category were factors like &#8220;Communication Flow&#8221;, &#8220;Support Cost&#8221;, &#8220;Development Time&#8221;. Under &#8220;Apply&#8221; were TRIZ principles which said &#8220;2 Take out:&#8221; and a brief description on what this principle meant. On the second half had our slogan

**&#8220;Cubicle Innovation&#8221;
  
&#8220;I am TRIZ Healthy&#8221;
  
** 

We believe that &#8220;Just like charity begins at home, Innovation begins at the cubicle&#8221;.

We have had 2 clinics so far, one in Bangalore and one in Chennai. One more in Pune on the 14th of December, one more in Hyderabad on the 17th of December.

There has been so much curiosity for TRIZ Clinic and people find it more appealing when they go thru something that they are well aware of and link it to a new theory.

More about TRIZ and how to relate to it using these tools in a later blog&#8230;