---
title: TRIZ India Summit 2010 proceedings are now online!
author: Karthikeyan Iyer
date: 2010-11-02T17:47:32+00:00
banner: /images/default.jpg
url: /2010/11/triz-india-summit-2010-proceedings-are-now-online/
post_views_count:
  - 345
  - 345
categories:
  - Events
  - Justrizin

---
<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;">Crafitti is pleased to announce that TRIZ India Summit 2010 proceedings are now public and online! You can access the proceedings at <a href="https://sites.google.com/a/crafitti.com/triz-india-summit-2010/" target="_blank" style="color: rgb(42, 93, 176);">https://sites.google.com/a/crafitti.com/triz-india-summit-2010/</a>. Please feel free to share the link with your network.</span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;"><br /></span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;">In lieu of our strategic partnership with Altshuller Institute for TRIZ Studies, TRIZ India Summit 2010 papers are also part of TRIZCON 2010 proceedings! You can access TRIZCON 2010 proceedings at <a href="http://www.aitriz.org/index.php?option=com_content&task=category&sectionid=18&id=83&Itemid=153" target="_blank" style="color: rgb(42, 93, 176);">http://www.aitriz.org/index.php?option=com_content&task=category&sectionid=18&id=83&Itemid=153</a></span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;"><br /></span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;">Cheers!</span>
</div>

<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial, sans-serif; border-collapse: collapse;">Karthik.</span>
</div>