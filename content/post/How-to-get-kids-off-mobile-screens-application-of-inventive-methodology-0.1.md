---
title: How to get kids off mobile screens - application of inventive methodology - 0.1
author: murali
date: 2017-08-16T06:23:40+05:30
pubdate: 2017-08-16T06:26:40+05:30
banner: /img/banners/children-1931189_640.jpg
categories:
  - Tools
tags:
  - evolution
  - system
  - trends
  - triz
  - ariz
  - mobile
  - gaming
  - children
  - addiction
  - ariz85c
  
---

*[Murali Loganathan](https://www.linkedin.com/in/muralidharanl/) and [Bala Ramadurai](https://www.linkedin.com/in/balaramadurai/) have been working on this project for a few weeks now with two objectives in mind-* 

1. *Learn ARIZ*
2. *To solve the "kids spending too much time with the mobile" problem.*

*This post is part of a [series](/tags/ariz85c/)*

<!--more-->

*This piece was already published here - https://trizindia.wordpress.com/2017/04/20/part-0-step-0-1-system-operator/*

#### **System Operator**

- is a grid based tool but required imagination and recalling the past. ****
- is a very useful tool to see how functionalities and subsystem elements have evolved or replaced with newer elements over time.

Here we follow 2 simple rules

1. To perform the system functionality the subsystem components are necessary, this will ensure components that are indispensable are only in the system operator and keeps it focused. If we remove a subsystem component, system cannot function, same with super system cannot function without system elements
2. Decide on whose view point the grid is being filled, in our case it is the parent and also the solver.

Purpose: To engage kid during non-school time and enable holistic development of kid

- School time – outsourced to the school i.e. it is out of scope for the operator
- Non-school time – substitute engagement – stay silent – Our study focal point

Viewpoint: Parent who is filling this

Neighbouring systems: Cable TV, Home, and others

Prior activity recommended is draw roughly what you think is going on with respect to the context and the problem. Poor drawings are accepted, but try to minimise words in the picture. My picture for this problem is below

| **Supersystem** | FamilyNo Ads/BuyWebsites/physical stores | Friends circle (incl school friends)Ads/in app purchasesApp store | Installed Groups,Associated media franchise-movie, TV shows? |
| --------------- | ---------------------------------------- | ---------------------------------------- | ---------------------------------------- |
| **System**      | <to play><PC Games><during their free play time> | <to play><mobile games><during their non-school time> | <to play><Virtual World><during non-school time> |
| **Subsystem**   | PC,Eyes,Mouse,MindPC Game(Offline)       | Device(mobile OS),Eyes,Thumb,Mind,App,Wifi | VR/AR, GogglesEyes,Gestures,Mind,Dynamic App,Wimax(?) |
|                 | **5 years ago**                          | **Now**                                  | **5 years later**                        |

that you see in the middle is a simple format that makes sure you have identified the function, and context clearly and not make do with vague nouns or system names

![sysopadd](https://trizindia.files.wordpress.com/2017/04/sysopadd.png?w=1120)

One of the glaring insights we got was – Being a hero among peers or
– Being the champion in the particular game or – Winning the game had far more social effect that just playing together. Note how dynamic sub systems emerged and my lack of imagination to see what could be an alternate play store for such apps at super system future level. Time frame of our choice is 5 years but can be shorter depending on the system evolution.

Related to the system operator is the anti system operator that takes the opposite functionality/view, in our case keeping the kids hooked to their devices all the non-school time and see how it affects the system space. Some times if the biases or pre-conceptions or mental models are too rigid this may work better in groups.

In our next post we will **Part** **0 Step 0.2 investigate a “by-pass approach”**, without ourselves solving the problem.

See the first in the series of posts on the problem of choice and introduction in [ARIZ 85C algorithm application introduction](https://trizindia.wordpress.com/2017/03/08/ariz-85c-algorithm-application-introduction/)
