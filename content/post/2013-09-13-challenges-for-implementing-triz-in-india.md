---
title: Challenges for implementing TRIZ in India
author: Joseph TK
date: 2013-09-13T03:16:05+00:00
banner: /images/default.jpg
url: /2013/09/challenges-for-implementing-triz-in-india/
post_views_count:
  - 593
  - 593
categories:
  - Justrizin

---
Dear All TRIZ Masters and TRIZniks

 

What are the challenges for implementing TRIZ in India?

1. Lack of patriotism

2. Brain drain

3. Chaturvarnyam (Treating people on 4 categories a. Brahmins who perform rituals to please God, b. Warriors, c. Traders d. Workers

4. abuse of drugs, cigarette, ganja, liquor

5. Corruption &#8211; an after effect of #4 above

6. Resistance to mindset change

7. Believing that products from other countries are good.

 

Let us debate on this

 

Best Regards

Joseph</p>