---
title: Simulate the Future
author: Tushar Kanikdale
date: 2011-05-17T06:30:00+00:00
banner: /images/default.jpg
url: /2011/05/simulate-the-future/
post_views_count:
  - 338
  - 338
categories:
  - Justrizin

---
What makes you most uncomfortable taking the decision?&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;.

&#8230;&#8230;&#8230;&#8230;..And the simplest answer could be &#8220;No clear idea of future outcome and events&#8221;. Although probability helps us to provide possibilities in future it doesn&#8217;t tell in any way the exact future outcome. If the coin is tossed, there is equal probabilty for head and tale however can someone tell the exact outcome. And that&#8217;s where the whole uncertainity is. I believe the most successful people have ability to guess future closer to it&#8217;s exact outcome. The data and information are necessary but not sufficient. The problem with numbers and data sometime could be misleading. The decision most of the time needs intuition, deep knowledge to see the situation beyond obvious&#8230;

          To be successful in any endeavour (Product design, choosing the right options, DFMEA, Planning, customer satisfaction etc) it is imperative to develop the ability to simulate and subsequently control the future outcome. And what is the future outcome we want?&#8230;..ofcourse one with highest Ideality.

Following are few options which in turn provides us ability to get the flavour of future

 

&#8211; Prototype testing (Automobile prototype)

&#8211; Trials (dressing rooms)

&#8211; Learn from experience (consultants, blogs, internet, knowledgbase, experineced person)

&#8211; Imagination (Imagine the probable future outcomes)

&#8211; Observed Trends ( Share market)

&#8211; Laws and principles ( Simulation tools based on underling laws and principles)

 