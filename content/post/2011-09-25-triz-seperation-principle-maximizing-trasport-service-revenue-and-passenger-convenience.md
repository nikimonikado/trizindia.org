---
title: 'TRIZ Seperation Principle: Maximizing Trasport Service Revenue and Passenger Convenience'
author: Tushar Kanikdale
date: 2011-09-25T05:30:00+00:00
banner: /images/default.jpg
url: /2011/09/triz-seperation-principle-maximizing-trasport-service-revenue-and-passenger-convenience/
post_views_count:
  - 327
  - 327
categories:
  - Justrizin
  - Tools

---
**  
** 

Being in Pune I am just taking the case of local public bus transport however I am sure this applies to any general transport meant for public. The public road transport service (PMPML) in Pune although wide spread across the city is not the certainly the most profitable with it&#8217;s fixed frequency schedule and  a  assumption that affordability of this transport is same for all commuters from all sections and classes.

Think of the peak traffic periods, where demand exceeds the supply leading to overloaded, inconvenient and unsafe travels. And yes..around 40% of potential commuters hire rickshaws with a affordability 10 times more than PMPML fare. If I have to think it from business perspective, people preferring other modes of expensive transport are PMPML&#8217;s potential customers and they preferring other mode of (expensive) transport is direct loss of revenue for PMPML. Secondly the frequency for buses is fixed so they run  
deserted in the non-peak and overloaded during peaks. PMPML has tremendous opportunity to increase it’s revenue and passenger convenience at the same time.  The key is to segment the market  and provide different service standards

&#8211; Reduce the frequency during non-peak hours and increase it during peak **(Seperation in Time)** with three bus types as below **(Separation in Space)**. Management terminology may want to call this **Market Segmentation  
** 

&#8211;  Convert regular buses to Type 1 buses with no seats (only standing)   for covering the peaks. This will increase the capacity of bus by atleast by 30% (and hence revenue even if fare is cut down by 10%). The convenience is same for all passengers. In the regular buses we see 60% seating passengers occupy 80% bus area and rest 40% standing get remaining 20%.   Fast commuters don’t want seat. They will prefer to stand when they know it is  fast , safe and low cost

&#8211; Convert regular buses  to Type 2 with better seats , cleanliness, promptness in service, fixed schedules, prior reservation etc.  Charge at least 3 times Type 1 buses (high revenue again).  It won’t be surprise if it is occupied by Sr. citizens, ladies , non rushing  and occasional commuters.

&#8211; Have Type 3 which are regular buses running today on the road. It can accommodate all types commuters happily but only during non-peaks. The fare could be little higher than Type 1 buses again.

   Once we understand the  crowd density, class of people and peak hours for particular route the type ,frequency and number of  buses can be accordingly  optimized to maximize the revenue and passenger convenience simultaneously

There could be an argument that it will increase complexity and cost. The answer is cost increase can be minimized if buses schedules and types are optimized across the city. If at all there is cost increase for additional buses, the additional profit should cover it up in long term (Payback period may not be too long). And complexity due to different type of buses will be compensated by flexibility inside people to choose different buses at different times.

 

Hope this makes social and business sense!