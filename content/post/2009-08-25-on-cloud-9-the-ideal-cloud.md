---
title: On Cloud 9 – the ideal cloud
author: Bala Ramadurai
date: 2009-08-25T03:00:30+00:00
banner: /images/default.jpg
url: /2009/08/on-cloud-9-the-ideal-cloud/
post_views_count:
  - 309
  - 309
categories:
  - Justrizin

---
The trigger for this mini blog post is

http://www.capgemini.com/ctoblog/2009/06/the\_ideality\_of\_the\_cloud.php

Nice article on cloud computing. IFR for people stuck with an operating system (Linux or Windows) is no OS, but still we need the email, the apps and other functions.

So what is IFR for the cloud ? one that waters your plants, but doesnt exist.. 🙂 one that will rain, but not block light.

in the virtual world, one that lets me communicate with my friends/clients/customers without anyone else listening in.

Let me attack this with the reversal of assumptions tool.
  
The things that we assume about an OS
  
1. installation
  
2. CDs
  
3. booting
  
4. wait for OS to boot
  
5. input devices
  
6. emergency recovery disk
  
7. hard disk
  
8. well, a computer
  
9. applications on the OS
  
10. UI

reverse each of them to get
  
1. no installation required
  
2. no CDs
  
3. no booting up
  
4. no booting time
  
5. no input devices
  
6. no emergency disk
  
7. no hard disk
  
8. no computer
  
9. no apps on the OS
  
10. no UI

hmmm&#8230; 1, 2, 3, 4, 6, 9 are satisfied by the cloud, in my opinion. no computer is sort of solved when we move to a mobile device
  
no input devices has not been taken up and there needs to be a basic hard disk to store, i think. some UI is present for the cloud although the voiceweb is closer to no UI.

so a lot more space to maneuver for the cloud.

A side note on the mechanism of reversal of assumptions tool &#8211; the list of assumptions actually give you a laundry list of needs that are satisfied. When you reverse the assumptions, you are trying to get away from the current way the need is satisfied, but not doing away with the need as such. In my opinion, this also defines the problem for you in a veiled sort of way.