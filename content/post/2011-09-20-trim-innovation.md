---
title: Trim Innovation
author: Bala Ramadurai
date: 2011-09-20T00:49:50+00:00
banner: http://www.pragyan.org/11/home/guest_lectures/sugata_mitra/hole-in-wall.jpg
url: /2011/09/trim-innovation/
enclosure:
  - |
    |
        http://www.archive.org/download/TrimInnovation/TRIZIndiaPodcast5.mp3
        29710628
        audio/mpeg
        
post_views_count:
  - 332
categories:
  - Podcasts
tags:
  - blue ocean
  - business
  - eliminate
  - innovation
  - scamper
  - trimming
  - triz
  - trizindiapodcast

---
<img class="alignnone" title="Hole in the wall" src="http://www.pragyan.org/11/home/guest_lectures/sugata_mitra/hole-in-wall.jpg" alt="" width="500" height="333" />

Can you get ideas by eliminating important elements from your system? What is an engineless car? What is a teacher-less classroom? Aggressive elimination of components from a system can change the markets a business caters to.

[Sugata Mitra&#8217;s TED talk][1]

[TRIZ India Podcast #5][2]

 [1]: http://www.ted.com/talks/lang/eng/sugata_mitra_the_child_driven_education.html
 [2]: http://www.archive.org/download/TrimInnovation/TRIZIndiaPodcast5.mp3