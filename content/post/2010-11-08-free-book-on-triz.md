---
title: Free Book on TRIZ
author: Mark L. Fox
date: 2010-11-08T17:52:26+00:00
banner: /images/default.jpg
url: /2010/11/free-book-on-triz/
post_views_count:
  - 315
  - 315
categories:
  - Justrizin

---
Also a free PDF of my latest book &#8220;Da Vinci and the 40 Answers&#8221; can be found on my web site if anyone wants to download it</p> 

[www.slyasafox.com][1]

 [1]: http://www.slyasafox.com