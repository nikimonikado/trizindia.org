---
title: 'Call for Papers: TrizIn 2011 – 2nd Annual TRIZ India Summit'
author: Karthikeyan Iyer
date: 2011-01-31T19:38:36+00:00
url: /2011/01/call-for-papers-trizin-2011-2nd-annual-triz-india-summit/
post_views_count:
  - 468
  - 468
categories:
  - Events
  - Justrizin

---
<font size="2"><span style="font-family: arial,sans-serif;"><a target="_self" href="blogs/18-TrizInLogo.png"><img class="align-left" src="blogs/19-TrizInLogo.png?width=250" width="250" /></a></span></font>

<font size="2"><span style="font-family: arial,sans-serif;">This is a Call for Papers to be presented at the 2nd Annual TRIZ India Summit, taking place on July 28 & 29, 2011 at Bangalore. The summit is being organized by Crafitti Consulting in partnership with The Altshuller Institute for TRIZ Studies (AITRIZ).</span></font><a href="http://2011.trizindiasummit.com" target="_blank"></a>

<a href="http://2011.trizindiasummit.com" target="_blank">http://2011.trizindiasummit.com</a>

<h1 style="font-family: arial,sans-serif;">
</h1>

<h1 style="font-family: arial,sans-serif;">
  <span class="font-size-2"><font size="2"><span style="color: windowtext;">Summit Topics</span></font></span>
</h1>

<h2 style="font-family: arial,sans-serif;">
  <a name="TOC-TRIZ-Papers" id="TOC-TRIZ-Papers"></a><font size="2"><span>TRIZ Papers</span></font>
</h2>

<p style="line-height: normal; font-family: arial,sans-serif;">
  <font size="2"><span>Our emphasis is on REAL case studies of TRIZ being applied in problem solving and innovation contexts. We are strongly interested in providing content for corporate and academic use. </span></font>
</p>

<p style="line-height: normal; font-family: arial,sans-serif;">
  <font size="2"><span>Papers on the following topics are suitable for the conference. This is not a restrictive list.</span></font>
</p>

<ul style="font-family: arial,sans-serif;">
  <li style="line-height: normal;">
    <font size="2"><span>TRIZ Evolution</span></font>
  </li>
  <li style="line-height: normal;">
    <font size="2"><span>TRIZ Applications</span></font>
  </li>
  <li style="line-height: normal;">
    <font size="2"><span>TRIZ Integration</span></font>
  </li>
  <li style="line-height: normal;">
    <font size="2"><span>TRIZ for Patent Analysis</span></font>
  </li>
  <li style="line-height: normal;">
    <font size="2"><span>TRIZ-based research</span></font>
  </li>
  <li style="line-height: normal;">
    <font size="2"><span>Classical TRIZ</span></font>
  </li>
  <li style="line-height: normal;">
    <font size="2"><span>Variants of TRIZ</span></font>
  </li>
  <li style="line-height: normal;">
    <font size="2"><span>Domain-specific TRIZ applications</span></font> <ul>
      <li style="line-height: normal;">
        <font size="2"><span>TRIZ for Electronics and Semiconductors</span></font>
      </li>
      <li style="line-height: normal;">
        <font size="2"><span>TRIZ for Chemical, Petrochemical and Plastics</span></font>
      </li>
      <li style="line-height: normal;">
        <font size="2"><span>TRIZ for Civil and Mechanical</span></font>
      </li>
      <li style="line-height: normal;">
        <font size="2"><span>TRIZ for Management and Quality</span></font>
      </li>
      <li style="line-height: normal;">
        <font size="2"><span>TRIZ for Nanotechnology & Ultra large systems</span></font>
      </li>
      <li style="line-height: normal;">
        <font size="2"><span>TRIZ for biotech and pharmacy</span></font>
      </li>
      <li style="line-height: normal;">
        <font size="2"><span>TRIZ for automobile</span></font>
      </li>
      <li style="line-height: normal;">
        <font size="2"><span>TRIZ for software</span></font>
      </li>
      <li style="line-height: normal;">
        <font size="2"><span>TRIZ in Education</span></font>
      </li>
    </ul>
  </li>
  
  <li style="line-height: normal;">
    <font size="2"><span>TRIZ Case Studies</span></font>
  </li>
</ul>

<p style="line-height: normal; font-family: arial,sans-serif;">
  <font size="2"><b><span> <br /></span></b></font>
</p>

<h2 style="font-family: arial,sans-serif;">
  <a name="TOC-New-category:-Non-TRIZ-Innovation-P" id="TOC-New-category:-Non-TRIZ-Innovation-P"></a><font size="2"><span>New category: Non-TRIZ Innovation Papers</span></font>
</h2>

<p style="line-height: normal; font-family: arial,sans-serif;">
  <font size="2"><span>Realizing that there are many other techniques followed/used in various contexts for Innovation, Invention and Problem Solving, we are happy to accept non-TRIZ papers from the Innovation community. These can be serendipitous or orchestrated innovation methods, experiments, case studies and experiences that have emerged through application of which are not originated from TRIZ. Possible examples may be Six-Sigma, Lean Thinking, Six Thinking Hats, SCAMPER, any other known method or even a new framework that the authors have developed and experimented with, failed or succeeded.<span> </span></span></font>
</p>

<p style="line-height: normal; font-family: arial,sans-serif;">
  <font size="2"><span>Our objective is to understand and try to integrate other techniques with TRIZ. With this objective we are inviting papers in the category &#8220;Non-TRIZ Innovation papers&#8221;. </span></font>
</p>

<p style="line-height: normal; font-family: arial,sans-serif;">
  <font size="2"><span>Authors of Non-TRIZ Innovation papers accepted at the conference will be expected to present their papers during the conference (similar to other paper presentations). Select papers <span> </span>will be taken up for panel discussions on how to synergize the problem solving technique(s) provided in the paper with TRIZ and discover new solutions or new ways to craft a solution.</span></font>
</p>

<h2 style="font-family: arial,sans-serif;">
  <a name="TOC-New-category:-Innovation-Problem-St" id="TOC-New-category:-Innovation-Problem-St"></a><font size="2"><span>New category: Innovation Problem Statements</span></font>
</h2>

<p style="line-height: normal; font-family: arial,sans-serif;">
  <font size="2"><span>While the above topics are focused on how TRIZ (or any other innovation framework) &#8220;has been&#8221; applied to reach a solution in a given context, we realize that conference participants would also like to listen to &#8220;live&#8221; discussions and debates on how TRIZ &#8220;can be&#8221; applied in an innovation context. Well-articulated problem statements offer a great basis for discussion and debate.</span></font>
</p>

<p style="line-height: normal; font-family: arial,sans-serif;">
  <font size="2"><span>With this objective, we are inviting papers in the category &#8220;Innovation Problem Statements&#8221;. </span></font>
</p>

<p style="line-height: normal; font-family: arial,sans-serif;">
  <font size="2"><span>Problem statements are expected to contain:</span></font>
</p>

<ul style="font-family: arial,sans-serif;">
  <li style="line-height: normal;">
    <font size="2"><span>Background of the problem</span></font>
  </li>
  <li style="line-height: normal;">
    <font size="2"><span>Scope of the problem</span></font>
  </li>
  <li style="line-height: normal;">
    <font size="2"><span>Detailed description of the problem</span></font>
  </li>
  <li style="line-height: normal;">
    <font size="2"><span>Any related or associated problems</span></font>
  </li>
  <li style="line-height: normal;">
    <font size="2"><span>Any known solutions and inadequacies in those solutions</span></font>
  </li>
</ul>

<p style="line-height: normal; font-family: arial,sans-serif;">
  <font size="2"><span>Problems defined and articulated using TRIZ would be preferred; however, this is not a mandatory requirement.</span></font>
</p>

<p style="line-height: normal; font-family: arial,sans-serif;">
  <font size="2"><span>Authors of innovation problem statement papers accepted at the conference will be expected to present their problem statement during the conference (similar to other paper presentations). Select problem statements will be taken up for a panel discussion.</span></font>
</p>

<p style="line-height: normal; font-family: arial,sans-serif;">
  <font size="2"><b><span> </span></b></font>
</p>

<h1 style="font-family: arial,sans-serif;">
  <a name="TOC-Submission-Requirements" id="TOC-Submission-Requirements"></a><font size="2"><span style="color: windowtext;">Submission Requirements</span></font>
</h1>

<p style="line-height: normal; font-family: arial,sans-serif;">
  <font size="2"><span>Interested parties are invited to submit an abstract (250 to 300 words) of their proposed paper with</span></font>
</p>

<ul style="font-family: arial,sans-serif;">
  <li style="line-height: normal;">
    <font size="2"><span>Title, along with a brief.</span></font>
  </li>
  <li style="line-height: normal;">
    <font size="2"><span>Name and current biography of the author/s.</span></font>
  </li>
  <li style="line-height: normal;">
    <font size="2"><span>Picture of the author (head shot).</span></font>
  </li>
  <li style="line-height: normal;">
    <font size="2"><span>Affiliation.</span></font>
  </li>
  <li style="line-height: normal;">
    <font size="2"><span>Contact details (phone number and email id)</span></font>
  </li>
</ul>

<p style="line-height: normal; font-family: arial,sans-serif;">
  <font size="2"><span>Abstract and papers to be posted to the following id(s):</span></font><font size="2"><b><i><span>papers@trizindiasummit.com</span></i></b></font>
</p>

<h1 style="font-family: arial,sans-serif;">
  <font size="2"><span style="color: windowtext;">Important Deadlines</span></font>
</h1>

<p style="line-height: normal; font-family: arial,sans-serif;">
  <font size="2"><span>Completing requirements of the Conference on time is critical for all presenters, conference planners and attendees, so the following deadlines are mandatory for your participation.</span></font>
</p>

<table style="border-collapse: collapse; border: medium none; font-family: arial,sans-serif;" border="1" cellspacing="0">
  <tr style="height: 30pt;">
    <td style="width: 186.75pt; padding: 0.75pt; height: 30pt;" valign="top" width="249">
      <p style="margin-bottom: 0.0001pt; line-height: normal;">
        <font size="2"><span> Abstract Submission Deadline</span></font>
      </p>
    </td>
    
    <td style="width: 83.35pt; padding: 0.75pt; height: 30pt;" valign="top" width="111">
      <p style="margin-bottom: 0.0001pt; line-height: normal;">
        <font size="2"><span> 15 March 2011</span></font>
      </p>
    </td>
  </tr>
  
  <tr style="height: 11.25pt;">
    <td style="width: 186.75pt; padding: 0.75pt; height: 11.25pt;" valign="top" width="249">
      <p style="margin-bottom: 0.0001pt;">
        <font size="2"><span> Draft of Paper Due</span></font>
      </p>
    </td>
    
    <td style="width: 83.35pt; padding: 0.75pt; height: 11.25pt;" valign="top" width="111">
      <p style="margin-bottom: 0.0001pt;">
        <font size="2"><span> 15 May 2011</span></font>
      </p>
    </td>
  </tr>
  
  <tr style="height: 21pt;">
    <td style="width: 186.75pt; padding: 0.75pt; height: 21pt;" valign="top" width="249">
      <p style="margin-bottom: 0.0001pt; line-height: normal;">
        <font size="2"><span> Final Papers and Electronic Files Due</span></font>
      </p>
    </td>
    
    <td style="width: 83.35pt; padding: 0.75pt; height: 21pt;" valign="top" width="111">
      <p style="margin-bottom: 0.0001pt; line-height: normal;">
        <font size="2"><span> 31 May 2011</span></font>
      </p>
    </td>
  </tr>
</table>
