---
title: The Secret Sauce of Innovation
author: nikimonikado
date: 2014-04-01T13:28:07+00:00
banner: https://lh6.googleusercontent.com/Shz7sL-9aq22BAbADT2k_5-3xpG0fOK0WWSKiCZMEyDHbij4wi4NfdbDUIXV9lOBEft6hKQi9F3bZoOZvl96BLYbLscDYfv2AGRhRg0MFhl6LJ6uoyG-yum-jPuesA
url: /2014/04/the-secret-sauce-of-innovation/
post_views_count:
  - 2994
categories:
  - Justrizin

---
<h1 dir="ltr">
</h1>

<p dir="ltr">
  TRIZ (Russian acronym for Theory of Inventive Problem Solving) has been known for many decades. Innovation for mankind has been around since fire and wheel were invented and used successfully. Prior to TRIZ and its ally ARIZ (The Algorithm of Inventive Problem Solving), inventing was best left to people who were born with such a gift (or believed they were born with such a gift). With the advent and successful implementations of TRIZ/ARIZ to solve many challenging problems, one can confidently say that inventiveness can certainly be learnt. Our own experience in the field also points to the very same conclusion. The most common (FFAQ or frequently frequently asked question) is “I have managed just fine solving problems without TRIZ/ARIZ, why should I learn something like this?”. Our (and many others like us) answer is “Good question. If you are able to solve problems on your own, please go ahead and do that, since that is the best way to solve problems. If you can’t, TRIZ can help, write to us”.
</p>

<p dir="ltr">
  What in the world is TRIZ/ARIZ? You might ask. we have a 10 word, 15 symbol, 8 number answer &#8211; <a href="http://trizindia.org/2011/11/what-in-the-world-is-triz-part-1-2/">http://trizindia.org/2011/11/what-in-the-world-is-triz-part-1-2/</a>. Or better still, click the link given above.. 🙂 Please listen to <a href="http://trizindia.org/2011/12/what-in-the-world-is-triz-part-2-2/">http://trizindia.org/2011/12/what-in-the-world-is-triz-part-2-2/</a> also.
</p>

<p dir="ltr">
  Once, you have applied the principles, solved many problems and you are ready to teach others how to solve these problems, there is always a problem coming your way &#8211; “boy, o boy, which one of the umpteen techniques that I applied should I teach these guys to solve their problem”. Of course, you are tired of hearing the dreaded consultant “It depends” answer. Also, you don’t want to come across as a retired Colonel reciting his Victoria Cross stories to his bored lieutenants (Direct reference to Jungle Book’s Colonel Hathi). Both these options are tempting, but can have undesirable consequences on your participants. 🙂<img alt="" src="https://lh6.googleusercontent.com/Shz7sL-9aq22BAbADT2k_5-3xpG0fOK0WWSKiCZMEyDHbij4wi4NfdbDUIXV9lOBEft6hKQi9F3bZoOZvl96BLYbLscDYfv2AGRhRg0MFhl6LJ6uoyG-yum-jPuesA" width="450px;" height="253px;" />
</p>

<p dir="ltr">
  (Image Source: <a href="http://www.animationsource.org/sites_content/jungle_book/upload/fanchars/pic_detail4de7df4ec4fac.png">http://www.animationsource.org/sites_content/jungle_book/upload/fanchars/pic_detail4de7df4ec4fac.png</a>)
</p>

<p dir="ltr">
  So what can you do now? Here is a secret sauce that has worked like magic for us.<img alt="" src="https://lh5.googleusercontent.com/ppQkEUrcB3JpQQ8Okf8rTvNxfdKRfnSdYPF5kIHEg_5T1f7CiUMs_rIZtnuPeF8oLmpUIbMG60tM3rnAZITcL9g498MV7NSz3H18BMEh9Bqwc2LzL6O0-Hdh1cO85w" width="532px;" height="323px;" />
</p>

<p dir="ltr">
  First of all, let us give you some theory.
</p>

<p dir="ltr">
  In selecting TRIZ techniques for application, you are faced with 3 major dimensions (as given in the Figure, the only figure, we promise),
</p>

<p dir="ltr">
  1.       The Person
</p>

<p dir="ltr">
  2.       The Problem
</p>

<p dir="ltr">
  3.       The Method
</p>

<p dir="ltr">
  Person dimension includes the nature of participant or someone who will benefit by solving the problem or resolving a contradiction. Our experience indicates participants may belong to any of the below levels
</p>

<p dir="ltr">
  ·         ‘Not for me’
</p>

<p dir="ltr">
  ·         ‘I Like this’
</p>

<p dir="ltr">
  ·         ‘I will apply as situation demands’
</p>

<p dir="ltr">
  ·         ‘this changed my way of thinking completely’
</p>

<p dir="ltr">
  For TRIZ to become sustainable the more the number of people maturing to level 4, the better it is. So the first 3 levels have to be handled with care to make sure applications are easy and rigorous considering the next 2 dimensions.
</p>

<p dir="ltr">
  Problem dimension includes how certain problems need special handling, with readymade tools from TRIZ portfolio.
</p>

<p dir="ltr">
  e.g.
</p>

<p dir="ltr">
  ·         For application around visualizing space-time, system operator (or 9 windows or 9 screens as it is fondly referred to) and an independent function analysis may well suffice
</p>

<p dir="ltr">
  ·         For imagining the ideal future, Ideal Final Result and follow up with the Size Time Cost operator may be useful
</p>

<p dir="ltr">
  ·         For resolving problems in tool/substance interactions, Substance-Field with standard solutions may be looked at
</p>

<p dir="ltr">
  ·         For pure differentiation or following possible evolution paths of systems, the Trends of Engineering Systems Evolution will be handy
</p>

<p dir="ltr">
  The final dimension of Methods is mentioned here because some methods lend itself for easy combination with others. e.g.
</p>

<p dir="ltr">
  ·         Immediately after doing functional analysis, time is right for trimming
</p>

<p dir="ltr">
  ·         While you finish a Su-Field, looking for resources then, makes solutions cheaper and robust
</p>

<p dir="ltr">
  With rigor being built into the method itself, only practice and benefiting for current contexts constantly can make TRIZ part of your company&#8217;s innovation culture.
</p>

<p dir="ltr">
  TRIZ India is a growing community of global TRIZ and systematic innovation practitioners. At <a href="http://trizindia.org/">TRIZ Innovation India | http://trizindia.org/</a>  we cater to those that are serious about ‘how to innovate’ with ‘no barriers to entry’. With TRIZ masters and experienced facilitators across the world actively engaged in the community, TRIZ Innovation India can give the necessary impetus to your specific innovation needs.
</p>

<p dir="ltr">
  Resources including podcasts, pro-forma for TRIZ techniques and workbooks, facilitation tips, cross domain applications, stories and special interest groups are available for free at the community site.
</p>

<p dir="ltr">
  It is ultimately a community of practice with the vision and objectives set by ‘you’ the community. Write to us, become part of the community, let us innovate together.
</p>