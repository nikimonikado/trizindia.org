---
title: Website runs without internet..Trim the most important function or resource
author: Prakasan K
date: 2012-01-23T05:12:19+00:00
banner: /images/default.jpg
url: /2012/01/website-runs-without-internet-trim-the-most-important-function-or-resource/
post_views_count:
  - 344
  - 344
categories:
  - Justrizin
  - Tools

---
<span>Re-post from <a href="http://trizit.blogspot.com">http://trizit.blogspot.com</a>  </span>

<span><br /></span>

<span>Ok; this is for those who follow South Indian films and also know the &#8220;super star&#8221; in the Tamil movie industry, Rajinikanth. Do Google about him to know his super star power (in movies) and read some PJ&#8217;s about him. But this is interesting, and not a joke, that his website <a href="http://www.allaboutrajni.com">www.allaboutrajni.com</a> runs when there is no internet (a very simple solution technically)</span>

<div>
  Read more about it here..
</div>

<div>
  <a href="http://www.thehindu.com/arts/cinema/article2818048.ece">http://www.thehindu.com/arts/cinema/article2818048.ece</a>
</div>

<div>
  Well, no technological break through. Your browsers will get you the wesbite only when you have an internet connection, but to browse Rajini sirs website, you should disconnect your internet, before you disconnected the internet they would have downloaded a flash file and it will work only after it identifies no internet connection.
</div>

<div>
  Coming to some interesting observation (I saw the comments in the Facebook posting where I got this from), and using Rajini&#8217;s power in other context and use Trimming.The most important ingredient of a website we take it granted is internet. Now, can we remove (trim down) this important resource required for a website? What if the same concepts used in all important website around us, especially for the rural areas of developing countries like India and yet to be developed countries where there is no power (or power only for few hours), internet is available like ration?
</div>

<div>
  I have read some interesting innovations similar to this where this kind of offline websites are syncd with servers twice a day in rural areas when a vehicle fit with the remote satellite connection pass through the villages. Now, we probably have better resources like mobile phone. How about a mobile mesh network connecting to this offline website and the realtime information is available through simple txt (SMS). The mobile mesh is again created by using your and my cell phones in the rural area..
</div>

<div>
  More ideas?
</div>