---
title: How to get kids off mobile screens - application of inventive methodology - 0.4
author: murali
date: 2018-01-11T06:24:40+05:30
pubdate: 2018-01-11T06:26:40+05:30
draft: false
banner: /img/banners/children-1931189_640.jpg
categories:
  - Tools
tags:
  - apple
  - ariz
  - ariz85c
  - learning
  - mobile
  
---

*[Murali Loganathan](https://www.linkedin.com/in/muralidharanl/) and [Bala Ramadurai](https://www.linkedin.com/in/balaramadurai/) have been working on this project for a few weeks now with two objectives in mind-* 

1. *Learn ARIZ*
2. *To solve the "kids spending too much time with the mobile" problem.*
3. *To publish a case study of application ARIZ85c from start to finish*

*This post is part of a [series](/tags/ariz85c/)*

<!--more-->

*This piece was already published here - https://trizindia.wordpress.com/2018/01/09/part-0-step-0-4-0-6-quantitative-characteristics-and-conditions-where-the-innovation-will-function/*

# Part 0 Step 0.4 – 0.6 Quantitative characteristics and conditions where the innovation will function

To be honest we did not expect this to happen so quickly, [Apple being pushed by major shareholders to fix youth/children getting addicted to devices](https://www.bloomberg.com/news/articles/2018-01-08/jana-calpers-push-apple-to-study-iphone-addiction-in-children). So the [problem we chose for solving with ARIZ is still relevant and timely](https://trizindia.wordpress.com/2017/04/20/part-0-step-0-1-system-operator/).

In this post we will look at figuring the required quantitative characteristics. Currently it is not difficult to find numbers and metrics for any of the system characteristics, irrespective of your domain.

### **0.4 Determine the required quantitative characteristics.**

We used fairly basic set of tools  like statista, wikipedia, research reports and google play store meta data to figure most of our quantitatives. These included video (that takes a major chunk of time in the form of binge watching), mobile games and available opportunities in those.

### **0.5. Increase the required quantitative characteristics by considering the time for implementing the invention**

### **0.6. Define the requirements of the specific conditions in which the invention is going to function.**

1. Consider the specific conditions for manufacturing the product: in particular, the acceptable degree of complexity.
2. Consider the scale of future applications.

So we figured the following conditions.

1. Convincing to both the user and owner of the device that on-screen time needs to be limited due to the detrimental effects of gaming
2. Distribution of the app/technology through play store, or school group or parent group or others
3. It has to be easy to use
4. It should not lead to loss of freedom in using the way i want the device/network/app to be used
5. It has to be free
6. Resource light on the device (memory, performance, free of lag)
7. Can take many device/app rights to use or control
8. Scale of future applications

In our next post we will look at **Part 0 Step 0.8. Define the problem more precisely using patent information** and summarize our learning from part 0.

Here is what we did for our problem you can skip the rest of the post…as it contains problem specific characteristics and lists sources of those data… if you are already working on a different problem you may want to focus on collecting data for that problem.

#### Evidence from Data source 1
~$500 million revenue in mobile games in India <https://www.statista.com/outlook/211/100/mobile-games/worldwide#takeaway>

#### Evidence from Data source 2
The average revenue per user (ARPU) in the “Mobile Games” segment amounts to US$25.26 in 2017 and is forecasted to increase to US$25.57

#### Evidence from Data source 3
Screen time parental control <https://play.google.com/store/apps/details?id=com.screentime.rc&hl=en>

#### Evidence from Data source 4
Google play services that has all the necessary permissions to the data that we need for building a layer of control or behaviour changing app. <https://play.google.com/store/apps/details?id=com.google.android.gms&hl=en>

API and service access of play is available for client apps through <https://developers.google.com/android/guides/api-client>

#### Evidence from Data source 5
<https://www2.deloitte.com/global/en/pages/technology-media-and-telecommunications/articles/tmt-pred16-media-mobile-games-leading-less-lucrative.html>

> Deloitte Global predicts that in 2016 mobile (smartphone and tablet) will become the leading games platform by software revenue, generating $35 billion in revenue up 20 percent from 2015. This compares to $32 billion for PC games and $28 billion for console games, up only five and six percent respectively from the previous year.
>
> However we expect average revenue per game by platform to vary significantly. We forecast $4.8 million per console game available, $2.9 million per PC game, but only $40,000 per mobile game. While many tens of thousands of companies create mobile games, we would expect only about 200 mobile games companies will gross over $1 million in 2016.
>
> One key characteristic of the mobile games market is low barriers to entry. A typical latest-generation console or PC-based game costs tens of millions of dollars to produce, a similar sum to market, and can take several years to develop. Mobile games can be created in mere hours. This has contributed to a profusion of mobile games titles. As of the start of 2016, we estimate app stores will offer more than 800,000 mobile games; this compares to 17,000 titles available for games consoles and PCs. Every day a further 500 mobile games titles are launched on a single platform.
>
> The immense number of mobile game titles renders many new titles invisible without substantial marketing spend. If mobile games publishers cannot afford a TV campaign, they could use outcome-based advertising, such as app-install ads. However this can be expensive. A mobile games publisher might pay several dollars per download with no resulting revenue. And since the predominant business model for mobile games is freemium — whereby games are downloaded for free and additional content is charged for — the vast majority of mobile players can (and do) spend tens of hours playing without having to pay a cent.
>
> The large investment required for a mobile game to stand out from the crowd is likely to keep the market stratified in 2016. We expect about 80 per cent of mobile games revenue in the top 1,000 titles to be earned by the top 20 publishers in each region: that leaves the remaining 20 percent of mobile games revenue to be shared among many tens of thousands of developers. One survey of 8,000 developers found that 17 percent generated no revenue; 18 percent made less than $100 a month, and half made less than $1,000 per month.
>
> Given these market characteristics, we predict that the rise of mobile games, in terms of revenues, will not ‘eat’ console and PC games revenues in the medium term: the three platforms will co-exist, with each serving largely distinct needs, underpinned by different business dynamics.
>
> We expect games play to remain a principal usage of mobile devices and predict that in subsequent years mobile games revenues will continue to grow, propelled by both a rising base of mobile devices, and a marked increase in device specification, particularly for smartphones. However, life may become increasingly arduous for some mobile games publishers, potentially leading to some major players exiting the market in 2016 or 2017.

 
#### Evidence from Data source 5

**Number of apps is one of our key concerns and more than the revenue of apps or video.**

<https://think.storage.googleapis.com/docs/mobile-app-marketing-insights.pdf>

> Shows that 46% installed daily app used are games, but basis age is 18-24, which is slightly beyond our target user group.
>
> 52% games installed are discovered through family/friends that comes close to what we see.
>
> Also of note is 3 in 4 expect apps to be free and are willing to pay only $1.5 per game app category downloaded on average versus overall $2.2 which is 33% less and that $2.2 is also falling as a global trend

 

<https://en.wikipedia.org/wiki/List_of_best-selling_video_games#Mobile_phone> has the entire trending on revenue history across various categories.

<https://www.statista.com/statistics/301185/average-active-app-usage-worldwide/>

 

| **Mobile Internet**                      | **Values** |
| ---------------------------------------- | ---------- |
| [Projected mobile phone internet user penetration in India for 2021](https://www.statista.com/statistics/309019/india-mobile-phone-internet-user-penetration/) | 37.36%     |
| [Average number of mobile apps actively used in India](https://www.statista.com/statistics/301185/average-active-app-usage-worldwide/) | 7.5        |
| [Number of monthly active WhatsApp users in India](https://www.statista.com/statistics/280914/monthly-active-whatsapp-users-in-india/) | 70m        |

#### Evidence from Data source 6

**Children and marketing violent content through games**

<https://www.ftc.gov/system/files/documents/reports/marketing-violent-entertainment-children-review-self-regulation-industry-practices-motion-picture/vioreport.pdf>

> “In a survey sponsored by the Commission, 24% of children between the ages of 11 and 16 included at least one M-rated game in their list of three favorite games.”
>
> - Profanity and adult content is the only consideration for M rating. Addiction and number of scholastic hours?
>
> Defn: Titles **rated M** (**Mature**) have content that may be suitable for persons ages 17 and older. Titles in this category may contain intense violence, blood and gore, sexual content and/or strong language. Titles **rated** AO (Adults Only) have content that should only be played by persons 18 years and older.

<http://www.ign.com/wikis/content-ratings/ESRB>

<http://www.esrb.org/ratings/search.aspx>

This is an interesting solution emerging with voluntary rating of apps and content in the apps, which can be used to control installation of apps.

Comparative Analysis of Rating

<http://www.classification.gov.au/Public/Resources/Documents/80000CPB+-+A+Comparative+Analysis+of+Ratings,+Classification+and+Censorship+Around+Commissioned+Research256794.pdf>

<http://www.cs.ccsu.edu/~stan/research/CSeducation/SIGCSE2009MGD.pdf>

#### Evidence from Data source 7

**Hours spent by game data required**

<http://www.vertoanalytics.com/consumers-spend-1-billion-hours-month-playing-mobile-games/>

> ​    Approximately if we have a pareto distribution of usage, for EA games, we have approximately 300 million hrs played across all users, of which 200 million hrs is taken by the top users that is about 10 million,
>
> So we are looking at 200 million hrs per month / 10 million users = 20 hrs per month per user.

