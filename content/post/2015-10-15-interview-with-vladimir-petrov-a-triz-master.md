---
title: Interview with Vladimir Petrov, a TRIZ Master
author: nikimonikado
date: 2015-10-15T09:59:42+00:00
banner: http://trizindia.org/wp-content/uploads/2015/09/Vladimir-Petrov-300x300.jpg
url: /2015/10/interview-with-vladimir-petrov-a-triz-master/
post_views_count:
  - 199
categories:
  - Justrizin

---
[<img class="alignnone wp-image-358" src="http://trizindia.org/wp-content/uploads/2015/10/bala-photo1.png" alt="bala-photo1" width="144" height="160" />][1][<img class="alignnone wp-image-353" src="http://trizindia.org/wp-content/uploads/2015/09/Vladimir-Petrov-300x300.jpg" alt="Vladimir-Petrov" width="155" height="155" srcset="http://trizindia.org/wp-content/uploads/2015/09/Vladimir-Petrov-300x300.jpg 300w, http://trizindia.org/wp-content/uploads/2015/09/Vladimir-Petrov.jpg 370w" sizes="(max-width: 155px) 100vw, 155px" />][2]

Vladimir Petrov (Profile here &#8211; ) has been an avid supporter of TRIZ Innovation India and has always volunteered to spread awareness of TRIZ in India, which is the mission of TRIZ Innovation India. His simple reply to my email was &#8220;How can I help TRIZ India?&#8221;. I present excerpts from our online interview, which was the result of his offer of help.

_Bala Ramadurai: From your track record, you have not only taught TRIZ, but also practically used it, as is obvious from your publications and patents. How did you get started with TRIZ?_
  
**Vladimir Petrov**: I started studying TRIZ by correspondence with Altshuller in 1972. He led the two-year university program and I successfully completed the program. At the time, I was an R&D engineer. I immediately began to apply TRIZ in my work and also helped others solve their problems. In parallel, I passed TRIZ knowledge to my staff as well. In 1973, I started my teaching experience in TRIZ to my first group of 25 persons, a 120 hour program. That was my beginning.

_**BR**: Please describe some of your memorable experiences with Altshuller. How was he as a teacher, friend?_
  
**VP**: About Altshuller, I have a lot of memories. This alone, I could write a great book. He was my teacher and colleague. We not only taught together at various occasions, we discussed various aspects of TRIZ as well. He became a friend of mine. When he came to Leningrad, he lived in my house.

_**BR**: You have travelled across the globe teaching TRIZ. What according to you are some conditions that need to be there in place for TRIZ to be learnt and applied?_
  
**VP**: In my opinion, the most important thing is that people have to have a desire to learn TRIZ.

_**BR**: Some people often question if TRIZ is meant only for engineers and technologists. What will be your response to such a question?_
  
**VP**: TRIZ methodology is universal and can be applied to any field of knowledge.

_**BR**: Are there problems that TRIZ should not be used for solving? if so, which ones are those fields?_
  
**VP**: TRIZ cannot be applied effectively when one does not have enough knowledge or experience with a problem.

_**BR**: Are there some problems that you solved or helped solve only to realize that the market for application of the solution had moved on and the solution was not relevant any more?_
  
**VP**: Even in this case, the specialist will solve problems faster and better than a non specialist only if they apply inventive thinking skills (TRIZ thinking).

_**BR**: Please tell us about your latest book. How will it benefit our readers on TRIZ India?_
  
**VP**: My latest book is written in Russian. It still needs to be translated into English. It&#8217;s a big job and I&#8217;m looking for helpers and sponsors. Maybe someone from your association can help me. This is a TRIZ tutorial.

_**BR**: Last question, what should practitioners from India do differently when applying TRIZ to solve problems?_
  
**VP**: Unfortunately, I do not know how and what you are doing [in India] now.

 [1]: http://trizindia.org/wp-content/uploads/2015/10/bala-photo1.png
 [2]: http://trizindia.org/wp-content/uploads/2015/09/Vladimir-Petrov.jpg
