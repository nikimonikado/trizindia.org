---
title: What Drives Battery Evolution?
author: Tushar Kanikdale
date: 2013-06-20T06:00:00+00:00
banner: blogs/2-BatteryEvolution.jpg
url: /2013/06/what-drives-battery-evolution/
post_views_count:
  - 819
  - 819
categories:
  - Trends

---
Hi Innovators,

I was reading about battery as one of the key barriers for mobile technology evolution. The capacity of battery has increased only 2 times in last 10 years whereas processing speed has gone 12 folds up. Here is a graph I came across showing Battery evolution with time.

          I am just wondering, what is actually changing over time. Does it follow the TRIZ laws of technology evolution? At least two major trends I know

1. **Increasing Contrability and Dynamics**

2. **Moving from MACRO to micro (Field changes from Mechanical &#8211;>Thermal &#8211;>Pneumatic /Hydraulic&#8211;>Chemical &#8211;> Electrical&#8211;> Electromagnetic) **

Apparently I am not able to link the battery trend with evolution priciples. Your views will help to bring more insights.

<a href="blogs/1-BatteryEvolution.jpg" target="_self"><img class="align-full" src="blogs/2-BatteryEvolution.jpg" width="498" height="390" /></a>

Source : <http://www.greenelectric.com.my/index.php?option=com_content&view=article&id=66&Itemid=93>

 

 

 