---
title: Two TRIZ India Summit 2010 Papers featured on AITRIZ this month!
author: Karthikeyan Iyer
date: 2010-09-18T11:30:00+00:00
banner: /images/default.jpg
url: /2010/09/two-triz-india-summit-2010-papers-featured-on-aitriz-this-month/
post_views_count:
  - 339
  - 339
categories:
  - Events
  - Justrizin

---
<div>
  <span class="Apple-style-span" style="color: rgb(0, 0, 0); font-family: arial; font-size: small;">Two TRIZ India Summit 2010 papers are featured on this month&#8217;s AITRIZ edition of Inside TRIZ, as a follow-up of Crafitti&#8217;s strategic partnership with AI for TRIZ India Summit.</span></p> 
  
  <div>
    <font class="Apple-style-span" color="#000000" face="arial"><span class="Apple-style-span" style="font-size: small;"><br /></span></font>
  </div>
  
  <div>
    <div>
      <ol>
        <li>
          <font class="Apple-style-span" color="#000000" face="arial"><span class="Apple-style-span" style="font-size: small;">Enhancing Air Safety for Pilots and ATC using TRIZ, A.K.Dhamija, K. Ramachandran, Divya Gautam, DRDO <span class="Apple-style-span" style="font-family: 'Lucida Grande', Arial, sans-serif; font-size: 13px; color: rgb(94, 94, 94);"><a href="http://www.aitriz.org/articles/InsideTRIZ/323031303039695452495A.pdf">http://www.aitriz.org/articles/InsideTRIZ/323031303039695452495A.pdf</a></span></span></font>
        </li>
        <li>
          <font class="Apple-style-span" color="#000000" face="arial">Determining Qualitative Parameters Using TRIZ for Estimating IP Value of Intangibles, Pinaki Ghosh, Subhadip Sarkar, Prasanna C. G, Santosh Pati, Infosys Technologies Limited <a href="http://www.aitriz.org/articles/TRIZFeatures/3230313030395452495A66.pdf">http://www.aitriz.org/articles/TRIZFeatures/3230313030395452495A66.pdf</a></font>
        </li>
      </ol>
    </div>
    
    <div>
      <font class="Apple-style-span" color="#000000" face="arial"><br /></font>
    </div>
    
    <div>
      <font class="Apple-style-span" color="#000000" face="arial">We look forward to other papers getting featured in coming months!</font>
    </div>
  </div>
</div>