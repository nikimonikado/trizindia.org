---
title: 'Decision Making : You need the tool!'
author: Tushar Kanikdale
date: 2011-05-16T07:30:00+00:00
banner: blogs/17-DM.JPG
url: /2011/05/decision-making-you-need-the-tool/
post_views_count:
  - 340
  - 340
categories:
  - Justrizin

---
 

**PRINCIPLE**: The best decision ensures **Highest Ideality Future Outcome** for situation under consideration. 

**Note**: The ideality for the short term and long term future may be different and needs judging on what is desired.

<a target="_self" href="blogs/16-DM.JPG"><img width="635" src="blogs/17-DM.JPG" class="align-center" /></a>

  

**TOOL :** TRIZ and Innovation methodology can be very powerful for decision making in every aspects we deal in personal and professional life. I am just highlighting the simplest decision tree especially when options are available to choose and start with.  The image shows a tool which helps to organize the thoughts and ideas at one common place including some quntification to information available. It helps to identify all the possible HE, Failure modes and UE for the options chosen.  The probabilty and magnitude information provides realistic ideality calculation. The option with highest ideality becomes the winner. Further the analysis of individual HE, UE and underlying contradictoins can help to arrive at better options. 

**UE : Useful**** effects are benefits. However first question to ask is**

**&#8220;What requirement (Y) the option (X) is able to meet?&#8221;**

The requirements answered must be caputured as UE. 

So this works littlel opposite to conventional practices where we know what we need (Y) and then we devise X&#8217;s to achieve Y