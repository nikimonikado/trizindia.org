---
title: TRIZ Trends 2014, A Conference in GE India
author: nikimonikado
date: 2014-02-26T12:30:02+00:00
banner: http://trizindia.org/wp-content/uploads/2014/02/jfwtc.jpg
url: /2014/02/triz-trends-2014-a-conference-in-ge-india/
post_views_count:
  - 4676
bp_admin_notifications_sent:
  - 1
categories:
  - Events

---
[<img class="alignnone size-full wp-image-230" alt="jfwtc" src="http://trizindia.org/wp-content/uploads/2014/02/jfwtc.jpg" width="996" height="359" srcset="http://trizindia.org/wp-content/uploads/2014/02/jfwtc.jpg 996w, http://trizindia.org/wp-content/uploads/2014/02/jfwtc-300x108.jpg 300w, http://trizindia.org/wp-content/uploads/2014/02/jfwtc-624x224.jpg 624w" sizes="(max-width: 996px) 100vw, 996px" />][1]

###### (Image courtesy: http://ge.geglobalresearch.com/locations/bangalore-india/)

John F. Welch Technology Center, Bangalore, INDIA is hosting an event on

_**Time: Thursday &#8211; March 20, 2014 **_

_**Venue: John F. Welch Technology Center, Bangalore, INDIA**_

on the following topic

## **<span style="color: #993366;">TRIZ Trends 2014</span>**

There will be invited speakers from

GE, Gen3 Partners, Reliance, Mindtree, SKF, Cummins, Target, TrizIndia Forum and TRIZ Asia

Registration is FREE.

Please register for the event here &#8211; <a title="https://supportcentral.ge.com/esurvey/takesurvey.asp?p=17778&d=3819761" href="https://supportcentral.ge.com/esurvey/takesurvey.asp?p=17778&d=3819761" target="_blank">https://supportcentral.ge.com/esurvey/takesurvey.asp?p=17778&d=3819761</a>

LAST DATE TO REGISTER : March 5, 2014

Please download the event flyer from here [http://trizindia.org/wp-content/uploads/2014/02/TRIZ-TRENDS-2014-Announcement.pdf][2][[<img class="alignnone size-full wp-image-230" alt="jfwtc" src="http://trizindia.org/wp-content/uploads/2014/02/jfwtc.jpg" width="996" height="359" srcset="http://trizindia.org/wp-content/uploads/2014/02/jfwtc.jpg 996w, http://trizindia.org/wp-content/uploads/2014/02/jfwtc-300x108.jpg 300w, http://trizindia.org/wp-content/uploads/2014/02/jfwtc-624x224.jpg 624w" sizes="(max-width: 996px) 100vw, 996px" />][1]

###### (Image courtesy: http://ge.geglobalresearch.com/locations/bangalore-india/)

John F. Welch Technology Center, Bangalore, INDIA is hosting an event on

_**Time: Thursday &#8211; March 20, 2014 **_

_**Venue: John F. Welch Technology Center, Bangalore, INDIA**_

on the following topic

## **<span style="color: #993366;">TRIZ Trends 2014</span>**

There will be invited speakers from

GE, Gen3 Partners, Reliance, Mindtree, SKF, Cummins, Target, TrizIndia Forum and TRIZ Asia

Registration is FREE.

Please register for the event here &#8211; <a title="https://supportcentral.ge.com/esurvey/takesurvey.asp?p=17778&d=3819761" href="https://supportcentral.ge.com/esurvey/takesurvey.asp?p=17778&d=3819761" target="_blank">https://supportcentral.ge.com/esurvey/takesurvey.asp?p=17778&d=3819761</a>

LAST DATE TO REGISTER : March 5, 2014

Please download the event flyer from here [http://trizindia.org/wp-content/uploads/2014/02/TRIZ-TRENDS-2014-Announcement.pdf][2]][3]

 [1]: http://trizindia.org/wp-content/uploads/2014/02/jfwtc.jpg
 [2]: http://trizindia.org/wp-content/uploads/2014/02/TRIZ-TRENDS-2014-Announcement.pdf "http://trizindia.org/wp-content/uploads/2014/02/TRIZ-TRENDS-2014-Announcement.pdf"
 [3]: http://trizindia.org/wp-content/uploads/2014/02/TRIZ-TRENDS-2014-Announcement.pdf