---
title: Welcome – 500th Member
author: Joseph TK
date: 2012-08-14T03:51:06+00:00
banner: /images/default.jpg
url: /2012/08/welcome-500th-member/
post_views_count:
  - 431
  - 431
categories:
  - Justrizin

---
Dear All TRIZINdian friends

 

I too welcome the 500<sup>th</sup> Member to this group.

One suggestion:

When you are recording the audio for publishing in the site, let the participant prepare the write up and read from it. Otherwise lot of extra noise (like yea.. ) arises and the user will not be able to grasp the content.

 

Best Regards

Joseph  

                  Mindset Solutions India

        S   M   S  I

SHARP Software Development India

Unit 5, Level 3 Innovator, ITPB, ITPL

Whitefield Road  Bangalore  560066

28410645 380 (LL) 28410649(FAX)

9880308433(M)

**P** **Please don&#8217;t print this email unless you really need to. This will preserve trees on our planet.** **b**_&#8220;__Use bicycle_ **_Stay Fit_** _and_ **_Save Environments_**_&#8220;_ 