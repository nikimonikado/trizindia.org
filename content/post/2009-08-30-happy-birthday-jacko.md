---
title: Happy Birthday, Jacko!!!
author: Bala Ramadurai
date: 2009-08-30T06:30:00+00:00
banner: blogs/41-jackpat.jpg
url: /2009/08/happy-birthday-jacko/
post_views_count:
  - 428
  - 428
categories:
  - Justrizin

---
I pay tribute to my teen idol on his birthday and what I find that Jacko and I have something in common. He is an inventor dude as well!

<p style="text-align: left;">
  <img src="blogs/41-jackpat.jpg" alt="" />
</p>

Yes, Michael Jackson has a <a href="http://www.google.com/patents?id=MAUgAAAAEBAJ&printsec=abstract&zoom=4&source=gbs_overview_r&cad=0#v=onepage&q=&f=false" target="_blank">US patent</a> issued to his name!!! Have you have seen his video &#8220;Smooth Criminal&#8221; or atleast this &#8220;leaning&#8221; position? This is his invention. What&#8217;s interesting is the prior art in the patent. One of the prior art cited is from a space application where an astronaut in zero gravity conditions can benefit out of this. Surf boards, skate boards, bicycle pedals, multi-functional personal restraints are all the other prior arts

<p style="text-align:left">
  <img src="http://images.usatoday.com/life/_photos/2009/07/01/smooth-criminal.jpg" />
</p>

Another one of Jacko&#8217;s innovation is his MoonWalk!! For the uninitiated, here goes <a href="http://www.youtube.com/watch?v=s7MmEMrCRfc" target="_blank">Michael Jackson&#8217;s best ever</a> and if you want to learn how to <a href="http://www.youtube.com/watch?v=1EEynvjfljU" target="_blank">MoonWalk</a>. So another space metaphor for Jacko, I think.

Anyways, going back to his patent, the current state in the 1990s was that one could do &#8220;gravity defying&#8221; stunts only in videos and not on live shows. There were wires used like in &#8220;Crouching Tiger, Hidden Dragon&#8221;, but never on stage and you needed people to hook and unhook. Well, in the Beijing Oympics in 2008, there were many such stunts, but one could tell that there were wires.

Thus this invention was aimed at doing gravity defying stunts on the stage. Best way that Jacko thought about was to rig his shoes. The illustrations in the patent give an exact way to do it. Now, I am not sure if he licensed it out and made money.

Another aspect that I wanted to bring out was something to do with <a href="http://en.wikipedia.org/wiki/Rasa_(aesthetics)" target="_blank">Navarasas</a>. That will be the topic of my next blog post.