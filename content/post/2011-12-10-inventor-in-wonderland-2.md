---
title: Inventor in Wonderland
author: Bala Ramadurai
date: 2011-12-10T03:50:07+00:00
banner: /images/default.jpg
url: /2011/12/inventor-in-wonderland-2/
publicize_results:
  - 'a:1:{s:2:"fb";a:1:{i:1563452634;a:2:{s:7:"user_id";s:10:"1563452634";s:7:"post_id";s:13:"2740721408236";}}}'
post_views_count:
  - 317
categories:
  - Podcasts
tags:
  - ideal final result
  - innovation
  - triz
  - trizindiapodcast
  - wonderland

---
**TRIZ podcast series**

Become a member at TRIZIndia.org for FREE to connect with the world&#8217;s experts on inventive problem solving

[youtube http://youtube.com/w/?v=l9d0398v4yY]

Bala Ramadurai, Innovation Facilitator, MindTree

Shankar Venugopal, Innovation Leader, Honeywell, emerging markets

Murali Loganathan, 4 years TRIZ practitioner, across industries in MindTree

Podcast series in TRIZ tools

This is the 1st podcast on Ideal Final Result

Father of TRIZ &#8211; Altshuller

value addition linked to Ideal Final Result (IFR)

What is your experience with IFR?

Shankar says &#8211; Challenge &#8211; mode of thinking into ideality &#8211; common way of thinking is on constraints &#8211; ideality is implementation free

Murali says &#8211; break psychological inertia (PI) &#8211; PI comes in the way of ideation. Think backwards in time &#8211; put constraints in the back. Vision and mission brings grandiose picture. Thinking backward in time, one cannot lie.

Bala quotes Murali &#8211; cannot lie backward in time

Freeland &#8211; no harm, only benefits &#8211; Wonderland with only benefits &#8211; problem already solved. Psychologists have proved that lying is difficult backward in time

Shankar &#8211; SCAMPER &#8211; M &#8211; maximize/minimize

2 steps &#8211; what should be maximized or minimized? zero cost &#8211; barrier to thinking

Murali &#8211; Isak Burkhman &#8211; nothing ideal or final about IFR. Free, perfect, now &#8211; ideal definition for IFR. Use metaphors &#8211; Wonderland is a better metaphor than IFR. Result = Process = goes forward in time.

Heaven or Wonderland &#8211; popular interface

Bala &#8211; Example &#8211; Altshuller &#8211; coolant leakage &#8211; usual way is trial and error. What if coolant is Cindrella? Cindrella shouts out "Here I am". How do you make Cindrella is visible? Turn off the lights. Then solution &#8211; Coolant needs to be colour emitting and says "Hi, I am here". Ideal coolant or ideal way to leak? 🙂

Murali &#8211; Free transport &#8211; A to B? marketing campaigns lets you go from A to B for a cause &#8211; global warming, travelogue writing &#8211; have fun for free.

Shankar &#8211; Why fly? find out the reason for flying. IFR + 5 whys gives a stronger solution. Fundamental problem identified and can arrive at solution.

Bala &#8211; CISCO cashed in on financial meltdown when corporates were slashing down prices &#8211; different domain, different solution

[youtube http://youtube.com/w/?v=GU0w0B2LNEE]

Alternate use of IFR &#8211; visioning. What should my team be doing in 2014, say? who are my customers? what is their IFR? work your way backwards and write down what your team should be doing now to meet that.

Murali &#8211; customer, customer&#8217;s customer, supplier &#8211; all of their IFRs are different. There could be conflicts or contradictions in each others&#8217; IFR. How does one solve this?

Bala &#8211; No compromise. AND is essential. customer wins AND Supplier wins. Inventive principles can help out here. First figure out IFR for all parties and solve the clash

Shankar adds &#8211; clashes are going to be standard practice.

Bala &#8211; IFR + 9-windows can be combined. 9-windows spread in time and space. Mouse &#8211; system, stuff around it &#8211; phone, laptop, screen &#8211; supersystem. Subsystem &#8211; hardware wheel, software, components. All these things back in time and forward in time, what were they? You have 9-windows. System evolves from trackpad, mouse, touchscreen &#8211; moves towards to an IFR. All systems/components move toward IFR.

Murali comments on Worst Final Result (WFR) &#8211;

Heaven: IFR = Hell: WFR.

You find out where systems can go wrong and fix it before things go drastically wrong

Shankar &#8211; take a pen and design a non-working pen and reverse to get a good pen.

Murali &#8211; IFR in groups &#8211; difficult to think of completely ideal future without accidents or lucky breaks (events). Publishing industry &#8211; internet was an accident for publishing. IFR needs an accident or break. electricity and other fundamental innovations can change course of other innovation

Shankar &#8211; Accident can be looked at as an enabler. Author &#8211; IFR &#8211; should be able to write a book and reach the audience instantly. Reader &#8211; read and access books instantly. Internet came in and enabled this. IFR can really help and accidents can be handled or enabled

Bala &#8211; Luckily, internet happened. George Lucas, starwars, was way ahead of his time in terms of graphics. Graphics and sound effects in the &#8217;70s sucked. He later remastered the Starwars series when technology was available

Shankar &#8211; do IFR for everyday objects around yourself before jumping to applying to your tech problems. Make yourself at ease

Murali &#8211; Add "Self" as a prefix to make anything ideal. That is the way to get an IFR

Bala &#8211; take something that you care about and apply IFR for permanent change

See you next time on TRIZ podcast series from TRIZIndia.org