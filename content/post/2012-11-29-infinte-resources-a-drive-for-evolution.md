---
title: 'Infinte Resources: A Drive for Evolution'
author: Tushar Kanikdale
date: 2012-11-29T14:30:00+00:00
banner: /images/default.jpg
url: /2012/11/infinte-resources-a-drive-for-evolution/
post_views_count:
  - 599
  - 599
categories:
  - Trends

---
If Evolution is continous then resources must be infinite. Othewise Evolution should stop at some economic juncture.

Let&#8217;s take an example using a simple economic analysis. Let&#8217;s say If we are left with only 100 units of energy in universe. Making it simple , let&#8217;s say 100 units of petrol. 

 

Here are few assumptions

#Let&#8217;s say 40 units are required to covert to electric car.

#Let&#8217;s say a  car with electric motor delivers 20% higher fuel economy. Or in simple words goes 20% more distance.

#1 unit of petrol drives 1 Km.

Now with 100 units of petrol left, I  have two options to travel 

**#1** 100 Unit Petrol + Mechanical Vehicle = 100 Km   
**#2** 60 Unit petrol (40 are consumed to create electric car) + Hybrid vehicle = 60+ 20% of 60 = 72 KM

So in this case I should never built Electric car as the core benefit (travel) itself has disappeared

 

In another case assume that we have 1000 Units of Petrol. I have two options again

**#1** 1000 Unit of petrol + Mechanical Vehicle = 1000 Km  
**#2** 960 Unit of petrol + Hybrid vehicle = 960 + 20% of 960 = 1152 KM

 See what happens!  The electric vehicle in second case has higher advantage than in first case.

The benefit will keep increasing as cost of coversion (40 in this case) from mechanical to electrical continoulsy comes down.

I am guessing that this may point out that  evolution always happens with the assumption that resources are infinite.