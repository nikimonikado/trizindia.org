---
title: TRIZ India Podcast – TRIZ Journal – Part 1
author: nikimonikado
date: 2015-09-08T07:16:23+00:00
banner: img/c4b447c3-58a9-4af5-a5a2-9b8fdb7b409f.jpg
url: /2015/09/triz-india-podcast-triz-journal-part-1/
post_views_count:
  - 609
enclosure:
  - |
    |
        https://archive.org/download/TRIZIndiaPodcastTRIZJournalPart1/TRIZIndiaPodcast-TRIZ-Journal-1.mp3
        0
        audio/mpeg
        
categories:
  - Podcasts

---
<img src="https://gallery.mailchimp.com/a4fe36e0021432bc3bc87a8ab/images/83727d47-c365-469f-90d0-a37e8bd1c3d7.png" alt="" width="148" height="164" align="none" data-cke-saved-src="https://gallery.mailchimp.com/a4fe36e0021432bc3bc87a8ab/images/83727d47-c365-469f-90d0-a37e8bd1c3d7.png" /><img src="https://gallery.mailchimp.com/a4fe36e0021432bc3bc87a8ab/images/c4b447c3-58a9-4af5-a5a2-9b8fdb7b409f.jpg" alt="" width="115" height="164" align="none" data-cke-saved-src="https://gallery.mailchimp.com/a4fe36e0021432bc3bc87a8ab/images/c4b447c3-58a9-4af5-a5a2-9b8fdb7b409f.jpg" />

Dr. Bala Ramadurai, Co-founder of TRIZ Innovation India (trizindia.org) has a chat with Dr. Ellen Domb, co-founder of PQR group and co-founding editor of TRIZ Journal, in a 2 part series of podcasts on TRIZ Journal 2.0 or better still, TRIZ Journal 3.0.

The second part of the series will involve a conversation with current TRIZ Journal Editors &#8211; Dr. Phil Samuel and Derek Bennington

<a href="https://archive.org/details/TRIZIndiaPodcastTRIZJournalPart1" target="_blank" data-cke-saved-href="https://archive.org/details/TRIZIndiaPodcastTRIZJournalPart1">https://archive.org/details/TRIZIndiaPodcastTRIZJournalPart1</a>

Download the offline version from <a href="https://archive.org/download/TRIZIndiaPodcastTRIZJournalPart1/TRIZIndiaPodcast-TRIZ-Journal-1.mp3" target="_blank" data-cke-saved-href="https://archive.org/download/TRIZIndiaPodcastTRIZJournalPart1/TRIZIndiaPodcast-TRIZ-Journal-1.mp3">https://archive.org/download/TRIZIndiaPodcastTRIZJournalPart1/TRIZIndiaPodcast-TRIZ-Journal-1.mp3</a>
