---
title: EMC Trends 2013 TRIZ overlay
author: Murali Loganathan
date: 2012-12-13T12:09:05+00:00
banner: /images/default.jpg
url: /2012/12/emc-trends-2013-triz-overlay/
post_views_count:
  - 615
  - 615
categories:
  - Trends

---
As [eleven EMC executives offer their predictions of which technologies and trends will transform cloud computing, Big Data and IT security the most in 2013][1], my aim is to find the underlying triz trend and possibly push one more evolution round. I assume a time frame of 1 year, basis is <a title="Permanent Link to 8 TRIZ evolution trends applied to&#xA0;Mobile" href="http://trizindia.wordpress.com/2012/10/01/8-triz-evolution-trends-applied-to-mobile/" rel="bookmark">8 TRIZ evolution trends applied to Mobile</a>, applied on EMC executives predictions.

<table width="633" border="1" cellspacing="0">
  <tr>
    <td width="253">
      <p align="center">
        <span class="font-size-1"><b>Prediction quote (emphasis and few links are mine)</b></span>
      </p>
    </td>
    
    <td width="386">
      <p align="center">
        <span class="font-size-1"><b>My warped explanation of the underlying TRIZ trend</b></span>
      </p>
    </td>
    
    <td width="288">
      <p align="center">
        <span class="font-size-1"><b>What can happen next on this trend</b></span>
      </p>
    </td>
  </tr>
  
  <tr>
    <td width="253">
      <p align="center">
        <span class="font-size-1"><i>an <a title="Intelligence-driven security model" href="http://www.rsa.com/innovation/docs/CISO-RPT-0112.pdf" target="_blank">intelligence-driven security model</a>…will require multiple components including:  a thorough understanding of <b>risk</b>, the use of <b>agile controls</b> based on pattern recognition and predictive analytics, and the use of big data analytics to give context to<b> vast streams of data from numerous sources</b> to produce timely, <b>actionable information</b></i></span>
      </p>
    </td>
    
    <td width="386">
      <p align="center">
        <span class="font-size-1"><b>Law of completeness</b> exemplified with the ENGINE understood as risks and risk related information originating across the board with a TRANSMISSION visualized as streams of information flowing to WORKING UNIT where actions are initiated from the information to contain risks and its effects and having some CONTROL on the above elements including analytics</span>
      </p>
      
      <p align="center">
        <span class="font-size-1"><b>Law of uneven development</b> on above will mean the 4 elements will evolve in different speeds.</span>
      </p>
    </td>
    
    <td width="288">
      <p align="center">
        <span class="font-size-1">Within the same time frame, I feel the engine element will evolve the slowest with not many newer risk categories getting added but we may have to deal with geometrically higher number of information streams, with big data analytics playing a super system role. Governance at working unit level will go through changes as well with many tasks getting automated.</span>
      </p>
    </td>
  </tr>
  
  <tr>
    <td width="253">
      <p align="center">
        <span class="font-size-1"><i>For CIOs, the common theme is “now.” Rapid time to value is the leading driver. In many cases today, the <a href="http://www.informationweek.com/global-cio/interviews/it-has-changed-but-it-budgets-havent/240009244">business unit holds the money and determines the priorities</a>, but they don’t care much about platforms, just the best solution for a specific problem…movement to cloud solutions is only <b>going to escalate</b></i></span>
      </p>
    </td>
    
    <td width="386">
      <p align="center">
        <span class="font-size-1"><b>Transition to Micro Level</b> will mean that instead of a single cloud solution at enterprise level, each department or project will begin its own adoption independently. Budgeting and allocation as always and the experiments and trials of solutions, both in size and time will shift to micro level. i.e. smaller projects with shorter time cycles to try. You can check the free trial offers from HP, Google, AWS, vmWare to see how micro this has actually become already.</span>
      </p>
      
      <p align="center">
        <span class="font-size-1">Adoption rates will most likely be at the beginning of the sharp rise in the S curve (with X axis linear time, Y axis cumulative % of adoption of a cloud solution).</span>
      </p>
    </td>
    
    <td width="288">
      <p align="center">
        <span class="font-size-1">IT will begin its delayed policy making role later in the year with governance as the central goal after many micro-level cloud solutions get adopted in the enterprise.  Possibly negotiate with popular choice vendors for supporting internal laggard/late adopter needs.</span>
      </p>
    </td>
  </tr>
  
  <tr>
    <td width="253">
      <p align="center">
        <span class="font-size-1"><i>The transformation to hybrid cloud environments, and the need to move data between corporate IT data centers and service providers, will accelerate. The concepts of both data and application mobility to enable organizations to move their virtual applications will become the norm.</i></span>
      </p>
      
      <p align="center">
        <span class="font-size-1"><i>Already the roles and responsibilities of the different channel entities are blurring. <b>SIs are becoming resellers; resellers are becoming service providers; and even end users are becoming service providers</b>. Over the next three years, it is probable that the traditional mix of end-user, channel, alliance and service organizations will change, merge or disappear.</i></span>
      </p>
      
      <p align="center">
        </td> 
        
        <td width="386">
          <p align="center">
            <span class="font-size-1"><b>Transition to Super system</b> will mean aggregation and unification of the entire service procurement including licensing, integration, migration, channel management etc</span>
          </p>
        </td>
        
        <td width="288">
          <p align="center">
            <span class="font-size-1">More sub systems and services of the past will move to the super system and will be on the path to become ubiquitous.Most partner ecosystems these days already include license offers, marketing support, education support and account management for partners. Examples could be <a href="http://www.google.co.in/enterprise/gep/overview.html">Google</a>, <a href="http://www.vmware.com/partners/programs/">VMware</a> partner programs</span>
          </p>
        </td></tr> 
        
        <tr>
          <td width="253">
            <p align="center">
              <span class="font-size-1"><i>The <a href="http://ibmdatamag.com/2012/10/true-hadoop-standards-are-essential-for-sustaining-industry-momentum-part-1/">emergence of the Hadoop data ecosystem</a> has made cost-effective storage and processing of petabytes of data a reality.  Innovative organizations are leveraging these technologies to create an entire new class of real-time data-driven applications</i></span>
            </p>
            
            <p align="center">
              <span class="font-size-1"><i>IT will continue to see abstractions with more intelligence in the data center moving to a software control plane that uses Web-based technologies to access compute, networking, and storage resources as a whole (e.g. software-defined data center). Cloud model tenets like efficiency and agility will expand to include simplicity as data centers look for <b>easier ways to consume technology.</b></i></span>
            </p>
          </td>
          
          <td width="386">
            <p align="center">
              <span class="font-size-1"><b>Law of Conduction </b>will mean emergence of standards for data and application portability this year.  <a href="http://gigaom.com/cloud/why-the-days-are-numbered-for-hadoop-as-we-know-it/">de-facto standard is my expectation</a> and <a href="http://www.computerweekly.com/news/2240164009/EC-sets-out-strategy-for-EU-cloud-data-and-standards">de-jure standards especially from EU region is also possible</a> as governments can step in to decide and declare norms from the “jungle of standards”</span>
            </p>
            
            <p align="center">
              <span class="font-size-1"><b>Law of Harmonization</b> will necessitate smooth transitions at application and portfolio level and will mean newer services especially in migration and testing to make sure business continuity.</span>
            </p>
          </td>
          
          <td width="288">
            <p align="center">
              <span class="font-size-1">Simplicity, agility, portability testing, or <<other cloud tenet>> services may emerge</span> as key sellers from offshore.
            </p>
          </td>
        </tr></tbody> </table> 
        
        <div id="jp-post-flair" class="sharedaddy sd-rating-enabled sd-like-enabled sd-sharing-enabled">
          <div class="sd-block sd-rating">
            Cross posting from <a href="http://cognitivenoise.wordpress.com/2012/12/11/emc-trends-2013-triz-overlay/">http://cognitivenoise.wordpress.com/2012/12/11/emc-trends-2013-triz-overlay/</a> 
          </div>
        </div>

 [1]: http://reflectionsblog.emc.com/2012/11/predictions-of-top-technology-trends-for-2013.html