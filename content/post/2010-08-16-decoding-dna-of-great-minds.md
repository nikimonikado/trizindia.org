---
title: Decoding DNA of Great Minds
author: Prashant Yeshwant Joglekar
date: 2010-08-16T15:30:36+00:00
banner: /images/default.jpg
url: /2010/08/decoding-dna-of-great-minds/
post_views_count:
  - 402
  - 402
categories:
  - Justrizin

---
<p class="MsoNormal" style="MARGIN: 0in 0in 10pt; TEXT-ALIGN: justify">
  <span style="FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: 'Arial','sans-serif'">Mathematics to most of us is a subject to be hated and loved at the same time. On one hand, the enmity with mathematics is because we had to learn, memories a pattern / method taught to solve a problem in anticipation of the problems we might face in our real life when we are out in the world from our safe cocoons. Although I never applied math to my real life problems as they seem to be more complex to solve with the scant knowledge of mathematics I possess.</span> <span style="FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: Wingdings; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial; mso-char-type: symbol; mso-symbol-font-family: Wingdings"><span style="mso-char-type: symbol; mso-symbol-font-family: Wingdings">J</span></span><span style="FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: 'Arial','sans-serif'">. On the other hand we were forced to love mathematics because it helped us score good marks and gave us a chance to progress by choosing lucrative professions. Answers were more important than the methods with which they are derived because you can’t get an answer with a different method than the one which is already established.</span>
</p>

<p style="TEXT-ALIGN: justify">
  <span style="FONT-SIZE: 11pt; FONT-FAMILY: 'Arial','sans-serif'"><font color="#000000">In my wildest of the dreams I never thought of seeing a play on the work & life of a mathematician, but I did see a very different play “The Disappearing Number” based on the theme of passionate collaboration between mathematics genius Srinivas Ramanujan (1887-1920) and his mentor, guide a Cambridge Scholar Godfrey Harold Hardy. (G.H.Hardy)</font></span>
</p>

<p style="TEXT-ALIGN: justify">
  <span style="FONT-SIZE: 11pt; FONT-FAMILY: 'Arial','sans-serif'"><font color="#000000">Ramanujan had no formal training in pure mathematics and was introduced to the subject when he was 10. He had a difficult childhood and adulthood. But that did not stop him pursuing his passion & the genius went on to make the greatest contribution in mathematical analysis, number theory, infinite series & continued fractions. For continuing with what he knew the best he had to take up a job at the Madras Port Trust as Class 3, Grade IV clerk, making merely 30 rupees a month. His seniors however encouraged him and gave him space to pursue his passion. At that time with guidance of some of the seniors he wrote to several famous mathematicians about his work which no one understood for lack of explanation / proof and was not considered for publication. After receiving several rejections he finally wrote to Cambridge mathematician G.H.Hardy. Initially he had also ignored his correspondence for lack of sound explanation of his work until he saw one formula that a struck a chord with him. To the uninitiated it seemed to make no sense at all. It read something like this</font></span>
</p>

<p style="TEXT-ALIGN: center" align="center">
  <span style="FONT-SIZE: 11pt; FONT-FAMILY: 'Arial','sans-serif'"><font color="#000000">1+2+3+4+5+….. = -1/ 12</font></span>
</p>

<p style="TEXT-ALIGN: justify">
  <span style="FONT-SIZE: 11pt; FONT-FAMILY: 'Arial','sans-serif'"><font color="#000000">It was this very formula that provided Hardy with his first inkling that Ramanujan was a far from a crank. Hardy knew that there were sophisticated mathematical techniques developed in Germany that has spread widely. He realized that Ramanujan must have single-handedly reconstructed them. It is for exploration of such mysterious things has resulted in making of this play.</font></span>
</p>

<p style="TEXT-ALIGN: justify">
  <span style="FONT-SIZE: 11pt; FONT-FAMILY: 'Arial','sans-serif'"><font color="#000000">Ramanujan went to Cambridge to work with Hardy against his Brahmin beliefs. Hardy was fascinated by his work & kept himself busy preparing explanation for his proofs while Ramanujan kept on adding several more during the day. It was exhausting for Hardy to work like that and he wondered from where Ramanujan gets this knowledge to come up with a new theorem that frequently. <span style="COLOR: black; mso-themecolor: text1">Ramanujan credited his acumen to his</span></font></span> <a title="Kuladevata" href="http://en.wikipedia.org/wiki/Kuladevata"><span style="FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: 'Arial','sans-serif'; TEXT-DECORATION: none; mso-themecolor: text1; text-underline: none">family Goddess</span></a><span style="FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: 'Arial','sans-serif'; mso-themecolor: text1">,</span> <a title="Namagiri" href="http://en.wikipedia.org/wiki/Namagiri"><span style="FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: 'Arial','sans-serif'; TEXT-DECORATION: none; mso-themecolor: text1; text-underline: none">Namagiri</span></a> <span style="FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: 'Arial','sans-serif'; mso-themecolor: text1">of</span> <a title="Namakkal" href="http://en.wikipedia.org/wiki/Namakkal"><span style="FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: 'Arial','sans-serif'; TEXT-DECORATION: none; mso-themecolor: text1; text-underline: none">Namakkal</span></a><span style="FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: 'Arial','sans-serif'; mso-themecolor: text1">, and looked to her for inspiration in his work. He often said, &#8220;An equation for me has no meaning, unless it represents a thought of God” </span>
</p>

<p style="TEXT-ALIGN: justify">
  <span style="FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: 'Arial','sans-serif'; mso-themecolor: text1">Unfortunately he had a tragic end at an early age of 32 with several liver infections; he spoilt his health due to his strict vegetarianism and spent many days of starvation and ate only rice & fruits occasionally. This part of story rides the audience on an emotional rollercoaster. Today Ramanujan’s mathematical discoveries are used in crystallography & string theory which explains how everything in the universe is connected with each other.</span>
</p>

<p style="TEXT-ALIGN: justify">
  <span style="FONT-SIZE: 11pt; FONT-FAMILY: 'Arial','sans-serif'"><font color="#000000">String of thoughts sparked off as I took the train back home after the play, one of them being the genesis of the work that these great minds do. Why they only are able to do what they do & not many of us. Three other genius minds struck to me at that very moment Genrich Altshuller- Father of TRIZ (Science of Systematic Innovation), Micheal Faraday inventor of electromagnetic theory & electromagnetism one of the first forces that exist in nature & the last one is Steve Jobs inventor of devices like smart phones i-pod, i-pad. What is common in all of them? I thought</font></span>
</p>

<p style="TEXT-ALIGN: justify">
  <font color="#000000"><b style="mso-bidi-font-weight: normal"><span style="FONT-SIZE: 11pt; FONT-FAMILY: 'Arial','sans-serif'">Altshuller</span></b><span style="FONT-SIZE: 11pt; FONT-FAMILY: 'Arial','sans-serif'">, a patent reader in the patent office of USSR was imprisoned by Stalin for political reasons when he shared his ideas of decoding innovation with fixed number of patterns. His communication with the dictator became a reason for his captivity, as dictators don’t like IDEAS they like IDIOTS</span><span style="FONT-SIZE: 11pt; FONT-FAMILY: Wingdings; mso-ascii-font-family: Arial; mso-hansi-font-family: Arial; mso-bidi-font-family: Arial; mso-char-type: symbol; mso-symbol-font-family: Wingdings"><span style="mso-char-type: symbol; mso-symbol-font-family: Wingdings">J</span></span><span style="FONT-SIZE: 11pt; FONT-FAMILY: 'Arial','sans-serif'">. He enthused his inmates who also were intellectuals with his ideas, after setting free they together saw a dream of decoding innovation and mined nearly 200,000 patents to make common rules that lead to a science of predictable innovation. Altshuller did not make fortune in his lifetime with what he did but left all of us a treasure to innovate with. (</span></font><a href="http://innovationnukkad.blogspot.com/search?q=remembering+altshuller"><span style="FONT-SIZE: 11pt; FONT-FAMILY: 'Arial','sans-serif'">http://innovationnukkad.blogspot.com/search?q=remembering+altshuller</span></a><span style="FONT-SIZE: 11pt; FONT-FAMILY: 'Arial','sans-serif'"><font color="#000000">)</font></span>
</p>

<p class="MsoNormal" style="MARGIN: 0in 0in 10pt; TEXT-ALIGN: justify">
  <b style="mso-bidi-font-weight: normal"><span style="FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: 'Arial','sans-serif'">Michael Faraday</span></b> <span style="FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: 'Arial','sans-serif'">was born to working class parents and eked out a meager existence as an apprentice bookbinder in the early 1800. The young Faraday was fascinated by the enormous breakthroughs in uncovering the mysterious properties of two new forces electricity & magnetism. He joined Prof. Humphrey Davy and in his absence went on to create series of stunning breakthroughs that led to the creation of generators. Empty space was not empty to Faraday at all they were forced lines. His was not trained in mathematics but his notebook was full of hand drawn force of lines. One day he found while moving a magnet over a coil that a current is generated so the lines of forces were not useless but had the power to generate electric current. What you are reading now is because of Faraday’s invention. His invention energized the world and changed the course of world’s civilization. Faraday’s DNA was imagination, experimentation and observation.</span>
</p>

<p class="MsoNormal" style="MARGIN: 0in 0in 10pt; TEXT-ALIGN: justify">
  <span style="FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: 'Arial','sans-serif'">I have nothing more to say about <b style="mso-bidi-font-weight: normal">Steve Jobs</b> as many of us use his creations i-pod, i-phone & i-pad to conduct our daily chores. To describe more eloquently about the form of creativity that we talked about so far I use his own expression</span>
</p>

<p class="MsoNormal" style="MARGIN: 0in 0in 10pt; TEXT-ALIGN: justify">
  <i style="mso-bidi-font-style: normal"><span style="FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: 'Arial','sans-serif'">When you ask creative people how did they do something, they feel little guilty because they didn’t really do it, they just saw something. It seemed obvious to them after a while that’s because they were able to connect experiences they have had and synthesize new things. And the reason they were able to do that was that they have had more experiences or they have thought more about their experiences than other people.</span></i>
</p>

<p class="MsoNormal" style="MARGIN: 0in 0in 10pt; TEXT-ALIGN: justify">
  <i style="mso-bidi-font-style: normal"><span style="FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: 'Arial','sans-serif'">Unfortunately, that’s too rare a commodity. A lot of people in our industry haven’t had very diverse experiences, so they don’t have enough dots to connect and they will end up with very linear solutions without a broad perspective on the problem.</span></i>
</p>

<p class="MsoNormal" style="MARGIN: 0in 0in 10pt; TEXT-ALIGN: justify">
  <i style="mso-bidi-font-style: normal"><span style="FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: 'Arial','sans-serif'">The broader one understands of the human experience, the better design we will have.</span></i>
</p>

<p style="TEXT-ALIGN: justify">
  <span style="FONT-SIZE: 11pt; FONT-FAMILY: 'Arial','sans-serif'"><font color="#000000">Why these minds were different & creative than the rest I seem to have found an answer in G.H.Hardy’s book “Mathematical Apology” (here the word apology needs to be taken as explanation and not as regret) there are two things that shape the work of great minds the first of which is intellectual curiosity and the other being the pride that they take in their own passion which has a ‘pukka’ connection with their soul. They see several patterns of things they are passionate about and connect those patterns differently to come up with some new creation each time. All these creations are against all odds. So greater is the difficulty the more beautiful their creation becomes. They follow a non-linear path of thinking.</font></span>
</p>

<p class="MsoNormal" style="MARGIN: 0in 0in 10pt; TEXT-ALIGN: justify">
  <span style="FONT-SIZE: 11pt; COLOR: black; FONT-FAMILY: 'Arial','sans-serif'">To conclude I would say every organization has at least few of its own Ramanujans, Faradays what probably is needed is equal proportion of Hardy & Maxwells to create a new knowledge and value.</span>
</p>