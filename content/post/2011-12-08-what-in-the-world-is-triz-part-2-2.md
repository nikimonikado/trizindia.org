---
title: What in the world is TRIZ? Part 2
author: Bala Ramadurai
date: 2011-12-08T05:29:34+00:00
banner: http://cdnimg.visualizeus.com/thumbs/a1/72/twitter,cartoon,communication,evolution,funny,joke-a172a30347ffa67544455ec0669a6f7e_h.jpg
url: /2011/12/what-in-the-world-is-triz-part-2-2/
publicize_results:
  - 'a:1:{s:2:"fb";a:1:{i:1563452634;a:2:{s:7:"user_id";s:10:"1563452634";s:7:"post_id";s:13:"2727764484321";}}}'
enclosure:
  - |
    |
        http://www.archive.org/download/WhatInTheWorldIsTrizPart2/TRIZIndia-WhatintheworldisTRIZ-part2.mp3
        23409824
        audio/mpeg
        
post_views_count:
  - 449
categories:
  - Podcasts
tags:
  - business
  - consultant
  - evolution
  - innovation
  - learn
  - method
  - teach
  - triz
  - trizindiapodcast

---
![The evolution of communication / Mike Keape][1]
  
<span style="font-size:xx-small;"><em>Image Courtesy:<a href="http://vi.sualize.us/view/a172a30347ffa67544455ec0669a6f7e/" target="_blank"><em>http://vi.sualize.us/view/a172a30347ffa67544455ec0669a6f7e/</em></a></em></span>

Dr. Ellen Domb, Prakasan Kappoth, Murali Loganathan and Bala Ramadurai discuss the finer aspects of TRIZ. A continuation from <http://www.archive.org/details/WhatInTheWorldIsTrizPart1>. The participants discuss about system evolution, system operators, creative problem solving, internal vs external consultant. . [http://www.trizindia.org][2]

[Listen to the podcast here][3]

 [1]: http://cdnimg.visualizeus.com/thumbs/a1/72/twitter,cartoon,communication,evolution,funny,joke-a172a30347ffa67544455ec0669a6f7e_h.jpg
 [2]: http://www.trizindia.org/
 [3]: http://www.archive.org/download/WhatInTheWorldIsTrizPart2/TRIZIndia-WhatintheworldisTRIZ-part2.mp3