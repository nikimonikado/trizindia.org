---
title: 'TRIZ India podcast #3 – 9-Windows'
author: Bala Ramadurai
date: 2011-08-06T01:15:55+00:00
banner: blogs/15-9windows.JPG?width=750
url: /2011/08/triz-india-podcast-3-9-windows/
post_views_count:
  - 594
  - 594
categories:
  - Justrizin
  - Videos

---
Bala Ramadurai, Murali Loganathan, Prakasan Kappoth and Shankar Venugopal discuss &#8220;thinking out of the box&#8221; and &#8220;big picture thinking&#8221; citing examples of BMW, software development, dispensing cash on the third podcast of the series.

 

<font size="3"><a href="blogs/14-9windows.JPG" target="_self"><img src="blogs/15-9windows.JPG?width=750" width="750" class="align-full" /></a><br /></font>