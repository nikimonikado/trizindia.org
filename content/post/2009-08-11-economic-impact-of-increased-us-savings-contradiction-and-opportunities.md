---
title: Economic impact of increased US savings – Contradiction and opportunities?
author: Prakasan K
date: 2009-08-11T11:30:43+00:00
banner: /images/default.jpg
url: /2009/08/economic-impact-of-increased-us-savings-contradiction-and-opportunities/
post_views_count:
  - 269
  - 269
categories:
  - Justrizin

---
<a href="http://www.mckinseyquarterly.com/The_economic_impact_of_increased_US_savings_2327" target="_blank">An article published by McKinsey</a> (March ’09) quarterly shows this interesting (to me at least) study on the increasing US consumer savings in the last couple of quarters. Well, in a time when we see the bad news all around, the tendency for the rest of us earning are naturally towards saving it for the rainy day ahead. Apparently this article also put some lights on the debt Vs income ratio and justifies the reason behind this savings trend.

However, my interest here is abstracting the contradictions which I think are the contradictions (you may find a different contradiction, which is ok, and good) from the two important stakeholder’s perspective, especially when the economists pointing the reasons behind the recession is the lack of (or reduced) consumer spending, not just limited to buying a new home or car, but down to the spending on their vacations, purchasing goods, and even food.

I, as a consumer, first want to save money to spend; from a business perspective, they like me to spend money to keep me in the job. (So that I can save). An interesting vicious circle and the contradiction may be;

As a consumer – Improve my savings Vs As a business – Reduced spending

If I, as a consumer focus only on the savings today, the existence of the businesses will be in question. They want me to spend, but I’m too concerned about my future if I don’t save.

There is also a physical contradiction for the consumer: I Save Vs Do Not Save – I want to save for the better and make sure that I can survive during this period, but I don’t want to save all my money (or want to spend) to make the businesses thrive so that I have my job!

According to TRIZ, when we are in a contradictory situation, there is an opportunity for innovation without compromising both the stakeholders need. I have no idea how my friends actually “Save” money back there, but presume they save using various financial instruments available, including, but not limited to, stocks, properties, mutual funds, pension funds, and even bank savings. (I may be completely wrong for bank related savings since what we have seen in the last few months of abysmal collapse of banking behemoths could have triggered another new trend in the way people save – Still an opportunity for innovation if you can identify that trend).

Translating the above contradictions in TRIZ terms, you have some thinking principles to brainstorm for ideas. I selected the improvement parameter from the classical TRIZ as “Reliability”, because I can dependent on my savings for securing my future. The obvious worsening parameter to me here is the “harmful side effects generated”, in this case is reduced spending, because I’m keen on saving.

With the following inventive principles, can you generate some ideas for the consumers to save money, and at the same time they spend money so that the businesses can run?
  
35. Parameter changes
  
\# Change a system’s object’s physical state
  
\# Change the concentration or consistency.
  
\# Change the degree of flexibility.
  
\# Change the temperature.
  
2. Taking out
  
\# Separate an interfering part or property from an object, or single out the only necessary part (or property) of an object.
  
40. Composite materials
  
\# Change from uniform to composite (multiple) materials.
  
26. Copying
  
\# Instead of an unavailable, expensive, fragile object, use simpler and inexpensive copies.
  
\# Replace an object, or process with optical copies.
  
\# If visible optical copies are already used, move to infrared or ultraviolet copies.

You may select another set of improving and worsening parameter to identify more principles to ideate. The quantity of ideas will certainly lead to quality of your end results. Oh, and do not forget to look at the “resources” available to you if you are serious about coming out with some innovations within your business domain