---
title: Can this be Innovation?
author: Kumar Rajamani
date: 2009-11-11T09:09:55+00:00
banner: blogs/32-Shower_Innovation.jpg
url: /2009/11/can-this-be-innovation/
post_views_count:
  - 274
  - 274
categories:
  - Justrizin

---
<p style="text-align: left;">
  <a class="noborder" href="blogs/31-Shower_Innovation.jpg" target="_blank"><img src="blogs/32-Shower_Innovation.jpg" alt="" /></a>
</p>

I had recently been to a star hotel in Chennai
  
(Name not revealed as it a public blog)
  
This was where I was staying when attending a workshop

The hotel room was good but what triggered me the most was the shower

It took me close to 10-15 mins to know how to operate it
  
And operating it left water spraying from all different unexpected corners

How does one identify function of each of the ten knobs?
  
How to get simple functionality was made literally impossible.

The last to expect was that there was no simple outlet to fill a
  
bucket even if I wanted

Finally if one does manage to operate the shower then one can have a
  
fancy shower with water jets from 6 different jets.
  
But if one wanted a normal simple shower one is left puzzling

I wonder how somebody advanced in age would learn to operate it

This left me thinking can this be considered Innovation, Or could one take this
  
example as what Innovation should not lead to?

Kumar