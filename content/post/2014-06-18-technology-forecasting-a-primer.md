---
title: Technology Forecasting – A Primer
author: nikimonikado
date: 2014-06-18T09:44:01+00:00
banner: /images/default.jpg
url: /2014/06/technology-forecasting-a-primer/
post_views_count:
  - 2829
categories:
  - Justrizin

---
I promised a [while ago][1] that I&#8217;d publish something about Technology Forecasting, which is my current occupation in Milan, Italy. I presented a talk on technology forecasting in Politecnico di Milano. I have attached the slideshow to this post. Enjoy!

Please feel free to download the presentation [google-drive-embed url=&#8221;https://docs.google.com/uc?id=0B3EZPxN\_ojAfTV9aazdCRWx1Rms&export=download&#8221; title=&#8221;2013\_03\_08\_Bala\_Ramadurai-Technology Forecasting.pptx&#8221; icon=&#8221;https://ssl.gstatic.com/docs/doclist/images/icon\_10\_powerpoint\_list.png&#8221; style=&#8221;download&#8221;]

[google-drive-embed url=&#8221;https://docs.google.com/presentation/d/1fsSr-IG9tO99z5Zb0eht2nTwcWtvfnOaJjsZVfl2hvI/preview&#8221; title=&#8221;2013\_03\_08\_Bala\_Ramadurai-Technology Forecasting&#8221; icon=&#8221;https://ssl.gstatic.com/docs/doclist/images/icon\_11\_presentation_list.png&#8221; width=&#8221;100%&#8221; height=&#8221;400&#8243; style=&#8221;embed&#8221;]

&nbsp;

 [1]: http://trizindia.org/2014/02/the-history-of-triz/ "The history of TRIZ"