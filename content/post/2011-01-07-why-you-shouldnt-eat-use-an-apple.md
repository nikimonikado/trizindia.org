---
title: Why you shouldn’t eat (use) an Apple?
author: Prakasan K
date: 2011-01-07T12:37:51+00:00
banner: /images/default.jpg
url: /2011/01/why-you-shouldnt-eat-use-an-apple/
post_views_count:
  - 362
  - 362
categories:
  - Justrizin

---
<div style="text-align: left; margin-top: 0px; padding: 0px;">
  <p>
    <font face="Calibri">If you are an Apple fan from India, this is for you, and I wouldn’t mind you shout at me. I <u>used</u> to be a great Apple fan. I used the Mac system in 2002 time period and ever since in love with their products, company for their innovations, and later Steve Jobs as a spatial thinker (also because I’m a spatial thinker) after reading his biography. Then the news came, Apple coming out with a phone. When I saw the prototype of the phone in internet, I became an obsessive lover of Apple. Not just because the iPhone UI and amazing design, but more happy because the iPhone became a product I could use for my TRIZ, innovation and design thinking sessions.</font>
  </p>
  
  <p>
    <font face="Calibri">TRIZ, because the phone used a classic concept of “Trimming”, a concept in TRIZ to remove an object performing a useful function, but still have the function achieved without additional cost by something else. And the iPhone did that beautifully by removing the keys from the phone, and use the screen as touch screen (don’t forget the touch screens were in market eons ago, but Apple did what revolutionized now). Then their ability to look at the Super-system, like iTunes, and Apple Dev forum, SDK etc..  So, now you know why I was in love with Apple!</font>
  </p>
  
  <p>
    <font face="Calibri">And then iPhone launched sometime in 2007; I was ready to burn my pocket to get that phone, and started using my social networking skills by connecting friends and colleagues in US (so that they could be a carrier), but Apple disappointed me by locking them with AT&T. (Good Side effect is that I became so well connected with people even before the Facebook era) Well, I thought it is ok; they might launch iPhone in India sometime soon, and I will legally buy one in India. Then there was a long wait &#8211; they even launched iPhones in Indonesia, and I was dreaming, using iPhone photos and videos as examples for my innovation talk, jealously looking at those kids joined straight from campus flashing an iPhone. Finally news was out, Airtel and Vodafone are launching iPhone in India. Damn, I spent 5 bucks for an SMS register and get more update on the first day.. again wait, wait.. Then the bomb, 36K for an iPhone in India compared to $150 in US!!! Gruhhhhh</font>
  </p>
  
  <p>
    <font face="Calibri">Well, that is when I realized iPhone is just a looking good stuff but there are so many features missing what my Rs.5000 Samsung could provide (like Bluetooth, copy paste etc)!! I convinced myself that the price will come down after sometime, because the technology evolution is always in a direction towards increasing ideality as in TRIZ, which means more features, with decreasing price; and also the competition will make Apple to bring down the cost.</font>
  </p>
  
  <p>
    <font face="Calibri">Come 2009, and 2010.. Apple launched 3 more versions of iPhone and still thinks there is no country called India, where an impatient generation Y is ready to embrace the best, ready to splurge consumers at the middle class, a country of growing purchasing power at all levels. As if Apple doesn’t like the dollars coming from India or India is still a 3<sup>rd</sup> world country where people can’t afford an iPhone..</font>
  </p>
  
  <p>
    <font face="Calibri">Now I’m ready to forget that damn iPhone, I have Galaxy S, and other 100’s of touch screen phone better or close to iPhone for a cost that I don’t feel cheated; but again, Steve – you shouldn’t be doing this to a country which will define the future of consumer spending for companies like yours. You launched iPad all over the world, and India is still waiting.. Don’t you think that selling 100K of your iPhones and iPads to Indian consumers will not increase your top line growth? Perhaps your share holders will benefit few pennies from the Apple stocks they are holding???</font>
  </p>
  
  <p>
    <font face="Calibri">And now I’m ready to forget iPads too; all other manufacturers are launching their Pads and Tablets in India at the same time they launch in US; and I have Indian startup companies like <a href="http://notionink.in/">NotionInk</a> developing tablets better than iPad’s. Go and take a look at them at the CES 2011 show. As a tech savy Indian, I hate Apple, iPhones, and iPads, and whatever like glass-free 3D TV you may launch, Steve. I have more innovators like NotionInk guys developing products that will compete with Apple products in near future..</font>
  </p>
  
  <p>
    <em><font face="Calibri">Folks, anyone wants to sell an iPhone, please contact me…</font></em>
  </p>
  
  <p>
    <font face="Calibri">This posting is pun intended post to describe the technology development, little TRIZ, and of course to share the news about a company called NotionInk and the products they develop in India..</font>
  </p>
</div>