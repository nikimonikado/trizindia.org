---
title: Altshuller, Science Fiction and TRIZ
author: Karthikeyan Iyer
date: 2010-06-24T05:20:21+00:00
banner: /images/default.jpg
url: /2010/06/altshuller-science-fiction-and-triz/
post_views_count:
  - 388
  - 388
categories:
  - Justrizin

---
<span class="Apple-style-span" style="color: rgb(51, 51, 51); font-family: Georgia, Times, serif;">Altshuller not only classified inventions (based on study of patents), he also classified science fiction concepts, although in a slightly different manner! Not a surpise at all, after all inventions are science fiction till they become reality!<br /></span>

<blockquote style="margin-top: 1em; margin-right: 20px; margin-bottom: 1em; margin-left: 20px;">
  <p>
    <span class="Apple-style-span" style="color: rgb(51, 51, 51); font-family: Georgia, Times, serif;">In order to study the mechanism of SF, it was necessary first of all to collect ideas scattered in thousands of works, to classify them, and then compile A Register of SF Ideas. This work has been in progress over several years with the result that now there are some 3000 ideas in the Register, catalogued according to classes, sub-classes, groups, and sub-groups. Up to this, no one had attempted to collect SF ideas systematically. But the Register served as a research instrument to help in the examination of the sophisticated and curious process of how SF ideas originate.</p> 
    
    <p>
      It turned out that in the evolution of any SF theme (&#8220;cosmic voyages,&#8221; &#8220;contact with extra-terrestrial civilisations,&#8221; etc.} there are four distinct categories of SF ideas:I) ideas based on a single object, with a certain fantastic result; II) ideas based on several objects, which add up to a rather different fantastic result; III) ideas leading to similar results, but obtained without an object; and IV) ideas based on a set of conditions that do not require these results &#8211; Altshuller.</span>
    </p></blockquote> 
    
    <p>
      <span class="Apple-style-span" style="color: rgb(51, 51, 51); font-family: Georgia, Times, serif;">Sounds familiar? Journey into Altshuller&#8217;s mind at <a href="http://www.altshuller.ru/world/eng/science-fiction5.asp" style="color: rgb(0, 0, 0); text-decoration: underline;">http://www.altshuller.ru/world/eng/science-fiction5.asp</a></span>
    </p>