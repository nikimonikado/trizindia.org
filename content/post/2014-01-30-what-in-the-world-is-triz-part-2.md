---
title: What in the world is TRIZ? Part 2
author: nikimonikado
date: 2014-01-30T09:16:36+00:00
banner: http://cdnimg.visualizeus.com/thumbs/a1/72/twitter,cartoon,communication,evolution,funny,joke-a172a30347ffa67544455ec0669a6f7e_h.jpg
url: /2014/01/what-in-the-world-is-triz-part-2/
enclosure:
  - |
    |
        http://www.archive.org/download/WhatInTheWorldIsTrizPart2/TRIZIndia-WhatintheworldisTRIZ-part2.mp3
        161
        audio/mpeg
        
  - |
    |
        http://www.archive.org/download/WhatInTheWorldIsTrizPart2/TRIZIndia-WhatintheworldisTRIZ-part2.mp3
        161
        audio/mpeg
        
post_views_count:
  - 2220
  - 2220
categories:
  - Justrizin

---
![The evolution of communication / Mike Keape][1]



Dr. Ellen Domb, Prakasan Kappoth, Murali Loganathan and Bala Ramadurai discuss the finer aspects of TRIZ. A continuation from <http://www.archive.org/details/WhatInTheWorldIsTrizPart1>. The participants discuss about system evolution, system operators, creative problem solving, internal vs external consultant. . [http://www.trizindia.org][2]

[Listen to the podcast here][3]

<span style="font-size:xx-small;"><em>Image Courtesy:<a href="http://vi.sualize.us/view/a172a30347ffa67544455ec0669a6f7e/" target="_blank"><em>http://vi.sualize.us/view/a172a30347ffa67544455ec0669a6f7e/</em></a></em></span>

 [1]: http://cdnimg.visualizeus.com/thumbs/a1/72/twitter,cartoon,communication,evolution,funny,joke-a172a30347ffa67544455ec0669a6f7e_h.jpg
 [2]: http://www.trizindia.org/
 [3]: http://www.archive.org/download/WhatInTheWorldIsTrizPart2/TRIZIndia-WhatintheworldisTRIZ-part2.mp3