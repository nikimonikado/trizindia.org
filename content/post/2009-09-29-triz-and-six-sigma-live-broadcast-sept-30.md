---
title: TRIZ and SIx Sigma live broadcast Sept. 30
author: Ellen Domb
date: 2009-09-29T18:48:28+00:00
banner: /images/default.jpg
url: /2009/09/triz-and-six-sigma-live-broadcast-sept-30/
post_views_count:
  - 277
  - 277
categories:
  - Justrizin

---
<a href="http://www.blogtalkradio.com/QualityConversations/2009/09/30/Ellen-Domb--What-is-TRIZ-Its-Relationship-to-Six-Sigma" target="_blank">Event details</a>. The timing is perfect for participation from TRIZIndia&#8211;this is a live internet &#8220;radio&#8221; broadcast on TRIZ and Six Sigma (or any improvement method.) You can participate now, or listen/download later&#8211;which is better learning AND more fun? Thanks in advance!