---
title: The history of TRIZ
author: nikimonikado
date: 2014-02-07T09:26:33+00:00
banner: /images/default.jpg
url: /2014/02/the-history-of-triz/
post_views_count:
  - 3508
  - 3508
bp_admin_notifications_sent:
  - 1
  - 1
categories:
  - Justrizin

---
I am right now in Milan, Italy in close proximity to some of the people who have dedicated their careers to helping others solve problems.

One of them is my friend and colleague, <a title="Dmitry Kucharavy" href="http://seecore.org/id4.htm" target="_blank">Dmitry Kucharavy</a>.

The simple question I asked him about 2 weeks ago was &#8220;What is the history of TRIZ?&#8221;.Dmitry replied, &#8220;I am sending you a bunch of links, where I have attempted to answer the exact same question that everybody asks me on a routine basis.&#8221;

So the following is an excerpt of what Dmitry sent me.

TRIZ: A Few Words about the History (1) (3:52)



TRIZ: A Few Words about the History (2) (4:45)



TRIZ: A Few Words about the History (3) (5:30)



The slides used in the previous presentation are given below in this link.

<a href="http://www.tetris-project.org/index.php?option=com_docman&task=cat_view&gid=61&Itemid=55&lang=en" target="_blank">http://www.tetris-project.org/index.php?option=com_docman&task=cat_view&gid=61&Itemid=55&lang=en</a>

BTW, you can also check out <a title="Tetris Project" href="http://www.tetris-project.org" target="_blank">http://www.tetris-project.org</a>. (A fun way to learn TRIZ. This project was headed by another colleague of mine, <a href="it.linkedin.com/pub/gaetano-cascini/0/666/69" target="_blank">Prof. Gaetano Cascini</a>)

For the next post, I&#8217;ll talk about the project that I am working on currently in Milan..