---
title: International IPR Conference @ Bangalore
author: Navneet Bhushan
date: 2009-12-22T03:31:54+00:00
banner: /images/default.jpg
url: /2009/12/international-ipr-conference-bangalore/
post_views_count:
  - 453
  - 453
categories:
  - Events
  - Justrizin

---
ITAG is organizing a conference on IP Leveraging in Software, Electronics and Green Technology in co-operation with Sughrue Mion PLLC, a leading law firm in USA and Bangalore Chamber of Industry and Commerce as our co organizer.. The conference will be held on 29th -30th January 2010 at Hotel LaLiT Ashok, Bangalore. The main objective of this conference is to create IP awareness in the field of Software, Electronics Industry and in Green Technology.

Please visit our website for further information- <http://www.iprconference.com>

Conference Programme given below

Tentative Program Details
  
DAY 1 FRIDAY – 29TH JANUARY, 2010
  
09:00-09:30 AM : Registration & Morning Refreshment
  
9:30-10:45 AM :

Welcome Address: Dr. D. R. Agarwal, Director, ITAG Business Solutions Ltd.

Opening Remarks: Mr. Peter McKenna, Partner, Sughrue Mion PLLC, USA

Inaugural Address: By Chief Guest Hon’ble Sri. B.S.Yeddyurappa, the Chief Minister of Karnataka, India

Guests of Honour: Mr. S. Chandrasekaran, Technical Member, Intellectual Property Appellate Board (IPAB), Chennai

Sri. Narayana Murthy, Chief Mentor of Infosys (*TBC)

Mr K R Girish, President, Bangalore Chamber of Industry and Commerce

Dr. Anand Kumar, Director , Indian Oil Corporation Limited

Mr. M. K. Agarwal, Managing Director, GATI Ltd.

Dr. Prabuddha Ganguli, CEO, Vision IPR

Dr. N. L. Mitra Partner, Fox Mandal & Former Vice-Chancellor of NLSIU, Bangalore

Keynote Address: Mr. Dominic Keating, (First Intellectual Property Secretary at US Embassy)
  
10:45-11:00 AM : Tea and Coffee Break
  
11:00 AM-01:00 PM :

Technical Session I: Importance of IP protection and use of patents as a competitive tool for the electronics and software industry in India

Chair Person: Mr. Dominic Keating, First Intellectual Property Secretary at US Embassy

Mr. Chid Iyer, Partner, Sughrue Mion PLLC, USA

Speakers from Samsung and LG (*TBC)

Dr. Pinaki Ghosh, Head IP Cell at Infosys Technologies Ltd
  
1:00-2:00 PM : Networking Executive Lunch
  
02:00-03:00 PM :

Technical Session II: Patenting of software and electronic technologies in the United States in view of recent case law including Bilski

Chair Person: Mr. Chid Iyer, Partner, Sughrue Mion PLLC, USA

Ms. Susan P. Pan, Partner, Sughrue Mion PLLC, USA
  
03:00-04:00 PM :

Technical Session III: Developing and maintaining an optimal patent portfolio for large and medium size electronics companies with special emphasis on patents covering industry standards

Chair person: Mr. S. Chandrasekaran, Technical Member, Intellectual Property Appellate Board (IPAB), Chennai

Mr. Peter McKenna, Partner, Sughrue Mion PLLC, USA
  
4:00-4:15 PM : Networking Coffee Break
  
4:15-5:30 PM :

Technical Session IV: Technology Transfer in the fields of Software, Electronics and
  
Green Technology

Chair Person: Ms. Susan P. Pan, Partner, Sughrue Mion PLLC

Mr. Dominic Keating, First Intellectual Property Secretary at US Embassy

Dr. Prabuddha Ganguli, CEO, Vision IPR
  
5:30-6:15 PM :

Open House Discussion & Vote of Thanks
  
(End of Day I)

DAY 2

SATURDAY – 30TH JANUARY, 2010

9:00-9:30 AM

:

Morning Refreshment

9:30-10:45 AM

:

Technical Session V: Innovation Process Management in Research Institution

Chair Person: Mr. Peter McKenna, Partner, Sughrue Mion PLLC, USA

Mr. Navneet Bhusan, Founder-Director, Crafitti Consulting Pvt. Ltd

Mr. Lakshmikant Goenka, Managing Director, Dolcera

Mr. Kanwal Rai, Founder, Innovation Vault Applications Products & Services Private Limited (IVAPS)

10:45-11:00 AM

:

Networking Coffee Break

11:00-1:00 PM

:

Technical Session VI: International patent filing under PCT route and A comparative study of patentability of electronics and software technologies in US, Japan, EU, China and India

Chair Person: Dr. N. L. Mitra, Partner, Fox Mandal & Former Vice-Chancellor of NLSIU,
  
Bangalore

Mr. Miku H. Mehta, Partner, Sughrue Mion PLLC, USA

Senior Legal Counsel of ATMD Bird & Bird LLP

1:00-2:00 PM

:

Networking Executive Lunch

2:00-3:00 PM

:

Technical Session VII: Green Technology for oil, gas and energy sector

Chair person: Dr. Anand Kumar, Director (R&D), Indian Oil Corporation Limited

Dr. N. L. Mitra Partner of Fox Mandal & Former Vice-Chancellor of NLSIU, Bangalore

Mr. B. L. Chandak, Vice President Corporate Finance, RPG Enterprises

Mr. S. Chandrasekhar, Managing Director, Bhoruka Power Corporation Limited
  
3:00-4:00 PM :

Technical Session VIII: IP policies, strategies and FTO analysis under a competitive
  
environment.

Chair Person: Dr. N. L. Mitra Partner of Fox Mandal & Former Vice-Chancellor of NLSIU, Bangalore

Dr. Prabuddha Ganguli, CEO, Vision IPR

Mr. Jitin Talwar , Advocate & Advocate and Patent Consultants , Talwar & Talwar Consultants

Mr. Roshan Agarwal, Director, Siddhast IP Innovation Pvt. Ltd

4:00-4:15 PM

:

Networking Coffee Break

4:15-6:15 PM

:

Open house discussion with International speakers and Vote of Thanks.

*TO BE CONFIRMED