---
title: Design for Wow
author: Mir Abubakr Shahdad
date: 2009-10-30T11:30:00+00:00
banner: /images/default.jpg
url: /2009/10/design-for-wow/
post_views_count:
  - 306
  - 306
categories:
  - Justrizin

---
Have you ever seen a new innovation, heard some music or looked at some art and uncontrollably said’ **WoW’?**

If so, then please click here: **<u>_[www.DesignForWow.][1]_</u>**

You may also like to see our survey of over 30 creative and problem solving techniques which may have helped develop the WoW

**(Death by a Million Tools).**

What ever you do, please rate our WoW examples AND add your own. We want to hear what you think.

The Design4Wow research Team

United Kingdom

 [1]: http://www.DesignForWow