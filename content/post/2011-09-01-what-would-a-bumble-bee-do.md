---
title: What would a Bumble Bee do ?
author: Dr Shankar MV
date: 2011-09-01T16:25:05+00:00
banner: blogs/13-bumblebee.jpg
url: /2011/09/what-would-a-bumble-bee-do/
post_views_count:
  - 393
  - 393
categories:
  - Justrizin
  - Tools

---
**Look at your Problem through different lenses**

The first step to come up with innovative ideas is to look at a problem innovatively i.e look at the problem through different lenses. We tend to look at a problem from only one angle &#8211; but how to trigger our mind to look at it from different angles ? Try asking the question what would X do &#8211; X could be Steve Jobs or Buddha or Don Quixote or Gandhi or your own Grandfather or Mother-in-law &#8211; the idea is to bring a new perspective to the problem. I listed 100+ such people &#8211; studied their innovative thinking style &#8211; and asked myself &#8220;What would X do?&#8221; to create a new perspective to the problem that I work on. I conciously limit this exercise only to define the problem &#8211; i do not extend this concept to idea generation (for certain reasons that i will elaborate later). When you try to solve a problem innovatively, a trigger that you could use is:

 

<p style="text-align: center;">
  <strong>What would a Bumble Bee do?</strong>
</p>

<a target="_self" href="blogs/12-bumblebee.jpg"><img width="128" src="blogs/13-bumblebee.jpg" class="align-full" /></a>

**Bumble Bee:**

According to 20th century folklore, the laws of aerodynamics prove that the bumble bee should be incapable of flight, as it does not have the capacity (in terms of wing size or beats per second) to achieve flight with the degree of wing loading necessary. The origin of this claim has been difficult to pin down with any certainty (<http://en.wikipedia.org/wiki/Bumble_bee>). So Science said that the bumblebee can not fly. But luckily it the bumble bee did not know that. That is why it flies ! I am reminded here of Arthur Clarke ([http://en.wikiquote.org/wiki/Arthur\_C.\_Clarke][1] ) Clarke&#8217;s First Law: When a distinguished but elderly scientist states that something is possible, he is almost certainly right. When he states that something is impossible, he is very probably wrong.

**Innovation Lesson from Bumble Bee:**

Keep the Experts away from your early brainstorming sessions if you dont want your new ideas to get killed. So if you are a bumble bee you would not be biased by conventional wisdom or bogged down by what the expert say. You will just go ahead and make your new idea work.

**Applying the funda:**

List down a four or five things that conventional wisdom in that domain says and reverse each one of those statements. Brainstorm around these reversed statements and jot down the ideas.

**Example:** Conventional wisdom says that to run a good School, we need good teachers. But what do we do if we cant find good teachers? Reversed statement is we can run a good school without good teachers. A bumble bee would not be worried by lack of good teachers &#8211; it will just go ahead and run the school. Brainstorm how to run a school without teachers &#8211; experiential learning, student teachers, elearning, run videos etc

Image courtesy: wpclipart.com

 [1]: http://en.wikiquote.org/wiki/Arthur_C._Clarke