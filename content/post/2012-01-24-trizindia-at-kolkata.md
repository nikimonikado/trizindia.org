---
title: TRIZIndia at Kolkata
author: Dr Shankar MV
date: 2012-01-24T06:54:23+00:00
banner: blogs/11-TRIZIndiaatKolkata.bmp?width=750
url: /2012/01/trizindia-at-kolkata/
post_views_count:
  - 436
  - 436
categories:
  - Justrizin

---
I represented TRIZIndia in the recent conference on Invention, Innovation & Technology held at Kolkata 19-21 Jan. The conference was dedicated to Acharya Prafulla Chandra Ray (P.C. Ray) who was a great Chemist Innovator and Entrepreneur &#8211; he started the Bengal chemical industry ([http://en.wikipedia.org/wiki/Prafulla\_Chandra\_Ray][1])

 

<a href="blogs/10-TRIZIndiaatKolkata.bmp" target="_self"><img class="align-full" src="blogs/11-TRIZIndiaatKolkata.bmp?width=750" width="750" /></a>I came across a bunch of very dedicated scientists who are passionate about TRIZ..  The conference efforts were lead by Dr Mukherji, Dr Ambar Ghose and Dr Shubrato Ghosh of IAPQR. The participants demonstrated rich experience in applying TRIZ to solving problems in materials science.

 

The conference was very packed with many presentations describing innovatiosn across healthcare, energy, infrastructure, education etc. I gave a 1 Hr talk on TRIZ-based innovation and it was followed by two speakers who demonstrated the application of TRIZ in their projects &#8211; Himanshu Dave and Jhanker Basu &#8211; both talks were excellent. I will scan the list of talks and share it soon.

 

The venue was CGCRI (a CSIR lab specialising in Materials Science Innovations) in South Kolkata.

 

Food was great (lots of sweets) and there was a good magic show (Mukherji&#8217;s) in the evening.

 [1]: http://en.wikipedia.org/wiki/Prafulla_Chandra_Ray