---
title: Analogical Chemistry – Learning Can be Made Fun
author: Prashant Yeshwant Joglekar
date: 2009-09-19T12:23:57+00:00
banner: /images/default.jpg
url: /2009/09/analogical-chemistry-learning-can-be-made-fun/
post_views_count:
  - 300
  - 300
categories:
  - Justrizin

---
Chemistry can be boring, if one’s chemistry with it is poor. The option then is to
  
accept without understanding what is written, memorize & score marks to have
  
better career prospects. . After all atomic theory was not invented by us.
  
We merely accepted it.
  
I was teaching my daughter a lesson in Chemistry and the topic was “Chemical
  
BONDS”. Since I left school, the only BOND I thought I knew was James Bond; I
  
was a Chemistry Scholar then and had scored good marks in my 12th & in preschool.
  
(Now a days the highest level of education is taken at a place called
  
SCHOOL, therefore the term pre-school is for all the other schools starting with
  
Play School)
  
Level of understanding many times is inversely proportional to the marks scored.
  
. Therefore, those who do poorly in these subjects are in fact often more
  
intelligent, only problem with them is that they are trying to comprehend
  
“Chemistry” or for that matter any subject , in a manner, which would let them
  
keep its learning for the life time.
  
But unfortunately these days the rate at which one needs to appear for the exam
  
is far greater than the time available to study & comprehend the subject, so many
  
times children are forced to memorize and score good marks. After all, tangible is
  
what is liked. These children grow up as professionals & chase targets; habit of
  
chasing begin in childhood & continues entire professional life (post retirement,
  
they become a consultant, hate chasing targets and also advice people not to
  
indulge in such a bad habit.)
  
Habit of enquiry, playfulness soon dies in this mad race and those who score
  
maximum marks are admitted to the best schools and are termed as
  
SCHOLARS, who after many years write their memoirs which read something
  
like this “I was born intelligent, expectation to be right in quick time ruined my
  
intelligence”, These scholars are then employed by school drop outs like Bill
  
Gates & Steve Jobs 
  
Oh!!! enough of preamble I will come to the point now … I have decided to use
  
my taste for Systematic Innovation to change all this, staring with self first
  
It was a challenge to explain my daughter the concepts of chemical bonds. The
  
only choice for me to add value was to be different, my customer had an unmet
  
need therefore I started thinking in terms of analogies to be able to contribute
  
meaningfully.
  
We started with IONIC BONDS. The bond is formed between metal and non
  
metal when metal atoms donate electrons to complete the octet of the non metal.
  
Example is NACL where Sodium atom donate electron to complete Chlorine
  
octate. Thus the metals are like “resource-rich, wealthy, blessed and ever willing
  
to help” population and the receiver (non metals) represents downtrodden,
  
opportunity deprived, needy population who falls short of resources and are thus
  
unstable. Once the donation is received the state of instability ceases. The bond
  
between the donor and the receiver thus is very strong, solid like IONIC BOND
  
developed by the force of gratitude & love.
  
Some of the real life examples are people donating huge sum of their earnings or
  
property to a social cause, their personal time in educating, serving others ( even
  
when their per day earning rate is lofty), few donate their body organs ( after
  
death or while they are alive) like Kidney, eyes, blood to the needy. Imagine the
  
strength of the bond thus created. Bond thus formed cannot be easily broken
  
much the same way Ionic bonds cannot be broken due to its higher melting and
  
boiling points.
  
Now let us turn to the COVALENT BONDS. The bond is formed between non
  
metals (equal level people like you & me). Here the electrons are not donated but
  
shared between equal atoms e.g. H2, Cl 2, O2 and Duplet or Octets are formed.
  
These atoms are like self help groups. Real life Example of such bonds are
  
· Group of people sharing their lunch ( they are equal, do you ( non metal)
  
share lunch with your BOSS ( metal), if you do then your boss is lucky)
  
· Pool Cars ( Maintaining own car is expensive, this year’s raise is not
  
sufficient, may also get pink slip, therefore pooling is a better option)
  
· Shared Cabins ( I want a cabin but there are not many, so let us share
  
and be happy)
  
· Common Balcony, Common Toilets, in Mumbai’s famous chawls etc
  
· Share a Cigarette, drink etc
  
· Exchange Notes if you are student ( & then fall in love in filmy style)
  
As they are circumstances based, these bonds can be easily broken taking few
  
cases from above it means if you leave the company you no more share lunch
  
with your friends, you are promoted and therefore you want to show status to
  
your poor partners so you break the pool & join your equals, you migrate to a
  
“tower” or “new housing complex” so no sharing of toilets, balcony with
  
neighbors. We continued till my daughter said she is bonded with bonds and that
  
she never imagined that bond theory was that interesting. I think I had disruption
  
creating a market for chemistry for a non consumer. 
  
Bond with the best, bond with Analogical Chemistry. 