---
title: TRIZ-Sequential or Systematic?
author: Murali Loganathan
date: 2009-11-12T12:00:37+00:00
banner: blogs/30-ShankarTRIZVertical.png
url: /2009/11/triz-sequential-or-systematic/
post_views_count:
  - 240
  - 240
categories:
  - Justrizin
  - Tools

---
We had Shankar visiting us at Bangalore last month. One of the activities that we did while he was here was without getting into any technique-terminology of TRIZ is it possible for some one to use TRIZ?

We took one sample problem from the IT industry context about a web site experience. While we were doing this I was actively applying TRIZ techniques without deliberately naming it. As a note of thanks to Shankar I created a visual representation of what we did that day.

<p style="text-align: left;">
  <img src="blogs/30-ShankarTRIZVertical.png" alt="" />
</p>

Several techniques can fit into the picture above for example
  
· Understanding the context clearly is something 9 windows can do effectively giving you a sense of history and moving through present to future.
  
· FAA could come in next with identification of system elements and relationships
  
· Problem explorer could come next helping us to find what&#8217;s stopping us and why we solve a problem. IFR also can come in here.
  
· Abstracting and Identifying contradictions may be tricky but here is where practice comes
  
· From the principles you can ideate, as you can see in the picture as well the order in which principles are listed in the matrix, the top most is likely to give you better solutions. During our own exercise one idea was reached through multiple principles which is a good thing.

My fear is stating above is that someone who sees it may think TRIZ is a sequential process to innovate. In reality it is not, TRIZ provides multiple set of tools for you to work with and resolve contradictions, and getting radical.

Still there is utility for using TRIZ without speaking its hard language. What do you think?