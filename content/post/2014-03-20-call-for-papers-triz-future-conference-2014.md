---
title: CALL FOR PAPERS – TRIZ FUTURE CONFERENCE 2014
author: nikimonikado
date: 2014-03-20T09:38:27+00:00
url: /2014/03/call-for-papers-triz-future-conference-2014/
post_views_count:
  - 2901
categories:
  - Events

---
<div id="parent-fieldname-description">
  European TRIZ Association (ETRIA) is organizing TRIZ Future Conference (TFC2014) along with Exelop SA will be hosted by EPFL Lausanne, Switzerland. TFC2014 is also sponsored by CIRP, the international Academy for Production Engineering. TFC2014 will provide an international forum for exchanging new ideas on knowledge‐ based innovation and TRIZ, presenting recent achievements by the scientific community and enabling further advances and collaboration also with the industrial community.
</div>

<div id="viewlet-above-content-body">
</div>

<div id="content-core">
  <div id="parent-fieldname-text">
    <div id="page_1">
      <p>
        <a href="http://trizindia.org/wp-content/uploads/2014/03/CALL-FOR-PAPERS-TFC20142x1.jpg"><img class="alignnone size-full wp-image-261" alt="CALL FOR PAPERS TFC20142x1" src="http://trizindia.org/wp-content/uploads/2014/03/CALL-FOR-PAPERS-TFC20142x1.jpg" width="552" height="57" srcset="http://trizindia.org/wp-content/uploads/2014/03/CALL-FOR-PAPERS-TFC20142x1.jpg 552w, http://trizindia.org/wp-content/uploads/2014/03/CALL-FOR-PAPERS-TFC20142x1-300x30.jpg 300w" sizes="(max-width: 552px) 100vw, 552px" /></a>
      </p>
      
      <h2>
        CALL FOR PAPERS
      </h2>
      
      <h2>
        TRIZ FUTURE CONFERENCE 14
      </h2>
      
      <h2>
        Global innovation Convention
      </h2>
      
      <p>
        &nbsp;
      </p>
    </div>
    
    <div id="page_2">
      <div>
      </div>
      
      <h3>
        Place:
      </h3>
      
      <p>
        EPFL (Ecole Polytechnique Fédérale de Lausanne), Switzerland
      </p>
      
      <h3>
        Duration:
      </h3>
      
      <p>
        From 29th to 31th October 2014
      </p>
      
      <h3>
        Cost:
      </h3>
      
      <p>
        650 EUR per participant (Further prices for students, Early birds, others are available on www.tfc14.com )
      </p>
      
      <h3>
        Organizer:
      </h3>
      
      <p>
        ETRIA’s TRIZ Future Conference TFC2014 is this year locally organized by Exelop SA in association with the ETRIA board and will be hosted by EPFL Lausanne – Switzerland. TFC2014 is also sponsored by CIRP, the international Academy for Production Engineering.
      </p>
      
      <h3>
        The event:
      </h3>
      
      <p>
        The 3 days conference aims at linking:
      </p>
      
      <p>
        ‐Industrialcompanies
      </p>
      
      <p>
        ‐Research centers
      </p>
      
      <p>
        ‐Educational organizations
      </p>
      
      <p>
        ‐Individuals
      </p>
      
      <p>
        to share their experience on systematic innovation and to develop research in the systematic innovation area.
      </p>
      
      <p>
        TFC2014 will provide an international forum for exchanging new ideas on knowledge‐ based innovation and TRIZ, presenting recent achievements by the scientific community and enabling further advances and collaboration also with the industrial community
      </p>
      
      <p>
        The conference is divided into twoparts:
      </p>
      
      <p>
        ‐Scientific Session: Professors and researchers present results on scientific studies
      </p>
      
      <p>
        ‐Industrial Session: Professionals present results of practical experiences, best practices, and case studies
      </p>
    </div>
    
    <div id="page_3">
      <div>
      </div>
      
      <h2>
        Abstracts
      </h2>
      
      <h3>
        Theories, methods, tools and experiences on:
      </h3>
      
      <p>
        ‐Global Knowledge management for innovative conception
      </p>
      
      <p>
        ‐Innovation on Servicesusing robust and systematic tools
      </p>
      
      <p>
        ‐TRIZ‐based or inspiredtheories, methodologies, techniques
      </p>
      
      <p>
        ‐Patent Mining, knowledge harvesting and representing
      </p>
      
      <p>
        ‐Any professional/industrial case study where TRIZ has played a significant role
      </p>
      
      <p>
        ‐Further advanced Innovative, Inventive & creative design processes based on systematicmethods
      </p>
      
      <p>
        ‐IT for innovative engineering /Computer Aided Innovation (CAI)
      </p>
      
      <p>
        ‐Knowledge Based Systematic Innovation Customized Services
      </p>
      
      <p>
        ‐Product lifecycle management, laws of technical systemevolution
      </p>
      
      <p>
        ‐Methodological support to Creativeand Inventive Design
      </p>
      
      <p>
        ‐Inventiveness, creativity, innovation measurements (or assessment) based on systematicmethods
      </p>
      
      <p>
        The subjects listed above are not limitative. They are expected to form the major themes of this TFC 2014 Conference. Submissions concerning these subjects are specifically encouraged, but all abstracts describing any aspect of systematic creative and innovative design will be given fair consideration. Authors interested in submitting abstracts are asked toupload their proposal to the conference website.
      </p>
      
      <h3>
        Review:
      </h3>
      
      <p>
        There will be a formal review of all abstracts.
      </p>
      
      <p>
        The abstracts must be submitted through the TFC2014 conference management system available through: www.tfc14.com and should be of a quality suitable for selection. The abstract should outline the major points of interest and novelty of the paper.
      </p>
      
      <p>
        Selection of the papers will be made on the basis of the following criteria:
      </p>
      
      <p>
        ‐General interest of the subject
      </p>
      
      <p>
        ‐Quality of the content
      </p>
      
      <p>
        ‐Relevance to subjects covered by any of the two committees, namely (scientific or professional).
      </p>
      
      <p>
        Authors should indicate to which category of contribution they intend to contribute.
      </p>
      
      <h3>
        Submission requirements
      </h3>
      
      <p>
        Interested parties are invited to submit an abstract (250 to 300 words) of their proposed paper with
      </p>
      
      <p>
        ‐Title, along with a brief
      </p>
      
      <p>
        ‐Name and current biography of the author/s.
      </p>
      
      <p>
        ‐Affiliation
      </p>
      
      <p>
        ‐Contact details (phone number, email id, mailing address with postal code and country)
      </p>
      
      <p>
        In order to submit your paper, a link will be available soon published onwww.tfc14.com
      </p>
    </div>
    
    <div id="page_4">
      <h3>
        Important Deadlines
      </h3>
      
      <p>
        Completing requirements of the Conference on time is critical for all presenters, conference planners and attendees, so the following deadlines are mandatory for your participation.
      </p>
      
      <table>
        <tr>
          <td>
            Abstract due
          </td>
          
          <td>
            15th March 2014
          </td>
        </tr>
        
        <tr>
          <td>
            Notification of acceptance
          </td>
          
          <td>
            30th March 2014
          </td>
        </tr>
        
        <tr>
          <td>
            Full paper due
          </td>
          
          <td>
            15th May 2014
          </td>
        </tr>
        
        <tr>
          <td>
            Reviewer comments
          </td>
          
          <td>
            30th June 2014
          </td>
        </tr>
        
        <tr>
          <td>
            Final paperdue
          </td>
          
          <td>
            22nd July 2014
          </td>
        </tr>
        
        <tr>
          <td>
            Conference
          </td>
          
          <td>
            From 29th to 31st October 2014
          </td>
        </tr>
      </table>
      
      <h3>
        Author Guidelines
      </h3>
      
      <p>
        Two different types ofpapers will be considered:
      </p>
      
      <p>
        ‐«Scientific papers» related to the TRIZ theory and methodological developments
      </p>
      
      <p>
        ‐«Practitioners papers» related to the application of Innovation methodologies
      </p>
      
      <p>
        Two lengths of papers will be distinguished:
      </p>
      
      <p>
        ‐<strong>« Short papers »</strong>
      </p>
      
      <p>
        They should be at least 2 pages long (approximately 800 words), leading to 15 minutes of presentation (including Q&A)
      </p>
      
      <p>
        ‐<strong>« Long papers »</strong>
      </p>
      
      <p>
        They should be at least 4 and maximum 6 pages long (approximately 2400 words), granting access to a full‐lengthpresentation (30 minutesincluding Q&A)
      </p>
      
      <p>
        The finalpaper template will be available soon on <a href="http://www.tfc14.com/">www.tfc14.com</a>
      </p>
      
      <p>
        This flyer can be downloaded <a href="http://www.format-project.eu/news/copy_of_call-for-papers-for-triz-future-conference-2014">here</a>.
      </p>
      
      <p>
        EXELOP SA
      </p>
      
      <p>
        Route de Saint Julien 184 A, 1228 Plan‐les‐Ouates
      </p>
      
      <p>
        GENEVE SUISSE
      </p>
      
      <p>
        Email : <a href="mailto:info@exelop.com?subject=TFC%20Future%20Conference%202014">info@exelop.com</a>‐ Tél: 022 556 0801
      </p>
    </div>
  </div>
</div>
