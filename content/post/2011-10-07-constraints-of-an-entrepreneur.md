---
title: Constraints of an Entrepreneur
author: Bala Ramadurai
date: 2011-10-07T07:28:44+00:00
banner: http://www.avembsys.com/images/av_orange_white.png
url: /2011/10/constraints-of-an-entrepreneur/
enclosure:
  - |
    |
        http://www.archive.org/download/ConstraintsOfAnEntrepreneur/TrizindiaPodcast-faizan.mp3
        16839776
        audio/mpeg
        
post_views_count:
  - 396
categories:
  - Podcasts
tags:
  - business
  - constraints
  - entrepreneur
  - entrepreneurship
  - innovation
  - invention
  - triz
  - trizindiapodcast

---
<img class="alignnone" title="Avembsys" src="http://www.avembsys.com/images/av_orange_white.png" alt="Avembsys" width="244" height="63" />

<img class="alignnone" title="Avembsys" src="http://www.avembsys.com/images/avembsys_banner.png" alt="" width="698" height="130" />

A special podcast from TRIZ India on constraints. Mirza Faizan, an entrepreneur based out of Bangalore, India, shares his experience on the kind of constraints he faced and is currently facing. He describes his aerospace invention in detail and how his prototype can save lives not in the air, but right here on terra firma. In the process, Faizan also gives out advice to budding entrepreneurs. <a title="http://www.avembsys.com" href="http://www.avembsys.com" target="_blank">http://www.avembsys.com</a> is the link to Faizan&#8217;s company.

[Listen to the podcast here][1]

 [1]: http://www.archive.org/download/ConstraintsOfAnEntrepreneur/TrizindiaPodcast-faizan.mp3