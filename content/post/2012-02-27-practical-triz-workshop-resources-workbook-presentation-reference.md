---
title: Practical TRIZ Workshop Resources Workbook + Presentation + Reference
author: Murali Loganathan
date: 2012-02-27T06:42:20+00:00
banner: /images/default.jpg
url: /2012/02/practical-triz-workshop-resources-workbook-presentation-reference/
post_views_count:
  - 1469
  - 1469
categories:
  - Justrizin
  - Tools

---
We had given out a <a href="https://docs.google.com/document/d/1QJ_2d1vmc_Y_X8yjyTfXNUD77hJjUGxUFv6tY4KJJgg/edit" target="_blank">TRIZ India Workbook</a> with enough space to doodle, brainstorm and use the specific guidelines on techniques used. 

While the appendix with the Business and Management Contradiction Matrix can be referred on  Darrell Mann reference, <a href="http://www.amazon.com/Hands-Systematic-Innovation-Business-Management/dp/1898546738/ref=sr_1_5?ie=UTF8&qid=1330324587&sr=8-5" target="_blank">available on amazon</a></p> 

We had also used  <a href="https://docs.google.com/presentation/d/1b1aIYt2u1CsqW0gbFMageAqvLp6lnd7lsKW8cmjjESY/edit" target="_blank">TRIZ India Workshop Presentation</a> , typical structure that we had for techniques is as follows

  * the basic theory or other basis behind the technique
  * a strong story where it was applied from either a personal experience or from other sources
  * personal/individual application
  * group application and facilitation notes

While we saw tremendous enthusiasm from industry (across IT, Oil and Gas, Energy, Pharma, Engineering in this workshop alone) we hope to get broader participation from academia as well. </p> 

Chennai workshop is coming up in March, we could come to your city too if there is a critical strength and participation. 

Road trip anyone?</p> 

Questions/Comments welcome&#8230;