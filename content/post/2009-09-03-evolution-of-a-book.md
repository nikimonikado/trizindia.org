---
title: Evolution of a Book
author: Prakasan K
date: 2009-09-03T17:42:32+00:00
banner: /images/default.jpg
url: /2009/09/evolution-of-a-book/
post_views_count:
  - 305
  - 305
categories:
  - Justrizin

---
So, this must be something you have already seen in the news. <a href="http://www.redorbit.com/news/technology/1746919/diginovel_will_include_book_video_web/" target="_blank">‘Digi-Novel’ Will Include Book, Video, Web</a>
  
If there is one thing which has been around for centuries without much of an evolution is a book. While the process of manufacturing, papers, inks, machines etc would have gone through multiple S-curves, book is a book has been around as it is.

After reading this news, I was wondering if we are going to see a new S-curve in the evolution of a book. Will the technology make this evolution success?