---
title: TRIZ India Podcast with Jack Hipple – Interesting Case of the Hairnet bags and the Lucy Show
author: nikimonikado
date: 2016-01-11T10:03:38+00:00
banner: http://www.idsa.org/sites/default/files/colleenb/Jack%20Hipple%20Photo_cb_0.jpg
url: /2016/01/triz-india-podcast-with-jack-hipple-hairnet-bags-and-the-lucy-show/
post_views_count:
  - 212
enclosure:
  - |
    |
        https://archive.org/download/JackHippleTRIZIndiapodcast/JackHipple-TRIZIndiapodcast.mp3
        0
        audio/mpeg
        
categories:
  - Podcasts

---
<div style="width: 298px" class="wp-caption alignnone">
  <img class="" src="http://www.idsa.org/sites/default/files/colleenb/Jack%20Hipple%20Photo_cb_0.jpg" alt="" width="288" height="288" />
  
  <p class="wp-caption-text">
    Jack Hipple
  </p>
</div>

Dr. Bala Ramadurai, Co-founder of TRIZ Innovation India (trizindia.org) has a chat with Jack Hipple, Chemical Engineer, TRIZ practitioner and trainer (http://www.innovation-triz.com/personnel/JHipple.html) about his experiences on learning and applying TRIZ.

Download the podcast from here &#8211; https://archive.org/download/JackHippleTRIZIndiapodcast/JackHipple-TRIZIndiapodcast.mp3

_1. When did you come across TRIZ? When did you decide to take on TRIZ as your path forward in your career?_
  
An accident. As Director of Dow Chemical&#8217;s Discovery Research program, an attempt to broaden Dow&#8217;s business beyond commodity chemicals, I sought out training in many psychologically based creativity tools such as brainstorming, CPS, Debono&#8217;s Lateral Thinking and Six Hats. I found these tools rather unproductive and time consuming when it came to serious technical problems. After my 26 years with Dow in 1993, I became a project manager for the National Center for Manufacturing Sciences, and as part of this role, decided to gain some knowledge of quality tools and attended a quality conference in Detroit, MI in 1994 when I learned a little about QFD, the house of quality, measurement tools, and around 3:30 in the afternoon, when I was thinking about leaving, I looked at the agenda and saw a talk by two Russian scientists (and I have apologized many times for not remembering their names) entitled &#8220;The Use of TRIZ to resolve the contradiction of Pizza Box Design&#8221;. First of all, I had no idea what &#8220;TRIZ&#8221; was nor what cosmic issues were involved in the design of pizza boxes. So I stayed out of total curiosity, and as we all have had at one time or another, it was a life changing event. The talk was about a very practical aspect of take out pizza. You want the pizza to be hot, but when it is, it gives off steam which softens the cardboard lid, causing it to sag into the pizza and when you lift the lid at home, it takes a good deal of the pizza topping with it. So there&#8217;s a contradiction (and everyone involved in TRIZ knows that solving a contradiction is the key to an invention). The little plastic tripod found in most all take out pizzas (at least in the US) was invented with TRIZ. It resolves this contradiction in a very simple way. I literally fell off my chair after hearing this presentation and resolved to learn some more about this process. It took a couple of years for me to pursue TRIZ with any significant time, but it became my primary career in 2001 after serving in project manager roles at the National Center for Manufacturing Sciences and Cabot Corporation, neither of which allowed me to use TRIZ in any significant way. I was hired by one of the TRIZ software companies and became their primary chemical industry trainers for TRIZ and later by a management consulting company interested in bringing TRIZ into their business. I started my own company in 2001 and also began dong TRIZ training for the American Society of Mechanical Engineers and the American Institute of Chemical Engineers in 2003.

_2. What were some of the challenges that you faced on the way? How did you overcome it?_
  
I think anyone who has started their own business has had to overcome the short term lack of cash flow and the slow process of reintroducing your self to prior contacts and telling them you had something new they might be interested in. There were also presentation at appropriate conferences trying to get the word out. It also helps to have a very supportive family. Though we had become empty nesters by the time my business got started, my wife went back to work to help sustain us and also provide medical insurance coverage, which always more expensive when it is bought privately and not through a group policy. Without this support, it would have been difficult to get things off the ground. The challenge in getting the professional engineering societies interested took a while to overcome, but once done, provided both income and possible client contacts.

_3. What are some of the common learning difficulties that one should avoid while learning and applying TRIZ?_
  
There are two primary difficulties in starting with TRIZ within an organization. The first is ego. The primary postulate within TRIZ is that there a limited number of inventive principles that we continuously reuse. This is not typically recognized within an industry or company due, in part, to the extensive use of acronyms and fancy words that make someone think that there problem is special and unique. It almost never is. One of the exercises I run with my clients or within a workshop is to force the attendees to describe their problem to a ten year old. The language is never the same and using these much simpler, more generic terms in a Google search will yield at least an order of magnitude more possible areas to consider.

The second barrier is not being willing to take the time to sufficiently define what the problem is that we are attempting to solve. It is always amazing to me, as an outsider working with a client (and I am told the team has been working on some problem for a long time), and using any one of a number of TRIZ problem definition tools, that these groups, when separated, come back with very different definitions of the problem. The good or bad functions are not seen the same, the number of contradiction is not the same, etc. They have spent a huge amount of time trying to solve the wrong problem. Though this is one of TRIZ&#8217;s greatest strengths, it is also a very time consuming step. There are many organizations I have worked with that simply can&#8217;t or won&#8217;t take this time and TRIZ implementation is a failure. As frustrating as this is, I have learned to accept this fact and it is one shared by many other TRIZ professionals that I know.

_4. The common belief about TRIZ that I hear a lot is that TRIZ is only for engineers. I am sure you&#8217;ve heard that a lot as well. What are some of your experiences that should tell the listeners of this podcast otherwise?_
  
Though much of my work is in the technical area, the principals of TRIZ can be equally well applied in what I call &#8220;soft&#8221; applications. There are contradictions and functional conflicts in marketing, business strategy, and human resources. I find the use of the basic TRIZ separation principles particularly useful in these situations. The problems in this area are easier to define, but as opposed to the technical area, may have even more views of what is &#8220;ideal&#8221;, what &#8220;resources&#8221; are available, and what the important contradictions are that need to be resolved. It is also important for TRIZ professionals to not overstate what TRIZ can accomplish in this area. For example, TRIZ is not the right tool to help decide what the color of something should be.

There are some technical areas that TRIZ has some limited applications such as pure chemical synthesis and biological processes. Though there some interesting examples that we can point to, there is not, in my opinion anything close to a contradiction table or AREIZ algorithm for either of these areas.

_5. In your experience, what is an excellent example of application of TRIZ that still stands out?_
  
There is one in particular which is no longer covered by a confidentiality agreement and that is one of my first major commercial projects with S.C. Johnson, a multi-billion $$, privately held, consumer products company that manufactures a multitude of cleaning and household products. When Dow decided to divest itself of its consumer products business (products such as Dow Oven Cleaner, Handiwrap, Saran Wrap, Bathroom cleaner) it sold this business to S.C. Johnson, already a major player in this business space. After this business sale, Dow continued to sell the Saran polymer to S.C. Johnson.

Most people are unaware that this plastic wrap is not the same polymer/plastic as others, which are primarily polyethylene. This plastic has excellent barrier to moisture transfer, but very poor barrier against oxygen transfer. Saran is a copolymer of vinyl chloride and vinylidene chloride and has outstanding barrier properties to both and this is important in the preservation of many fruits and vegetables. Unfortunately, along with this plus, the negative is that this polymer has very poor tensile strength, and in its typical roll form, splits and tears and since it is clear, the consumer cannot readily find where the film cut was and in many cases would simply throw away the product and return to using polyethylene, knowing that it is not as good a product. So the polymer itself was superior, but its form was not.

When S.C. Johnson inherited this product from Dow, and with its far greater sensitivity to consumer needs, decided to change the form of the product into something that looked liked a &#8220;hairnet bag&#8221; which would stretch around any size container and eliminate the need to a roll form and its limitations. That&#8217;s where the great idea stopped being so great. The decision was made to ship the plastic film overseas where it would be converted into its hairnet shape and then &#8220;wrap&#8221; ten of these bags with a double rubber band wrap (think about how much fun this was for a plant operator to do), and then ship back thousands of these bundles, compressed in an overseas shipping container (don&#8217;t want to pay for space not used!), back to Racine, WI (S.C. Johnson&#8217;s primary corporate site), where another operator would now have to do two very difficult tasks. First, take the rubber band off (not easy and ergonomic injuries are possible), and while this is being done, try to grab the bags, which now want to fly everywhere, and stuff them In a cottage container and put a lid on it. Those on this podcast from the US may remember the famous Lucy TV show where the bakery line was slowly sped up and Lucy had to keep up and that&#8217;s exactly what this looked like. (Please refer to https://www.youtube.com/watch?v=HnbNcQlzV-4 for the snippet)

After a short introduction to TRIZ principles, the project team quickly came up with a simple solution which allowed the entire process to be simplified with huge cost savings and that was to simply put nine bags into the tenth bag. No rubber bands, virtually no labor (process could be automated), the packing simplified, the shelf space reduced dramatically (just hang on a spindle) and since there was no human involvement, sterilization costs were virtually eliminated.

I frequently use this example in my workshops as it illustrates the use of the simplest of TRIZ tools (IFR: the bags package themselves, RESOURCES: use an existing bag instead of a rubber band, and TRIMMING: get rid of the band and additional packaging)

_6. What is you advice to the listeners of this podcast who want to start off applying TRIZ? If you have worked with Indian companies, please give us some Indian context as well._
  
I have not had the pleasure of working with any Indian companies, but my advice to anyone starting out with TRIZ is to NEVER start with any software product. It is critical to get the brain part down first; the basic TRIZ algorithm, the use of the IFR, resources, and contradictions. Once these are well understood, software can be considered, but I am constantly amazed at how many problems are solved with the simplest of TRIZ tools.

_7. Hypothetical question to end this podcast &#8211; According to you, what developments in the invention world would Altshuller have commented, &#8220;Wish I had thought of that!&#8221;_
  
I am not sure if Altshuller recognized or saw the degree to which TRIZ could be applied in business, strategic, IP, and organizational problem solving.

Some References suggested by Jack Hipple

His books are given below: If anyone is interested in bulk discounts of these books, please let us know and we&#8217;ll be happy to pass it on to Jack Hipple (admin AT trizindia DOT org)

http://www.aitriz.org/online-store/triz-online-store/books/ideal-result-detail

http://www.springer.com/us/book/9781461437062

The Chemical Engineering Progress series is at;

http://www.aiche.org/resources/publications/cep/2005/april/solve-problems-inventively

http://www.aiche.org/resources/publications/cep/2005/may/use-triz-reverse-analyze-failures

http://www.aiche.org/resources/publications/cep/2005/june/use-triz-plan-forecast-and-think-strategically

When you go to these CEP pages, you will see, at the bottom, a link and info about reprints, etc.

Anyone who is an AIChE member can download.

His next public TRIZ classes for ASME are in Las Vegas and Atlanta:

https://www.asme.org/products/courses/triz&#8211;the-theory-of-inventive-problem-solving

His other podcasts are available at:

http://www.ideaconnection.com/open-innovation-articles/ (scroll down a bit)

as well as for Georgia Tech:

https://smartech.gatech.edu/handle/1853/48282